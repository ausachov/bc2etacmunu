from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles, FilterInTrees
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)


def fillTuple( tuple, myTriggerList, tupleType ):

    from Configurables import TupleToolVtxIsoln, TupleToolIsolationTwoBody, TupleToolDira, TupleToolDocas, TupleToolTauMuDiscrVars, TupleToolSLTools
    # TupleToolmuTrackIsolationSaveTracks

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      # "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                      "TupleToolVtxIsoln",
                      "TupleToolIsolationTwoBody",
                      "TupleToolDira"
                      # "TupleToolmuTrackIsolationSaveTracks"
                     ]
    track_tool = tuple.addTupleTool('TupleToolTrackInfo')
    track_tool.Verbose = True




    if tupleType=="Bc":
      from Configurables import TupleToolNeutrinoReco
      tuple.addTool( TupleToolNeutrinoReco,name = "TupleToolNeutrinoReco" )
      tuple.TupleToolNeutrinoReco.Verbose = True
      tuple.TupleToolNeutrinoReco.MotherMass=6274.9 #MeV
      tuple.TupleToolNeutrinoReco.ExtraName='mBc'

    from Configurables import TupleToolTISTOS, TupleToolDecay

    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTupleTool(TupleToolDecay, name = 'Ks')
    tuple.Ks.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForKs" ]
    tuple.Ks.addTool(TupleToolTISTOS, name="TupleToolTISTOSForKs" )
    tuple.Ks.TupleToolTISTOSForKs.Verbose=True
    tuple.Ks.TupleToolTISTOSForKs.TriggerList = myTriggerList

    LoKi_Ks=LoKi__Hybrid__TupleTool("LoKi_Ks")
    LoKi_Ks.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Ks.addTupleTool(LoKi_Ks)


    tuple.addTupleTool(TupleToolDecay, name = 'Bc')
    tuple.Bc.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBc" ]
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBc" )
    tuple.Bc.TupleToolTISTOSForBc.Verbose=True
    tuple.Bc.TupleToolTISTOSForBc.TriggerList = myTriggerList


    tuple.Bc.addTupleTool("TupleToolSLTools")
    tuple.Bc.addTupleTool("TupleToolTauMuDiscrVars")


    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    LoKi_Bc.Preambulo = [
      "MASSES = LoKi.AuxDTFBase.MASSES",
      "massEtac = MASSES() ",
      "massEtac['eta_c(1S)'] = 2.9836 * GeV"
      ]

    varsB = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        # "m_scaled"             : "DTF_FUN ( M , False )",
        "DOCA"                 : "DOCA(1,2)",
        "m_pv"     : "DTF_FUN ( M , True )",
        "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"  : "DTF_CHI2NDOF( True  )",
        "DTFEtac_M": 'DTF_FUN(M , False, strings( [ "eta_c(1S)"] ) , massEtac)'
      }


    LoKi_Bc.Variables = varsB
    tuple.Bc.addTupleTool(LoKi_Bc)

    from Configurables import TupleToolDecayTreeFitter
    tuple.Bc.ToolList += [ "TupleToolDecayTreeFitter/DTFJpsi"]
    tuple.Bc.addTool(TupleToolDecayTreeFitter("DTFJpsi"))
    tuple.Bc.DTFJpsi.Verbose=True
    tuple.Bc.DTFJpsi.daughtersToConstrain=["eta_c(1S)"]
    tuple.Bc.DTFJpsi.constrainToOriginVertex=False


    if tupleType=="Bs":
      tuple.addTupleTool(TupleToolDecay, name = 'Bs')
      tuple.Bs.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBs" ]
      tuple.Bs.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBs" )
      tuple.Bs.TupleToolTISTOSForBs.Verbose=True
      tuple.Bs.TupleToolTISTOSForBs.TriggerList = myTriggerList

      LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bc")
      LoKi_Bs.Variables = varsB
      tuple.Bs.addTupleTool(LoKi_Bs)


    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTupleTool(LoKi_All)

    tuple.Bc.addTupleTool( 'TupleToolSubMass' )
    tuple.Bc.TupleToolSubMass.Substitution += ["mu- => pi-"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu- => e-"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu- => K-"]
    tuple.Bc.TupleToolSubMass.Substitution += ["K+ => pi+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["K+ => p+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["pi- => K-"]
    tuple.Bc.TupleToolSubMass.Substitution += ["pi- => p~-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["pi-/pi+ => K-/p+"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["pi-/pi+ => K-/K+"]
    # tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["pi-/pi+ => p~-/p+"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["pi-/pi+ => p~-/K+"]
    # tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["K+/pi- => p+/p~-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["K+/pi- => p+/K-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["K+/pi- => pi+/p~-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["K+/pi- => pi+/K-"]









myTriggerList = [
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",

                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1SingleMuonNoIPDecision",
                 "Hlt1DiMuonNoIPDecision"

                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TrackAllL0Decision",

                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",

                 "Hlt2PhiIncPhiDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision",

                 "Hlt2Bc2JpsiXTFBc2JpsiMuXDecision",
                 "Hlt2Bc2JpsiXBc2JpsiHDecision",

                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",

                 "Hlt2SingleMuonDecision",
                 "Hlt2SingleMuonRareDecision",
                 "Hlt2SingleMuonVHighPTDecision",

                 "Hlt2XcMuXForTauB2XcMuDecision"
                 ]



from PhysSelPython.Wrappers import MergedSelection

inputData = AutomaticData("/Event/CharmCompleteEvent/Phys/Bc2EtacMu_KsKpiLine/Particles")



KsFromStr = FilterInTrees("KsFromStr",Code="('KS0' == ABSID)")
KsSel = Selection("KsSel",Algorithm=KsFromStr,
                  RequiredSelections=[inputData])

from StandardParticles import StdAllNoPIDsPions as Pions
from StandardParticles import StdAllNoPIDsKaons as Kaons
from PhysConf.Selections import CombineSelection

JpsiSel = CombineSelection(
        'JpsiSel',
        [Pions, Kaons, KsSel],
        DecayDescriptor='eta_c(1S) -> KS0 K+ pi-',
        DaughtersCuts = {
           "K+"  : "(PROBNNk > 0.65)  & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)",
           "pi-" : "(PROBNNpi > 0.65) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (MIPCHI2DV(PRIMARY) > 4)"
        },
        CombinationCut="in_range(2.7*GeV, AM, 3.3*GeV)",
        MotherCut="in_range(2.7*GeV, MM, 3.3*GeV) & (VFASPF(VCHI2/VDOF) < 9.)"
    )

# JpsiFromStr = FilterInTrees("JpsiFromStr",Code="('eta_c(1S)' == ABSID)")
# JpsiSel = Selection("JpsiSel",Algorithm=JpsiFromStr,
#                   RequiredSelections=[inputData])

MuFromStr = FilterInTrees("MuFromStr",Code="('mu-' == ABSID)")
MuSel = Selection("MuSel",Algorithm=MuFromStr,
                  RequiredSelections=[inputData])

MakeBc = CombineParticles('MakeBc',
                      DecayDescriptor='[B- -> eta_c(1S) mu-]cc',
                      DaughtersCuts={"eta_c(1S)": "(M > 2600)"},
                      CombinationCut="(AM < 50000) & (AM > 2500)",
                      MotherCut="ALL")
BcSel = Selection(MakeBc.getName() + 'Selection',
                  Algorithm=MakeBc,
                  RequiredSelections=[MuSel, JpsiSel])
BcSelSeq = SelectionSequence(BcSel.name() + 'Sequence', TopSelection=BcSel)


Bc2JpsimunuDecay = "[B- -> ^(eta_c(1S) -> ^(KS0 -> ^pi+  ^pi-) ^K+  ^pi-) ^mu-]CC"
Bc2JpsimunuBranches = {
     "Bc"     :  "[B- -> (eta_c(1S) -> (KS0 -> pi+  pi-) K+  pi-)  mu-]CC",
     "Jpsi"   :  "[B- ->^(eta_c(1S) -> (KS0 -> pi+  pi-) K+  pi-)  mu-]CC",
     "Ks"     :  "[B- -> (eta_c(1S) ->^(KS0 -> pi+  pi-) K+  pi-)  mu-]CC",
     "K"      :  "[B- -> (eta_c(1S) -> (KS0 -> pi+  pi-)^K+  pi-)  mu-]CC",
     "Pi"     :  "[B- -> (eta_c(1S) -> (KS0 -> pi+  pi-) K+ ^pi-)  mu-]CC",
     "ksPiP"  :  "[B- -> (eta_c(1S) -> (KS0 ->^pi+  pi-) K+  pi-)  mu-]CC",
     "ksPiM"  :  "[B- -> (eta_c(1S) -> (KS0 -> pi+ ^pi-) K+  pi-)  mu-]CC",
     "Mu"     :  "[B- -> (eta_c(1S) -> (KS0 -> pi+ ^pi-) K+  pi-)  ^mu-]CC"
}



Bc2JpsimunuTuple = DecayTreeTuple('Bc2JpsimunuTuple')
Bc2JpsimunuTuple.Inputs   = [BcSelSeq.outputLocation()]
Bc2JpsimunuTuple.Decay    =  Bc2JpsimunuDecay
Bc2JpsimunuTuple.Branches =  Bc2JpsimunuBranches
fillTuple( Bc2JpsimunuTuple, myTriggerList, "Bc" )



from StandardParticles import StdAllNoPIDsMuons
from PhysConf.Selections import CombineSelection
Bs2JpsimumuSel = CombineSelection(
        'Bs2JpsimumuSel',
        [BcSel, StdAllNoPIDsMuons],
        DecayDescriptor="[B_s0 -> B- mu+]cc",
        DaughtersCuts = {
           "mu+" : "(PROBNNmu > 0.001) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
           "B-"  : "(BPVCORRM>3000)",
        },
        CombinationCut="(AM > 3000)",
        MotherCut="(VFASPF(VCHI2/VDOF)<16.0)"
    )
Bs2JpsimumuSelSeq = SelectionSequence('Bs2JpsimumuSequence', TopSelection=Bs2JpsimumuSel)

Bs2JpsimumuDecay = "[B_s0 -> ^(B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-) ^mu+]CC"
Bs2JpsimumuBranches = {
     "Bs"       :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-) mu-) mu+]CC",
     "Bc"       :  "[B_s0 ->^(B- -> (J/psi(1S) -> p+ p~-) mu-) mu+]CC",
     "Jpsi"     :  "[B_s0 -> (B- ->^(J/psi(1S) -> p+ p~-) mu-) mu+]CC",
     "ProtonP"  :  "[B_s0 -> (B- -> (J/psi(1S) ->^p+ p~-) mu-) mu+]CC",
     "ProtonM"  :  "[B_s0 -> (B- -> (J/psi(1S) -> p+^p~-) mu-) mu+]CC",
     "MuP"      :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-)^mu-) mu+]CC",
     "MuM"      :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-) mu-)^mu+]CC"
}
Bs2JpsimumuTuple = DecayTreeTuple('Bs2JpsimumuTuple')
Bs2JpsimumuTuple.Inputs   = [Bs2JpsimumuSelSeq.outputLocation()]
Bs2JpsimumuTuple.Decay    =  Bs2JpsimumuDecay
Bs2JpsimumuTuple.Branches =  Bs2JpsimumuBranches
fillTuple( Bs2JpsimumuTuple, myTriggerList, "Bs" )


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Bc2JpsimunuFilters = LoKi_Filters (
     STRIP_Code = """
                  HLT_PASS('StrippingBc2EtacMu_KsKpiLineDecision')
                  """
    )


year = "2017"
from Configurables import CondDB
DaVinci().EventPreFilters = Bc2JpsimunuFilters.filters('Bc2JpsimunuFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = False
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [BcSelSeq.sequence(), Bc2JpsimunuTuple, Bs2JpsimumuSelSeq.sequence(), Bs2JpsimumuTuple]       # The algorithms

DaVinci().InputType = "DST"
DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20190206-3"
LHCbApp().CondDBtag = "cond-20191004-2"

# # 2017:
# from GaudiConf import IOHelper
# IOHelper().inputFiles(['../Ppbar_newStripping/00104246_00000529_1.charmcompleteevent.dst'], clear=True)
