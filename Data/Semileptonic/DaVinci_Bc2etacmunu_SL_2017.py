from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles, FilterInTrees
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)


def fillTuple( tuple, myTriggerList, tupleType ):

    from Configurables import TupleToolVtxIsoln, TupleToolIsolationTwoBody, TupleToolDira, TupleToolDocas, TupleToolTauMuDiscrVars, TupleToolSLTools
    # TupleToolmuTrackIsolationSaveTracks

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                      "TupleToolVtxIsoln",
                      "TupleToolIsolationTwoBody",
                      "TupleToolDira"
                      # "TupleToolmuTrackIsolationSaveTracks"
                     ]




    if tupleType=="Bc":
      from Configurables import TupleToolNeutrinoReco
      tuple.addTool( TupleToolNeutrinoReco,name = "TupleToolNeutrinoReco" )
      tuple.TupleToolNeutrinoReco.Verbose = True
      tuple.TupleToolNeutrinoReco.MotherMass=6274.9 #MeV
      tuple.TupleToolNeutrinoReco.ExtraName='mBc'

    from Configurables import TupleToolTISTOS, TupleToolDecay

    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTupleTool(TupleToolDecay, name = 'Bc')
    tuple.Bc.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBc" ]
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBc" )
    tuple.Bc.TupleToolTISTOSForBc.Verbose=True
    tuple.Bc.TupleToolTISTOSForBc.TriggerList = myTriggerList


    tuple.Bc.addTupleTool("TupleToolSLTools")
    tuple.Bc.addTupleTool("TupleToolTauMuDiscrVars")


    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    LoKi_Bc.Preambulo = [
      "MASSES = LoKi.AuxDTFBase.MASSES",
      "massEtac = MASSES() ",
      "massEtac['J/psi(1S)'] = 2.9836 * GeV"
      ]

    varsB = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        # "m_scaled"             : "DTF_FUN ( M , False )",
        "DOCA"                 : "DOCA(1,2)",
        "m_pv"     : "DTF_FUN ( M , True )",
        "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"  : "DTF_CHI2NDOF( True  )",
        "DTFEtac_M": 'DTF_FUN(M , False, strings( [ "J/psi(1S)"] ) , massEtac)'
      }


    LoKi_Bc.Variables = varsB
    tuple.Bc.addTupleTool(LoKi_Bc)

    from Configurables import TupleToolDecayTreeFitter
    tuple.Bc.ToolList += [ "TupleToolDecayTreeFitter/DTFJpsi"]
    tuple.Bc.addTool(TupleToolDecayTreeFitter("DTFJpsi"))
    tuple.Bc.DTFJpsi.Verbose=True
    tuple.Bc.DTFJpsi.daughtersToConstrain=["J/psi(1S)"]
    tuple.Bc.DTFJpsi.constrainToOriginVertex=False


    if tupleType=="Bs":
      tuple.addTupleTool(TupleToolDecay, name = 'Bs')
      tuple.Bs.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBs" ]
      tuple.Bs.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBs" )
      tuple.Bs.TupleToolTISTOSForBs.Verbose=True
      tuple.Bs.TupleToolTISTOSForBs.TriggerList = myTriggerList

      LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bc")
      LoKi_Bs.Variables = varsB
      tuple.Bs.addTupleTool(LoKi_Bs)


    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTupleTool(LoKi_All)

    if tupleType=="Bc":
        tuple.Bc.addTupleTool( 'TupleToolSubMass' )
        tuple.Bc.TupleToolSubMass.Substitution += ["p+ => pi+"]
        tuple.Bc.TupleToolSubMass.Substitution += ["p+ => K+"]
        tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
        tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
        tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
        tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]
        tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => pi+"]
        tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => e+"]
        tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => K+"]
        tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => p+"]
    if tupleType=="Bs":
        tuple.Bs.addTupleTool( 'TupleToolSubMass' )
        tuple.Bs.TupleToolSubMass.Substitution += ["p+ => pi+"]
        tuple.Bs.TupleToolSubMass.Substitution += ["p+ => K+"]
        tuple.Bs.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
        tuple.Bs.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
        tuple.Bs.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
        tuple.Bs.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]
        tuple.Bs.TupleToolSubMass.Substitution += ["mu+ => pi+"]
        tuple.Bs.TupleToolSubMass.Substitution += ["mu+ => e+"]
        tuple.Bs.TupleToolSubMass.Substitution += ["mu+ => K+"]
        tuple.Bs.TupleToolSubMass.Substitution += ["mu+ => p+"]





myTriggerList = [
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",

                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1SingleMuonNoIPDecision",
                 "Hlt1DiMuonNoIPDecision"

                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TrackAllL0Decision",

                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",

                 "Hlt2PhiIncPhiDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision",

                 "Hlt2Bc2JpsiXTFBc2JpsiMuXDecision",
                 "Hlt2Bc2JpsiXBc2JpsiHDecision",

                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",

                 "Hlt2SingleMuonDecision",
                 "Hlt2SingleMuonRareDecision",
                 "Hlt2SingleMuonVHighPTDecision",

                 "Hlt2XcMuXForTauB2XcMuDecision"
                 ]



from PhysSelPython.Wrappers import MergedSelection
inputData = MergedSelection("inputData",
            RequiredSelections=[
            AutomaticData("/Event/Semileptonic/Phys/B2PPbarMuForTauMuLine/Particles"),
            AutomaticData("/Event/Semileptonic/Phys/B2PPbarMuForTauMuTopoLine/Particles")])

JpsiFromStr = FilterInTrees("JpsiFromStr",Code="('J/psi(1S)' == ABSID)")
Jpsi = Selection("Jpsi",Algorithm=JpsiFromStr,
                  RequiredSelections=[inputData])

MuFromStr = FilterInTrees("MuFromStr",Code="('mu-' == ABSID)")
Mu = Selection("Mu",Algorithm=MuFromStr,
                  RequiredSelections=[inputData])

MakeBc = CombineParticles('MakeBc',
                      DecayDescriptor='[B- -> J/psi(1S) mu-]cc',
                      DaughtersCuts={"J/psi(1S)": "(M > 2700)"},
                      CombinationCut="(AM < 50000) & (AM > 2600)",
                      MotherCut="ALL")
BcSel = Selection(MakeBc.getName() + 'Selection',
                  Algorithm=MakeBc,
                  RequiredSelections=[Mu, Jpsi])
BcSelSeq = SelectionSequence(BcSel.name() + 'Sequence', TopSelection=BcSel)


MakeBcMu = CombineParticles('MakeBc',
                      DecayDescriptor='[B- -> J/psi(1S) mu-]cc',
                      DaughtersCuts={"J/psi(1S)": "(M > 2700)"},
                      CombinationCut="(AM < 50000) & (AM > 2600)",
                      MotherCut="ALL")

Bc2JpsimunuDecay = "[B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-]CC"
Bc2JpsimunuBranches = {
     "Bc"       :  "[B- -> (J/psi(1S) -> p+ p~-) mu-]CC",
     "Jpsi"     :  "[B- ->^(J/psi(1S) -> p+ p~-) mu-]CC",
     "ProtonP"  :  "[B- -> (J/psi(1S) ->^p+ p~-) mu-]CC",
     "ProtonM"  :  "[B- -> (J/psi(1S) -> p+^p~-) mu-]CC",
     "Mu"       :  "[B- -> (J/psi(1S) -> p+ p~-)^mu-]CC"
}

Bc2JpsimunuTuple = DecayTreeTuple('Bc2JpsimunuTuple')
Bc2JpsimunuTuple.Inputs   = [BcSelSeq.outputLocation()]
Bc2JpsimunuTuple.Decay    =  Bc2JpsimunuDecay
Bc2JpsimunuTuple.Branches =  Bc2JpsimunuBranches
fillTuple( Bc2JpsimunuTuple, myTriggerList, "Bc" )

#   ======== ======== ======== ======== ======== ======== ======== ======== ========
AddMuonCut   = "(PROBNNmu > 0.1) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)"
AddProtonCut = "((PROBNNp > 0.2) | (PROBNNk > 0.2)) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (P>3000) & (PT>400)"
AddKaonCut   = "(PROBNNk > 0.2) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (P>3000) & (PT>250)"
AddPionCut   = "(PROBNNpi > 0.2) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5) & (P>3000) & (PT>250)"
BsCut        = "(VFASPF(VCHI2/VDOF)<9.0) & (BPVDIRA>0.)"


from StandardParticles import StdAllNoPIDsMuons, StdAllNoPIDsProtons
from PhysConf.Selections import CombineSelection
Bs2JpsimumuSel = CombineSelection(
        'Bs2JpsimumuSel',
        [BcSel, StdAllNoPIDsMuons],
        DecayDescriptor="[B_s0 -> B- mu+]cc",
        DaughtersCuts = {
           "mu+" : AddMuonCut,
           "B-"  : "(BPVCORRM>3500)",
        },
        CombinationCut = "(AM > 3000)",
        MotherCut      = BsCut
    )
Bs2JpsimumuSelSeq = SelectionSequence('Bs2JpsimumuSequence', TopSelection=Bs2JpsimumuSel)
Bs2JpsimumuDecay = "[B_s0 -> ^(B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-) ^mu+]CC"
Bs2JpsimumuBranches = {
     "Bs"       :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-) mu-) mu+]CC",
     "Bc"       :  "[B_s0 ->^(B- -> (J/psi(1S) -> p+ p~-) mu-) mu+]CC",
     "Jpsi"     :  "[B_s0 -> (B- ->^(J/psi(1S) -> p+ p~-) mu-) mu+]CC",
     "ProtonP"  :  "[B_s0 -> (B- -> (J/psi(1S) ->^p+ p~-) mu-) mu+]CC",
     "ProtonM"  :  "[B_s0 -> (B- -> (J/psi(1S) -> p+^p~-) mu-) mu+]CC",
     "Mu"       :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-)^mu-) mu+]CC",
     "MuAdd"    :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-) mu-)^mu+]CC"
}
Bs2JpsimumuTuple = DecayTreeTuple('Bs2JpsimumuTuple')
Bs2JpsimumuTuple.Inputs   = [Bs2JpsimumuSelSeq.outputLocation()]
Bs2JpsimumuTuple.Decay    =  Bs2JpsimumuDecay
Bs2JpsimumuTuple.Branches =  Bs2JpsimumuBranches
fillTuple( Bs2JpsimumuTuple, myTriggerList, "Bs" )



Bs2JpsiPMuSel = CombineSelection(
        'Bs2JpsiPMuSel',
        [BcSel, StdAllNoPIDsProtons],
        DecayDescriptor="[B_s0 -> B- p+]cc",
        DaughtersCuts = {
           "p+" : AddProtonCut,
           "B-"  : "(BPVCORRM>3500)",
        },
        CombinationCut="(AM > 3000)",
        MotherCut = BsCut
    )
Bs2JpsiPMuSelSeq = SelectionSequence('Bs2JpsiPMuSequence', TopSelection=Bs2JpsiPMuSel)
Bs2JpsiPMuDecay = "[B_s0 -> ^(B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-) ^p+]CC"
Bs2JpsiPMuBranches = {
     "Bs"       :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-) mu-) p+]CC",
     "Bc"       :  "[B_s0 ->^(B- -> (J/psi(1S) -> p+ p~-) mu-) p+]CC",
     "Jpsi"     :  "[B_s0 -> (B- ->^(J/psi(1S) -> p+ p~-) mu-) p+]CC",
     "ProtonP"  :  "[B_s0 -> (B- -> (J/psi(1S) ->^p+ p~-) mu-) p+]CC",
     "ProtonM"  :  "[B_s0 -> (B- -> (J/psi(1S) -> p+^p~-) mu-) p+]CC",
     "Mu"       :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-)^mu-) p+]CC",
     "PAdd"    :  "[B_s0 -> (B- -> (J/psi(1S) -> p+ p~-) mu-)^p+]CC"
}
Bs2JpsiPMuTuple = DecayTreeTuple('Bs2JpsiPMuTuple')
Bs2JpsiPMuTuple.Inputs   = [Bs2JpsiPMuSelSeq.outputLocation()]
Bs2JpsiPMuTuple.Decay    =  Bs2JpsiPMuDecay
Bs2JpsiPMuTuple.Branches =  Bs2JpsiPMuBranches
fillTuple( Bs2JpsiPMuTuple, myTriggerList, "Bs" )


Bs2JpsiPMuMuSel = CombineSelection(
        'Bs2JpsiPMuMuSel',
        [BcSel, StdAllNoPIDsMuons, StdAllNoPIDsProtons],
        DecayDescriptor="[B_c+ -> B- p+ mu+]cc",
        DaughtersCuts = {
           "mu+" : AddMuonCut,
           "p+"  : AddProtonCut,
           "B-"  : "(BPVCORRM>3500)",
        },
        CombinationCut="(AM > 3000)",
        MotherCut = BsCut
    )
Bs2JpsiPMuMuSelSeq = SelectionSequence('Bs2JpsiPMuMuSequence', TopSelection=Bs2JpsiPMuMuSel)
Bs2JpsiPMuMuDecay = "[B_c+ -> ^(B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-) ^p+ ^mu+]CC"
Bs2JpsiPMuMuBranches = {
     "Bs"       :  "[B_c+ -> (B- -> (J/psi(1S) -> p+ p~-) mu-) p+ mu+]CC",
     "Bc"       :  "[B_c+ ->^(B- -> (J/psi(1S) -> p+ p~-) mu-) p+ mu+]CC",
     "Jpsi"     :  "[B_c+ -> (B- ->^(J/psi(1S) -> p+ p~-) mu-) p+ mu+]CC",
     "ProtonP"  :  "[B_c+ -> (B- -> (J/psi(1S) ->^p+ p~-) mu-) p+ mu+]CC",
     "ProtonM"  :  "[B_c+ -> (B- -> (J/psi(1S) -> p+^p~-) mu-) p+ mu+]CC",
     "Mu"       :  "[B_c+ -> (B- -> (J/psi(1S) -> p+ p~-)^mu-) p+ mu+]CC",
     "PAdd"     :  "[B_c+ -> (B- -> (J/psi(1S) -> p+ p~-) mu-)^p+ mu+]CC",
     "MuAdd"    :  "[B_c+ -> (B- -> (J/psi(1S) -> p+ p~-) mu-) p+ ^mu+]CC",
}
Bs2JpsiPMuMuTuple = DecayTreeTuple('Bs2JpsiPMuMuTuple')
Bs2JpsiPMuMuTuple.Inputs   = [Bs2JpsiPMuMuSelSeq.outputLocation()]
Bs2JpsiPMuMuTuple.Decay    =  Bs2JpsiPMuMuDecay
Bs2JpsiPMuMuTuple.Branches =  Bs2JpsiPMuMuBranches
fillTuple( Bs2JpsiPMuMuTuple, myTriggerList, "Bs" )


Bs2JpsiPMuMuSSSel = CombineSelection(
        'Bs2JpsiPMuMuSSSel',
        [BcSel, StdAllNoPIDsMuons, StdAllNoPIDsProtons],
        DecayDescriptor="[B_c- -> B- p~- mu+]cc",
        DaughtersCuts = {
           "mu+" : AddMuonCut,
           "p~-"  : AddProtonCut,
           "B-"  : "(BPVCORRM>3500)",
        },
        CombinationCut="(AM > 3000)",
        MotherCut = BsCut
    )
Bs2JpsiPMuMuSSSelSeq = SelectionSequence('Bs2JpsiPMuMuSSSequence', TopSelection=Bs2JpsiPMuMuSSSel)
Bs2JpsiPMuMuSSDecay = "[B_c- -> ^(B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-) ^p~- ^mu+]CC"
Bs2JpsiPMuMuSSBranches = {
     "Bs"       :  "[B_c- -> (B- -> (J/psi(1S) -> p+ p~-) mu-) p~- mu+]CC",
     "Bc"       :  "[B_c- ->^(B- -> (J/psi(1S) -> p+ p~-) mu-) p~- mu+]CC",
     "Jpsi"     :  "[B_c- -> (B- ->^(J/psi(1S) -> p+ p~-) mu-) p~- mu+]CC",
     "ProtonP"  :  "[B_c- -> (B- -> (J/psi(1S) ->^p+ p~-) mu-) p~- mu+]CC",
     "ProtonM"  :  "[B_c- -> (B- -> (J/psi(1S) -> p+^p~-) mu-) p~- mu+]CC",
     "Mu"       :  "[B_c- -> (B- -> (J/psi(1S) -> p+ p~-)^mu-) p~- mu+]CC",
     "PAdd"     :  "[B_c- -> (B- -> (J/psi(1S) -> p+ p~-) mu-)^p~- mu+]CC",
     "MuAdd"    :  "[B_c- -> (B- -> (J/psi(1S) -> p+ p~-) mu-) p~- ^mu+]CC",
}
Bs2JpsiPMuMuSSTuple = DecayTreeTuple('Bs2JpsiPMuMuSSTuple')
Bs2JpsiPMuMuSSTuple.Inputs   = [Bs2JpsiPMuMuSSSelSeq.outputLocation()]
Bs2JpsiPMuMuSSTuple.Decay    =  Bs2JpsiPMuMuSSDecay
Bs2JpsiPMuMuSSTuple.Branches =  Bs2JpsiPMuMuSSBranches
fillTuple( Bs2JpsiPMuMuSSTuple, myTriggerList, "Bs" )

algs  = [BcSelSeq.sequence(), Bc2JpsimunuTuple]
algs += [Bs2JpsimumuSelSeq.sequence(),  Bs2JpsimumuTuple]
algs += [Bs2JpsiPMuSelSeq.sequence(),   Bs2JpsiPMuTuple]
algs += [Bs2JpsiPMuMuSelSeq.sequence(), Bs2JpsiPMuMuTuple]
algs += [Bs2JpsiPMuMuSSSelSeq.sequence(), Bs2JpsiPMuMuSSTuple]
#   ======== ======== ======== ======== ======== ======== ======== ======== ========


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Bc2JpsimunuFilters = LoKi_Filters (
     STRIP_Code = """
                  HLT_PASS('StrippingB2PPbarMuForTauMuLineDecision') |
                  HLT_PASS('StrippingB2PPbarMuForTauMuTopoLineDecision')
                  """
    )


year = "2017"
from Configurables import CondDB
DaVinci().EventPreFilters = Bc2JpsimunuFilters.filters('Bc2JpsimunuFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = False
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = algs       # The algorithms

DaVinci().InputType = "DST"
DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "cond-20170724"

# # 2017:
# from GaudiConf import IOHelper
# IOHelper().inputFiles(['00069593_00000152_1.semileptonic.dst'], clear=True)
