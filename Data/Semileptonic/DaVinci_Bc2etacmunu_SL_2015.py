from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles, FilterInTrees
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)


def fillTuple( tuple, myTriggerList, tupleType ):

    from Configurables import TupleToolVtxIsoln, TupleToolIsolationTwoBody, TupleToolCorrectedMass, TupleToolDira, TupleToolSLTools, TupleToolmuTrackIsolationSaveTracks

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                      "TupleToolVtxIsoln",
                      "TupleToolIsolationTwoBody",
                      "TupleToolCorrectedMass",
                      "TupleToolDira",
                      "TupleToolSLTools",
                      "TupleToolmuTrackIsolationSaveTracks"
                     ]


    if tupleType=="Bc":
      from Configurables import TupleToolNeutrinoReco
      tuple.addTool( TupleToolNeutrinoReco,name = "TupleToolNeutrinoReco" )
      tuple.TupleToolNeutrinoReco.Verbose = True
      tuple.TupleToolNeutrinoReco.MotherMass=6274.9 #MeV
      tuple.TupleToolNeutrinoReco.ExtraName='mBc'

    from Configurables import TupleToolTISTOS, TupleToolDecay

    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      # "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTupleTool(TupleToolDecay, name = 'Bc')
    tuple.Bc.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBc" ]
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBc" )
    tuple.Bc.TupleToolTISTOSForBc.Verbose=True
    tuple.Bc.TupleToolTISTOSForBc.TriggerList = myTriggerList

    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    LoKi_Bc.Preambulo = [
      "MASSES = LoKi.AuxDTFBase.MASSES",
      "massEtac = MASSES() ",
      "massEtac['J/psi(1S)'] = 2.9836 * GeV"
      ]

    varsB = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        # "m_scaled"             : "DTF_FUN ( M , False )",
        "DOCA"                 : "DOCA(1,2)",
        "m_pv"     : "DTF_FUN ( M , True )",
        "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"  : "DTF_CHI2NDOF( True  )",
        "DTFEtac_M": 'DTF_FUN(M , False, strings( [ "J/psi(1S)"] ) , massEtac)'
      }

    LoKi_Bc.Variables = varsB
    tuple.Bc.addTupleTool(LoKi_Bc)

    from Configurables import TupleToolDecayTreeFitter
    tuple.Bc.ToolList += [ "TupleToolDecayTreeFitter/DTFJpsi"]
    tuple.Bc.addTool(TupleToolDecayTreeFitter("DTFJpsi"))
    tuple.Bc.DTFJpsi.Verbose=True
    tuple.Bc.DTFJpsi.daughtersToConstrain=["J/psi(1S)"]
    tuple.Bc.DTFJpsi.constrainToOriginVertex=False

    if tupleType=="Bs":
      tuple.addTupleTool(TupleToolDecay, name = 'Bs')
      tuple.Bs.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBs" ]
      tuple.Bs.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBs" )
      tuple.Bs.TupleToolTISTOSForBs.Verbose=True
      tuple.Bs.TupleToolTISTOSForBs.TriggerList = myTriggerList

      LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bc")
      LoKi_Bs.Variables = varsB
      tuple.Bs.addTupleTool(LoKi_Bs)

    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)

    tuple.Jpsi.addTupleTool( 'TupleToolSubMass' )
    tuple.Jpsi.ToolList += [ "TupleToolSubMass" ]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => pi+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => K+"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]





myTriggerList = [
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",

                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1SingleMuonNoIPDecision",
                 "Hlt1DiMuonNoIPDecision"

                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TrackAllL0Decision",

                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",

                 "Hlt2PhiIncPhiDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision",

                 "Hlt2Bc2JpsiXTFBc2JpsiMuXDecision",
                 "Hlt2Bc2JpsiXBc2JpsiHDecision",

                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",

                 "Hlt2SingleMuonDecision",
                 "Hlt2SingleMuonRareDecision",
                 "Hlt2SingleMuonVHighPTDecision",

                 "Hlt2XcMuXForTauB2XcMuDecision"
                 ]




inputData = AutomaticData("/Event/Semileptonic/Phys/B2PPbarMuForTauMuLine/Particles")

JpsiFromStr = FilterInTrees("JpsiFromStr",Code="('J/psi(1S)' == ABSID)")
Jpsi = Selection("Jpsi",Algorithm=JpsiFromStr,
                  RequiredSelections=[inputData])

MuFromStr = FilterInTrees("MuFromStr",Code="('mu-' == ABSID)")
Mu = Selection("Mu",Algorithm=MuFromStr,
                  RequiredSelections=[inputData])

MakeBc = CombineParticles('MakeBc',
                      DecayDescriptor='[B- -> J/psi(1S) mu-]cc',
                      DaughtersCuts={"J/psi(1S)": "(M > 2700)"},
                      CombinationCut="(AM < 50000) & (AM > 2600)",
                      MotherCut="ALL")
BcSel = Selection(MakeBc.getName() + 'Selection',
                  Algorithm=MakeBc,
                  RequiredSelections=[Mu, Jpsi])
BcSelSeq = SelectionSequence(BcSel.name() + 'Sequence', TopSelection=BcSel)


Bc2JpsimunuDecay = "[B- ->^(J/psi(1S) -> ^p+ ^p~-) ^mu-]CC"
Bc2JpsimunuBranches = {
     "Bc"       :  "[B- -> (J/psi(1S) -> p+ p~-) mu-]CC",
     "Jpsi"     :  "[B- ->^(J/psi(1S) -> p+ p~-) mu-]CC",
     "ProtonP"  :  "[B- -> (J/psi(1S) ->^p+ p~-) mu-]CC",
     "ProtonM"  :  "[B- -> (J/psi(1S) -> p+^p~-) mu-]CC",
     "Mu"       :  "[B- -> (J/psi(1S) -> p+ p~-)^mu-]CC"
}

Bc2JpsimunuTuple = DecayTreeTuple('Bc2JpsimunuTuple')
Bc2JpsimunuTuple.Inputs   = [BcSelSeq.outputLocation()]
Bc2JpsimunuTuple.Decay    =  Bc2JpsimunuDecay
Bc2JpsimunuTuple.Branches =  Bc2JpsimunuBranches
fillTuple( Bc2JpsimunuTuple, myTriggerList, "Bc" )


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Bc2JpsimunuFilters = LoKi_Filters (
     STRIP_Code = """
                  HLT_PASS('StrippingB2PPbarMuForTauMuLineDecision') ||  HLT_PASS('StrippingB2PPbarMuForTauMuTopoLineDecision')
                  """
    )


year = "2015"
from Configurables import CondDB
DaVinci().EventPreFilters = Bc2JpsimunuFilters.filters('Bc2JpsimunuFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = False
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [BcSelSeq.sequence(), Bc2JpsimunuTuple]       # The algorithms

DaVinci().InputType = "DST"

DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


# # 2015:
# from GaudiConf import IOHelper
# IOHelper().inputFiles(['00069593_00000152_1.semileptonic.dst'], clear=True)
