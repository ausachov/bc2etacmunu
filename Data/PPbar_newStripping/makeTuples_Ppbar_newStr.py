import sys
print(sys.argv)

import argparse



def makeSingleTuple(year,polarity):

   myJobName = 'Bc2Jpsimunu_Ppbar_newStr_Mag'+polarity+'_'+year

   platform = 'x86_64-centos7-gcc62-opt'
   # if year=='2015':
   #   year_short = '15'
   #   reco       = '15a'
   #   strip      = 'Stripping24r1'
   #   davinciVer = 'v42r6p1'
   # elif year=='2016':
   #   year_short = '16'
   #   reco       = '16'
   #   strip      = 'Stripping28r1'
   #   davinciVer = 'v41r4p4'
   if year=='2017':
     year_short = '17'
     reco       = '17'
     strip      = 'Stripping29r2p1'
     davinciVer = 'v42r9p2'
   # elif year=='2018':
   #   year_short = '18'
   #   reco       = '18'
   #   strip      = 'Stripping34'
   #   davinciVer = 'v44r9'
   #   platform   = 'x86_64-slc6-gcc62-opt'

   davinciDir = "/afs/cern.ch/user/a/ausachov/cmtuser/dv4bc/DaVinciDev_"+davinciVer
   import os
   if os.path.exists(davinciDir):
     myApplication = GaudiExec()
     myApplication.directory = davinciDir
   else:
     myApplication = prepareGaudiExec('DaVinci',davinciVer, myPath='$HOME/cmtuser/dv4bc')
   myApplication.platform = platform
   myApplication.options = ['DaVinci_Bc2etacmunu_newStr_'+year+'.py']

   data  = BKQuery('/LHCb/Collision'+year_short+'/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco'+reco+'/'+strip+'/90000000/CHARMCOMPLETEEVENT.DST', dqflag=['OK']).getDataset()

   validData  = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().items() if len(rep)])
   mySplitter = SplitByFiles( filesPerJob = 40, maxFiles = -1, ignoremissing = True, bulksubmit = False )

   myBackend = Dirac()
   j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ DiracFile('Tuple.root'),
                          DiracFile('DVHistos.root')],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
   queues.add(j.submit)
   print('\n\n\n')



years      = ['2017']
polarities = ['Down','Up']

for year in years:
  for polarity in polarities:
     print('Bc2Jpsimunu newStr for ', year, polarity, ' submitting below! \n')
     makeSingleTuple(year=year,polarity=polarity)
