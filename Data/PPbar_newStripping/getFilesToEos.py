import sys
import argparse


import os
dir = "/eos/user/a/ausachov/Data_Bc2etacmunu/PPbar_newStripping/"
if not os.path.exists(dir):
    os.makedirs(dir)


years = ["2017"]

for year in years:
    yearDir = dir + year
    if not os.path.exists(yearDir):
        os.makedirs(yearDir)
    if not os.path.exists(yearDir+"/MagUp"):
        os.makedirs(yearDir+"/MagUp")
    if not os.path.exists(yearDir+"/MagDown"):
        os.makedirs(yearDir+"/MagDown")



istart = 1866
iend = 1867
for ii in range(istart, iend+1):
    try:
        ijob = jobs(ii)
        name = ijob.name
        print("Job {} : downloading tuples".format(ii))
        print(name)
        strs = name.split("_")

        polarity = strs[-2]
        year     = strs[-1]
        polDir = dir+year+"/"+polarity

        print(polDir)
        for sj in ijob.subjobs:
            sjid = sj.id
            idDir = "{0}/{1}".format(polDir,sjid)
            print(idDir)
            if not os.path.exists(idDir):
                os.makedirs(idDir)
            try:
                sj.outputfiles[0].localDir = idDir
                sj.outputfiles[0].get()
            except:
                pass
    except:
        pass
