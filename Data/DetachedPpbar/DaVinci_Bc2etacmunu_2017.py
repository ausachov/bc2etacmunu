from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)


def fillTuple( tuple, myTriggerList, tupleType ):

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                     ]


    if tupleType=="Bc":
      tuple.ToolList += [ "TupleToolNeutrinoReco" ]
      from Configurables import TupleToolNeutrinoReco
      tuple.addTool( TupleToolNeutrinoReco,name = "TupleToolNeutrinoReco" )
      tuple.TupleToolNeutrinoReco.Verbose = True
      tuple.TupleToolNeutrinoReco.MotherMass=6274.9 #MeV
      tuple.TupleToolNeutrinoReco.ExtraName='mBc'

    from Configurables import TupleToolTISTOS, TupleToolDecay

    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      # "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    varsB = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        # "m_scaled"             : "DTF_FUN ( M , False )",
        "DOCA"                 : "DOCA(1,2)",
        "m_pv"     : "DTF_FUN ( M , True )",
        "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
      }

    tuple.addTupleTool(TupleToolDecay, name = 'Bc')
    tuple.Bc.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBc" ]
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBc" )
    tuple.Bc.TupleToolTISTOSForBc.Verbose=True
    tuple.Bc.TupleToolTISTOSForBc.TriggerList = myTriggerList

    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    LoKi_Bc.Variables = varsB
    tuple.Bc.addTupleTool(LoKi_Bc)

    if tupleType=="Bs":
      tuple.addTupleTool(TupleToolDecay, name = 'Bs')
      tuple.Bs.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBs" ]
      tuple.Bs.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBs" )
      tuple.Bs.TupleToolTISTOSForBs.Verbose=True
      tuple.Bs.TupleToolTISTOSForBs.TriggerList = myTriggerList

      LoKi_Bs=LoKi__Hybrid__TupleTool("LoKi_Bc")
      LoKi_Bs.Variables = varsB
      tuple.Bs.addTupleTool(LoKi_Bs)

    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)

    tuple.Jpsi.addTupleTool( 'TupleToolSubMass' )
    tuple.Jpsi.ToolList += [ "TupleToolSubMass" ]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => pi+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => K+"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]



myTriggerList = [
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",

                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1SingleMuonNoIPDecision",
                 "Hlt1DiMuonNoIPDecision"

                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TrackAllL0Decision",

                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",

                 "Hlt2PhiIncPhiDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision",

                 "Hlt2Bc2JpsiXTFBc2JpsiMuXDecision",
                 "Hlt2Bc2JpsiXBc2JpsiHDecision",

                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",

                 "Hlt2SingleMuonDecision",
                 "Hlt2SingleMuonRareDecision",
                 "Hlt2SingleMuonVHighPTDecision"
                 ]




MakeBc = CombineParticles("MakeBc")
MakeBc.DecayDescriptor =  "[B_c+ -> J/psi(1S) mu+]cc"
MakeBc.MotherCut = ("(VFASPF(VCHI2/VDOF)<16.0)"
                    "& (MINTREE('mu+'==ABSID, PROBNNmu) > 0.1)")
MakeBc.CombinationCut = "(AM < 7000) & (AM > 2600)"
BcSel = Selection(
    MakeBc.getName() + 'Selection',
    Algorithm=MakeBc,
    RequiredSelections=[
        AutomaticData("/Event/CharmCompleteEvent/Phys/Ccbar2PpbarDetachedLine/Particles"),
        AutomaticData("Phys/StdLooseMuons/Particles")
    ]
    )
BcSelSeq = SelectionSequence(BcSel.name() + 'Sequence', TopSelection=BcSel)

Bc2JpsimunuDecay = "[B_c+ ->^(J/psi(1S) -> ^p+ ^p~-) ^mu+]CC"
Bc2JpsimunuBranches = {
     "Bc"       :  "[B_c+ -> (J/psi(1S) -> p+ p~-) mu+]CC",
     "Jpsi"     :  "[B_c+ ->^(J/psi(1S) -> p+ p~-) mu+]CC",
     "ProtonP"  :  "[B_c+ -> (J/psi(1S) ->^p+ p~-) mu+]CC",
     "ProtonM"  :  "[B_c+ -> (J/psi(1S) -> p+^p~-) mu+]CC",
     "Mu"       :  "[B_c+ -> (J/psi(1S) -> p+ p~-)^mu+]CC"
}

Bc2JpsimunuTuple = DecayTreeTuple('Bc2JpsimunuTuple')
Bc2JpsimunuTuple.Inputs   = [BcSelSeq.outputLocation()]
Bc2JpsimunuTuple.Decay    =  Bc2JpsimunuDecay
Bc2JpsimunuTuple.Branches =  Bc2JpsimunuBranches
fillTuple( Bc2JpsimunuTuple, myTriggerList, "Bc" )







MakeBs2Jpsimumu = CombineParticles("MakeBs2Jpsimumu")
MakeBs2Jpsimumu.DecayDescriptor =  "[B_s0 -> B_c+ mu-]cc"
MakeBs2Jpsimumu.MotherCut = ("(VFASPF(VCHI2/VDOF)<16.0)")
MakeBs2Jpsimumu.CombinationCut = "(AM < 6000) & (AM > 3000)"
Bs2JpsimumuSel = Selection(
    MakeBs2Jpsimumu.getName() + 'Selection',
    Algorithm=MakeBs2Jpsimumu,
    RequiredSelections=[BcSel, AutomaticData("Phys/StdLooseMuons/Particles")]
    )
Bs2JpsimumuSelSeq = SelectionSequence(Bs2JpsimumuSel.name() + 'Sequence', TopSelection=Bs2JpsimumuSel)

Bs2JpsimumuDecay = "[B_s0 -> ^(B_c+ ->^(J/psi(1S) -> ^p+ ^p~-) ^mu+) ^mu-]CC"
Bs2JpsimumuBranches = {
     "Bs"       :  "[B_s0 -> (B_c+ -> (J/psi(1S) -> p+ p~-) mu+) mu-]CC",
     "Bc"       :  "[B_s0 ->^(B_c+ -> (J/psi(1S) -> p+ p~-) mu+) mu-]CC",
     "Jpsi"     :  "[B_s0 -> (B_c+ ->^(J/psi(1S) -> p+ p~-) mu+) mu-]CC",
     "ProtonP"  :  "[B_s0 -> (B_c+ -> (J/psi(1S) ->^p+ p~-) mu+) mu-]CC",
     "ProtonM"  :  "[B_s0 -> (B_c+ -> (J/psi(1S) -> p+^p~-) mu+) mu-]CC",
     "MuP"      :  "[B_s0 -> (B_c+ -> (J/psi(1S) -> p+ p~-)^mu+) mu-]CC",
     "MuM"      :  "[B_s0 -> (B_c+ -> (J/psi(1S) -> p+ p~-) mu+)^mu-]CC"
}
Bs2JpsimumuTuple = DecayTreeTuple('Bs2JpsimumuTuple')
Bs2JpsimumuTuple.Inputs   = [Bs2JpsimumuSelSeq.outputLocation()]
Bs2JpsimumuTuple.Decay    =  Bs2JpsimumuDecay
Bs2JpsimumuTuple.Branches =  Bs2JpsimumuBranches
fillTuple( Bs2JpsimumuTuple, myTriggerList, "Bs" )








"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Bc2JpsimunuFilters = LoKi_Filters (
     STRIP_Code = """
                  HLT_PASS('StrippingCcbar2PpbarDetachedLineDecision')
                  """
    )


year = "2017"
from Configurables import CondDB
DaVinci().EventPreFilters = Bc2JpsimunuFilters.filters('Bc2JpsimunuFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = False
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [BcSelSeq.sequence(), Bc2JpsimunuTuple,
                            Bs2JpsimumuSelSeq.sequence(), Bs2JpsimumuTuple]       # The algorithms

DaVinci().InputType = "DST"

DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


# 2017:
from GaudiConf import IOHelper
IOHelper().inputFiles(['00071499_00000086_1.charmcompleteevent.dst'], clear=True)
