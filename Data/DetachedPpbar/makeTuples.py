import sys
print sys.argv

import argparse



def makeSingleTuple(year,polarity):

   myJobName = 'Bc2Jpsimunu_Mag'+polarity+'_'+year

   if year=='2015':
     year_short = '15'
     reco       = '15a'
     strip      = 'Stripping24r1'
     davinciVer = 'v42r6p1' 
   if year=='2016':
     year_short = '16'
     reco       = '16'
     strip      = 'Stripping28r1'
     davinciVer = 'v42r6p1'
   elif year=='2017':
     year_short = '17'
     reco       = '17'
     strip      = 'Stripping29r2'
     davinciVer = 'v42r7p2'

   davinciDir = '/afs/cern.ch/user/a/ausachov/cmtuser/DaVinciDev_'+davinciVer
   import os
   if os.path.exists(davinciDir):
     myApplication = GaudiExec()
     myApplication.directory = davinciDir
   else:
     myApplication = prepareGaudiExec('DaVinci',davinciVer, myPath='$HOME/cmtuser/')



   myApplication.options = ['DaVinci_Bc2etacmunu_'+year+'.py']



   data  = BKQuery('/LHCb/Collision'+year_short+'/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco'+reco+'/'+strip+'/90000000/CHARMCOMPLETEEVENT.DST', dqflag=['OK']).getDataset()


   validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

   mySplitter = SplitByFiles( filesPerJob = 40, maxFiles = -1, ignoremissing = True, bulksubmit = False )

   myBackend = Dirac()
   j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ DiracFile('Tuple.root'),
                          DiracFile('DVHistos.root')],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
   j.submit()
   print '\n\n\n'



years      = ['2015','2016','2017']
polarities = ['Down','Up']

for year in years:
  for polarity in polarities:
     print 'Bc2Jpsimunu for ', year, polarity, ' submitting below! \n'
     makeSingleTuple(year=year,polarity=polarity)
