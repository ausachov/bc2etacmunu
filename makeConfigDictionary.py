minMass = 2850.
maxMass = 3250.


cutsAprioriDetached = "ProtonP_PT>1000 && ProtonM_PT>1000 && \
                       ProtonP_P>10000 && ProtonM_P>10000 && \
                       ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && Mu_TRACK_CHI2NDOF<5 && \
                       ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && Mu_IPCHI2_OWNPV>9 && \
                       Jpsi_ENDVERTEX_CHI2<9 && \
                       Jpsi_FDCHI2_OWNPV>25 && \
                       nSPDHits<600"



cutsDict = {
    "secondaryTopoOpt" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8",
    "secondaryTopoOptProbNNMu02" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && \
                          (Mu_ProbNNmu>0.2)",
    "secondaryTopoOptSoftFD" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>25) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8",
    "secondaryTopoOptSoftPID" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4",
    "secondaryTopoOptMidPID" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.9 && ProtonM_ProbNNp>0.9",
    "secondaryTopoOptTightPID" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.97 && ProtonM_ProbNNp>0.97",
    "secondaryTopoOptTightPIDNoPT" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.97 && ProtonM_ProbNNp>0.97",
    "secondaryTopoOptNoPT" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8",
    "secondaryTopoOptNoPTMu250" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>250)",
    "secondaryTopoOptNoPTMu500" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>500)",
    "secondaryTopoOptNoPTMu250_02" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>250) && Mu_ProbNNmu>0.2",
    "secondaryTopoOptNoPTMu250_04" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>250) && Mu_ProbNNmu>0.4",
    "secondaryTopoOptNoPTMu250_02" : "(Jpsi_PT>3000) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>250) && Mu_ProbNNmu>0.2",
    "secondaryTopoOptMu250_04" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>250) && Mu_ProbNNmu>0.4",
    "secondaryTopoOptMu500" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8 && (Mu_PT>500)",
    "secondaryTopoOptMu250" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && (Mu_PT>250) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8",
    "secondaryTopoOptMu500" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && (Mu_PT>500) && \
                          ProtonP_ProbNNp>0.8 && ProtonM_ProbNNp>0.8",
    "Detached"            : cutsAprioriDetached
    }
