from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
from array import *

gStyle.SetOptStat(000)


Preselection = {
"Base" : "ProtonP_PT>500 && ProtonM_PT>500 && \
           ProtonP_P>10000 && ProtonM_P>10000 && \
           ProtonP_ProbNNp>0.4 && ProtonM_P>0.4 && \
           ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
           ProtonP_IPCHI2_OWNPV>2 && ProtonM_IPCHI2_OWNPV>2 && \
           Jpsi_ENDVERTEX_CHI2<9 && \
           nSPDHits<600",
"Trigger" : "(Bc_L0MuonDecision_TOS ||  Bc_L0HadronDecision_TOS) && \
             (Bc_Hlt1TwoTrackMVADecision_TOS || Bc_Hlt1TrackMVADecision_TOS || Bc_Hlt1SingleMuonNoIPDecision_TOS || Bc_Hlt1TrackMuonDecision_TOS || Bc_Hlt1TrackMuonMVADecision_TOS) && \
             (Bc_Hlt2Topo2BodyDecision_TOS || Bc_Hlt2Topo3BodyDecision_TOS || Bc_Hlt2TopoMu2BodyDecision_TOS || Bc_Hlt2TopoMu3BodyDecision_TOS || Bc_Hlt2SingleMuonDecision_TOS || Bc_Hlt2XcMuXForTauB2XcMuDecision_TOS)",
"Trigger_BcVCHI2NDOFgt4" : "(Bc_L0MuonDecision_TOS ||  Bc_L0HadronDecision_TOS) && \
             (Bc_Hlt1TwoTrackMVADecision_TOS || Bc_Hlt1TrackMVADecision_TOS || Bc_Hlt1SingleMuonNoIPDecision_TOS || Bc_Hlt1TrackMuonDecision_TOS || Bc_Hlt1TrackMuonMVADecision_TOS) && \
             (Bc_Hlt2Topo2BodyDecision_TOS || Bc_Hlt2Topo3BodyDecision_TOS || Bc_Hlt2TopoMu2BodyDecision_TOS || Bc_Hlt2TopoMu3BodyDecision_TOS || Bc_Hlt2SingleMuonDecision_TOS || Bc_Hlt2XcMuXForTauB2XcMuDecision_TOS) && \
             Bc_ENDVERTEX_CHI2>12"
}
#lower cut: P_PT, IPCHI2, FDCHI2 to check stripping cut


from ROOT import RDataFrame
ROOT.EnableImplicitMT()

import os.path
input_files = []
for year in ["2017"]:
    for pol in ["MagUp","MagDown"]:
        for isj in range(1000):
            dir_base = "/eos/user/a/ausachov/Data_Bc2etacmunu/PPbar_newStripping"
            filename = dir_base+"/"+year+"/"+pol+"/"+str(isj)+"/Tuple.root"
            if os.path.isfile(filename):
                input_files.append(filename)
print(input_files)

names = ROOT.std.vector('string')()
for n in input_files: names.push_back(n)

treeName   = "DecayTree"
keys = ["Bc2Jpsimunu","Bs2Jpsimumu","Bs2JpsiPMu","Bs2JpsiPMuMu","Bs2JpsiPMuMuSS"]
data_frames = {}
for key in keys:
    data_frames[key] = RDataFrame(key+"Tuple/"+treeName, names)

cutkeys = ["Trigger","Base"]
keysSum = ""
cut = "Jpsi_M>2600 && Jpsi_M<4000"
for cutkey in cutkeys:
	keysSum = keysSum + cutkey
	cut = cut + " && " + Preselection[cutkey]


out_files = {}
for key in keys:
    dir = "/eos/user/a/ausachov/Data_Bc2etacmunu/PPbar_newStripping/"
    out_files[key] = dir+key+"_selected_"+keysSum +".root"

signalMuonCut = "Mu_TRACK_CHI2NDOF<5 && Mu_IPCHI2_OWNPV>1"
#lower cut: P_PT, IPCHI2, FDCHI2 to check stripping cut

opts = RDF.RSnapshotOptions()
opts.fMode = "RECREATE"
filtered = {}
for key in keys:
    filtered[key] = data_frames[key].Filter(cut + " && " + signalMuonCut)
    filtered[key].Snapshot(treeName, out_files[key], ".*", opts)
