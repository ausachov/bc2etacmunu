from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
from array import *

gStyle.SetOptStat(000)


Preselection = {
"Base" : "ProtonP_PT>500 && ProtonM_PT>500 && \
           ProtonP_P>10000 && ProtonM_P>10000 && \
           ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
           ProtonP_IPCHI2_OWNPV>4 && ProtonM_IPCHI2_OWNPV>4 && \
           Jpsi_ENDVERTEX_CHI2<9 && \
           Jpsi_FDCHI2_OWNPV>16 && \
           nSPDHits<600",
"Trigger" : "(Bc_L0MuonDecision_TOS ||  Bc_L0HadronDecision_TOS) && \
             (Bc_Hlt1TwoTrackMVADecision_TOS || Bc_Hlt1TrackMVADecision_TOS || Bc_Hlt1SingleMuonNoIPDecision_TOS || Bc_Hlt1TrackMuonDecision_TOS || Bc_Hlt1TrackMuonMVADecision_TOS) && \
             (Bc_Hlt2Topo2BodyDecision_TOS || Bc_Hlt2Topo3BodyDecision_TOS || Bc_Hlt2TopoMu2BodyDecision_TOS || Bc_Hlt2TopoMu3BodyDecision_TOS || Bc_Hlt2SingleMuonDecision_TOS || Bc_Hlt2XcMuXForTauB2XcMuDecision_TOS)",
"Trigger_BcVCHI2NDOFgt4" : "(Bc_L0MuonDecision_TOS ||  Bc_L0HadronDecision_TOS) && \
             (Bc_Hlt1TwoTrackMVADecision_TOS || Bc_Hlt1TrackMVADecision_TOS || Bc_Hlt1SingleMuonNoIPDecision_TOS || Bc_Hlt1TrackMuonDecision_TOS || Bc_Hlt1TrackMuonMVADecision_TOS) && \
             (Bc_Hlt2Topo2BodyDecision_TOS || Bc_Hlt2Topo3BodyDecision_TOS || Bc_Hlt2TopoMu2BodyDecision_TOS || Bc_Hlt2TopoMu3BodyDecision_TOS || Bc_Hlt2SingleMuonDecision_TOS || Bc_Hlt2XcMuXForTauB2XcMuDecision_TOS) && \
             Bc_ENDVERTEX_CHI2>12"
}

#lower cut: P_PT, IPCHI2, FDCHI2 to check stripping cut




from ROOT import RDataFrame
ROOT.EnableImplicitMT()


import os.path
input_files = []
for year in ["2016","2017","2018"]:
    for pol in ["MagUp","MagDown"]:
        for isj in range(1000):
            dir_base = "/eos/user/a/ausachov/Data_Bc2etacmunu/Semileptonics_extended"
            filename = dir_base+"/"+year+"/"+pol+"/"+str(isj)+"/Tuple.root"
            if os.path.isfile(filename):
                input_files.append(filename)

print(input_files)

names = ROOT.std.vector('string')()
for n in input_files: names.push_back(n)

Bc_Folder = "Bc2JpsimunuTuple"
Bs_Folder = "Bs2JpsimumuTuple"
treeName   = "DecayTree"

Bc_df = RDataFrame(Bc_Folder+"/"+treeName, names)
Bs_df = RDataFrame(Bs_Folder+"/"+treeName, names)


keys = ["Trigger","Base"]
keysSum = ""
cut = "Jpsi_M>2600 && Jpsi_M<4000"
for key in keys:
	keysSum = keysSum + key
	cut = cut + " && " + Preselection[key]
Bc_out_file   = "/eos/user/a/ausachov/Data_Bc2etacmunu/Semileptonics_extended/Bc_selected_"+keysSum+".root"
Bs_out_file   = "/eos/user/a/ausachov/Data_Bc2etacmunu/Semileptonics_extended/Bs_selected_"+keysSum+".root"

signalMuonCutBc = "Mu_TRACK_CHI2NDOF<5 && Mu_IPCHI2_OWNPV>4"
signalMuonCutBs = "MuP_TRACK_CHI2NDOF<5 && MuP_IPCHI2_OWNPV>4"
#lower cut: P_PT, IPCHI2, FDCHI2 to check stripping cut

opts = RDF.RSnapshotOptions()
opts.fMode = "RECREATE"
Bc_filtered = Bc_df.Filter(cut + " && " + signalMuonCutBc)
Bc_filtered.Snapshot(treeName, Bc_out_file, ".*", opts)
Bs_filtered = Bs_df.Filter(cut + " && " + signalMuonCutBs)
Bs_filtered.Snapshot(treeName, Bs_out_file, ".*", opts)
