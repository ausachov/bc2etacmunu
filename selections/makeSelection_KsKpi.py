from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
from array import *

gStyle.SetOptStat(000)


Preselection = {
"Base" : "Bc_PT>4000 && Bc_MCORR>4000 && ((Bc_ENDVERTEX_CHI2/Bc_ENDVERTEX_NDOF)<4) && \
          K_TRACK_CHI2NDOF<5 && Pi_TRACK_CHI2NDOF<5 && ksPiP_TRACK_CHI2NDOF<5 && ksPiM_TRACK_CHI2NDOF<5 && \
          (Jpsi_ENDVERTEX_CHI2)/(Jpsi_ENDVERTEX_NDOF)<9 && \
          nSPDHits<600",
}
# Mu_TRACK_CHI2NDOF<5


ch = TChain("Bc2JpsimunuTuple/DecayTree")
for year in ["2017"]:
    for pol in ["MagUp","MagDown"]:
        for isj in range(500):
            ch.Add("/eos/user/a/ausachov/Data_Bc2etacmunu/KsKpi/"+year+"/KsKmPip"+pol+"/"+str(isj)+"/Tuple.root")
            ch.Add("/eos/user/a/ausachov/Data_Bc2etacmunu/KsKpi/"+year+"/KsKpPim"+pol+"/"+str(isj)+"/Tuple.root")

cut = "Jpsi_M>2600 && Jpsi_M<4000"
keysSum = ""
keys = ["Base"]
for key in keys:
	keysSum = keysSum + key
	cut = cut + " && " + Preselection[key]



file = TFile("/eos/user/a/ausachov/Data_Bc2etacmunu/KsKpi/selected_"+keysSum+".root","RECREATE")
tree = ch.CopyTree(cut)
tree.GetCurrentFile().Write()
tree.GetCurrentFile().Close()
