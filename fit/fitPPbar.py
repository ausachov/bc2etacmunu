from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
import random

import sys, os
sys.path.insert(0, '../')
from makeConfigDictionary import *

gROOT.Reset()
# gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")
proof = TProof.Open("")





from setupPlot import *


tupleFiles = {}
for key in cutsDict.keys():
    tupleFiles[key] = "/eos/user/a/ausachov/Data_Bc2etacmunu/SL/selected_"+key+".root"


minMcorr = 3300.
maxMcorr = 8000.
stepMcorr = 50.

nsteps = int((maxMcorr-minMcorr)/stepMcorr)


def GetFullDataSet(w=0, cutType="secondaryTopoOpt"):

    Jpsi_M = w.var("Jpsi_M")

    Bc_LOKI_BPVCORRM = RooRealVar("Bc_LOKI_BPVCORRM","Bc_LOKI_BPVCORRM",minMcorr,maxMcorr)

    ch = TChain("DecayTree")
    tupleFile = tupleFiles[cutType]
    ch.Add(tupleFile)
    tree = ch.CopyTree("")
    DataSetFull = RooDataSet("DataSetFull", "DataSetFull", tree, RooArgSet(Jpsi_M, Bc_LOKI_BPVCORRM), "", "")
    getattr(w,'import')(DataSetFull)

    del ch, tree



def getConstPars(SystType='Base', nMCorr=0):

    if(nMCorr==0) : MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__SndTopoOpt_wksp.root"
    elif(nMCorr>0): MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__SndTopoOpt_wksp"+str(nMCorr)+".root"

    f = TFile(MCResolutionFile,"READ")
    wMC = RooWorkspace()
    wMC = f.Get("w")

    ratioNtoW = wMC.var("rNarToW").getValV()  

    if SystType=='rEtaToJpsi':
        ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
    else:
        ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    ratioArea = wMC.var("rG1toG2").getValV()

    f.Close()

    return ratioNtoW, ratioEtaToJpsi, ratioArea



def fillRelWorkspace(w=0, SystType='Base'):

    nMCorr=0

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",minMass,maxMass)
    ratioNtoW, ratioEtaToJpsi, ratioArea = getConstPars(SystType=SystType)

    gammaEtac = 31.8
    mDiffEtac = 113.301
    eff = 0.035

    print "SETUP ", SystType
    if SystType=='eff_pppi0':  eff = 0.032
    # elif SystType=='halfBinShift':: Jpsi_M.setRange(2855,3245)
    

    rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    rNarToW    = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2    = RooRealVar("rG1toG2","rG1toG2",ratioArea)

    eff_pppi0  = RooRealVar("eff_pppi0","eff_pppi0",eff)
    nEtac      = RooRealVar("nEtac","num of Etac", 0, 1.e7)
    nJpsi      = RooRealVar("nJpsi","num of J/Psi", 0, 1.e7)
    # nEtacRel   = RooRealVar("nEtacRel","num of Etac", 0.3, 0.0, 10.0)

    # nEta_1 = RooFormulaVar("nEta_1","num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    # nEta_2 = RooFormulaVar("nEta_2","num of Etac","@0x*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))

    nEta_1 = RooFormulaVar("nEta_1","num of Etac","@0*@1",RooArgList(nEtac,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2","num of Etac","@0-@1",RooArgList(nEtac,nEta_1))

    nJpsi_1 = RooFormulaVar("nJpsi_1","num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2","num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr  = RooRealVar("nBckgr","num of backgr",1e3,0,1.e+9)
   
   
    # mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",3096.9, 3030, 3150)   
    # mass_res  = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130)
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",3096.9)   
    mass_res  = RooRealVar("mass_res","mean of gaussian",mDiffEtac)
 
    mass_eta    = RooFormulaVar("mass_eta","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res))
    
    if SystType=='FreeGamma': 
        gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 1., 80.)
    elif SystType=='Gamma34': 
        gamma_eta = RooRealVar("gamma_eta","width of Br-W", 34.)
    else:
        gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac)

    
    spin_eta    = RooRealVar("spin_eta","spin_eta", 0.)
    radius_eta  = RooRealVar("radius_eta","radius", 1.)
    proton_m    = RooRealVar("proton_m","proton mass", 938.3 )
    sigma_eta_1 = RooRealVar("sigma_eta_1","width of gaussian", 9., 5., 50.) 
    sigma_eta_2 = RooFormulaVar("sigma_eta_2","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))

    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))


    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1","gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2","gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)

    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)



    bwxg_1 = RooFFTConvPdf("bwxg_1","breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1)
    bwxg_2 = RooFFTConvPdf("bwxg_2","breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2)

    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1","gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_1)
    gauss_2 = RooGaussian("gauss_2","gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_2)



    if SystType=='Chebychev':
        a0 = RooRealVar("a0","a0",-0.4,-1.,1.)
        a1 = RooRealVar("a1","a1", 0.05,-1.,1.)
        a2 = RooRealVar("a2","a2",-0.005,-1.,1.)
    else:
        a0 = RooRealVar("a0","a0",-0.001,-3.0,0)
        a1 = RooRealVar("a1","a1", 0.,-1,1)
        a2 = RooRealVar("a2","a2",-0,-1,1)

    a3 = RooRealVar("a3","a3",0.005,-0.1,0.1)
    a4 = RooRealVar("a4","a4",0.005,-0.1,0.1)
    a5 = RooRealVar("a5","a5",-0.0001,-0.01,0)

    if SystType=='Chebychev':
        bkg = RooChebychev("bkg","Background",Jpsi_M,RooArgList(a0,a1,a2))
    else:
        bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2))

    pppi0 = RooGenericPdf("pppi0","Jpsi2pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0","nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))


    modelBkg = RooAddPdf("modelBkg","background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))

    if(SystType=='CB'):
        if(nMCorr==0) : MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__CB_SndTopoOpt_wksp.root"
        elif(nMCorr>0): MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__CB_SndTopoOpt"+str(nMCorr)+".root"
        f = TFile(MCResolutionFile,"READ") 

        wMC = f.Get("w")
        f.Close()
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.) 
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.) 
        n_eta_1.setConstant(True)       

        cb_etac_1 = BifurcatedCB("cb_etac_1_Mcorr%s", "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)   
        bwxg_1 = RooFFTConvPdf("bwxg_1_Mcorr%s","breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 

        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_Mcorr%s", "Cystal Ball Function", Jpsi_M, mass_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        modelSignal = RooAddPdf("modelSignal","signal", RooArgList(bwxg_1,cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model","signal+bkg", RooArgList(bwxg_1,cb_Jpsi_1, bkg,pppi0), RooArgList(nEta,nJpsi, nBckgr,nPPPi0))
    else:
        modelSignal = RooAddPdf("modelSignal","signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model","signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2,bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2,nBckgr,nPPPi0))


    getattr(w,'import')(model,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal,RooFit.RecycleConflictNodes())





def fitPpbar(w=0, SystType='Base', cutType="secondaryTopoOpt", nMCorr=0, fixSigma=0.):

    lowMcorr  = minMcorr + (nMCorr-1)*stepMcorr
    highMcorr = minMcorr + (nMCorr)*stepMcorr
    if nMCorr==0:
        McorrCut = "(Bc_LOKI_BPVCORRM>"+str(minMcorr)+") && (Bc_LOKI_BPVCORRM<"+str(maxMcorr)+")"
    else:
        McorrCut = "(Bc_LOKI_BPVCORRM>"+str(lowMcorr)+") && (Bc_LOKI_BPVCORRM<"+str(highMcorr)+")"


    Jpsi_M = w.var("Jpsi_M")
    pdfModel = w.pdf("model")


    DataSetFull = w.data("DataSetFull")
    DataSet     = DataSetFull.reduce(RooFit.Cut(McorrCut), RooFit.Name("DataSet"), RooFit.Title("DataSet"))

    if fixSigma!=0:
        w.var("sigma_eta_1").setVal(fixSigma)
        w.var("sigma_eta_1").setConstant(True)

    pdfModel.fitTo(DataSet, RooFit.Strategy(2))
    pdfModel.fitTo(DataSet, RooFit.Strategy(2), RooFit.Extended(True))
    r = pdfModel.fitTo(DataSet,RooFit.Minos(True), RooFit.Save(True), RooFit.Extended(True))
    r.Print("v") 


    frames = []; pulls  = []; names  = []

    frame, pull, chi2 = setupFrame(Jpsi_M, "Mcorr_"+str(nMCorr), DataSet, pdfModel)
    frames.append(frame)
    pulls.append(pull)
    names.append("")

    c = setupCanvas(names, frames, pulls)
    c.SaveAs("fitResults/plots/ppbarFit_bin{}_{}.pdf".format(nMCorr,SystType))

    save = os.dup( sys.stdout.fileno() ) 
    fileOut = file("fitResults/parsPrint/ppbarFit_bin{}_{}.txt".format(nMCorr,SystType), 'w' ) 
    os.dup2( fileOut.fileno(), sys.stdout.fileno() ) 
    os.dup2( save, sys.stdout.fileno() ) 
    fileOut.close()

    nEtac = w.var("nEtac")
    nJpsi = w.var("nJpsi")
    sigmaReso = w.var("nJpsi")
    return nJpsi.getVal(), nJpsi.getErrorHi(), nEtac.getVal(), nEtac.getErrorHi(), w.var("sigma_eta_1").getVal()


resultDict = {}
# systs = ["nonScaled","FreeGamma","eff_pppi0","Gamma34","Chebychev","rEtaToJpsi"]
systs = []


import os
SystType = 'Base'



keys = [
#        "secondaryTopoOpt",
#        "secondaryTopoOptProbNNMu02",
#        "secondaryTopoOptSoftFD",
#        "secondaryTopoOptSoftPID",
#        "secondaryTopoOptMidPID",
#        "secondaryTopoOptTightPID",
   	"secondaryTopoOptTightPIDNoPT",
    	"secondaryTopoOptNoPT",
    	"secondaryTopoOptNoPTMu250",
    	"secondaryTopoOptNoPTMu500",
    	"secondaryTopoOptNoPTMu250_02"
#    	"secondaryTopoOptNoPTMu250_04",
#    	"secondaryTopoOptNoPTMu250_02",
#    	"secondaryTopoOptMu250_04",
#    	"secondaryTopoOptMu500",
#    	"secondaryTopoOptMu250",
#    	"secondaryTopoOptMu500",
#    	"Detached"
       ]


#for cutType i1;2Dn tupleFiles.keys():
for cutType in keys:    

    exists = os.path.isfile(tupleFiles[cutType])
    fsizeOK = False
    if exists:
        fsizeOK = (os.path.getsize(tupleFiles[cutType]) > 1e6) #more than 1 MB
   
    if fsizeOK:

       histPureEtac = TH1F("histPureEtac","histPureEtac",nsteps,minMcorr,maxMcorr)
       histPureJpsi = TH1F("histPureJpsi","histPureJpsi",nsteps,minMcorr,maxMcorr)

       w = RooWorkspace("w",True)
       fillRelWorkspace(w=w,SystType=SystType)
       GetFullDataSet(w=w,cutType=cutType)

       resoFull = 0.
       NJpsi, NJpsiErr, NEtac, NEtacErr, resoFull = fitPpbar(w=w, SystType=SystType, cutType=cutType, nMCorr=0, fixSigma=0.)
       for iMcorr in range(nsteps):
          NJpsi, NJpsiErr, NEtac, NEtacErr, reso = fitPpbar(w=w, SystType=SystType, cutType=cutType, nMCorr=iMcorr+1, fixSigma=resoFull)
          NJpsi, NJpsiErr, NEtac, NEtacErr, reso = fitPpbar(w=w, SystType=SystType, cutType=cutType, nMCorr=iMcorr+1, fixSigma=resoFull) 
         

          histPureEtac.SetBinContent(iMcorr+1,NEtac)
          histPureEtac.SetBinError(iMcorr+1,NEtacErr)

          histPureJpsi.SetBinContent(iMcorr+1,NJpsi)
          histPureJpsi.SetBinError(iMcorr+1,NJpsiErr)


       histPureEtac.GetXaxis().SetTitle("Mcorr(J/#psi#mu)")
       histPureJpsi.GetXaxis().SetTitle("Mcorr(J/#psi#mu)")

       histPureEtac.SaveAs("histPureEtac_"+cutType+".root")
       histPureJpsi.SaveAs("histPureJpsi_"+cutType+".root")

       c = TCanvas()
       histPureEtac.Draw("E")
       c.SaveAs("histPureEtac_"+cutType+".pdf")

       histPureJpsi.Draw("E")
       c.SaveAs("histPureJpsi_"+cutType+".pdf")


       del histPureEtac, histPureJpsi, w, c



