from ROOT import *

import sys
sys.path.insert(0, '../')
from makeConfigDictionary import *


nBins = int((maxMass-minMass)/binWidthSnd)

def setupFrame(Jpsi_M, namePostFix,hist,model):

    nameHist = "hist_"+namePostFix
    ## Construct plot frame and draw
    Title = RooFit.Title
    Components = RooFit.Components
    LineColor = RooFit.LineColor
    Normalization = RooFit.Normalization  
    Normalization1 = RooFit.Normalization(1.0,RooAbsReal.RelativeExpected)
    Normalization0 = RooFit.Normalization(0.0,RooAbsReal.RelativeExpected)


    plot_binWidth = 5.
    plot_binN = int((maxMass-minMass)/plot_binWidth)
    binning = RooFit.Binning(plot_binN,minMass,maxMass)
    mrkStyle = RooFit.MarkerStyle(7)
    lineWidth1 = RooFit.LineWidth(1)
    lineWidth2 = RooFit.LineWidth(2)
    lineWidth0 = RooFit.LineWidth(0)
    lineColor1 = RooFit.LineColor(6)
    lineColor2 = RooFit.LineColor(8)
    lineStyle  = RooFit.LineStyle(2)
    name = RooFit.Name

     
    frame = Jpsi_M.frame(Title(" "))
    frame.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")
    frame.GetXaxis().SetTitleSize(0.06)
    frame.GetYaxis().SetTitleSize(0.06)
    frame.GetXaxis().SetTitleOffset(0.9)
    frame.GetYaxis().SetTitleOffset(0.3)  

    hist.plotOn(frame, name(nameHist), binning,mrkStyle)
    model.plotOn(frame,Normalization1,lineWidth2, name("pdf_"+namePostFix))
    hist.plotOn(frame, name(nameHist), binning,mrkStyle)
    chi2 = frame.chiSquare()
    print 'chi2 = ', chi2


    
 

    hPull = frame.pullHist(nameHist,"pdf_"+namePostFix,True)
    hPull.SetFillColor(1)
    pull = Jpsi_M.frame(Title(" ")) 
    for ii in range(hPull.GetN()):
        hPull.SetPointEYlow(ii,0)
        hPull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.05)
    pull.GetYaxis().SetLabelSize(0.05)
    pull.GetYaxis().SetTitleOffset(0.3) 
    pull.SetFillColor(1) 
    pull.addPlotable(hPull,"B")

    return frame, pull, chi2



def setupCanvas(names, frames, pulls):

    nPads = len(names)

    texData = TLatex()
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    c = TCanvas("c1","c1", 1400, 700)
    c.Divide(nPads, 2, 0.001, 0.001)
    for i in range(nPads):
            pad = c.cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.15,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.1)
            frames[i].Draw()

            texData.DrawLatex(0.3, 0.95, names[i])


            pad = c.cd(nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.15)
            pad.SetTopMargin(0.)
            pad.SetLeftMargin(0.1)
            pulls[i].Draw()
    return c