import os, sys, re
from ROOT import *

MCDir = "../MC/RapidSim"

MCdict = {
          "Bc2JpsiMuNu"          : "Bc2JpsiMuNu_tree.root",
          "Bc2JpsiTauNu"         : "Bc2JpsiTauNu_tree.root",
          "Bc2ChicMuNu"          : "Bc2ChicMuNu_tree.root",
          "Bs2JpsiPhi2MuMu"      : "Bs2JpsiPhi2MuMu_tree.root",
          "Bs2JpsiOmega2MuMu"    : "Bs2JpsiOmega2MuMu_tree.root",
          "Bs2Chic2JpsiMuMuPhi"  : "Bs2Chic2JpsiMuMuPhi_tree.root"
         }




def compareMcorr():

  gStyle.SetOptStat(000)

  histosDict = {}
  for key in MCdict.keys():
    ch = TChain("DecayTree")
    ch.Add(MCDir+"/"+MCdict[key])
    histosDict[key] = TH1D("h"+key,"h"+key,100,3.2,8.2)
    ch.Draw("Bcp_0_Mcorr>>h"+key,"","E")
    del ch


  c = TCanvas("compareMcorr","compareMcorr")
  LineColor = 0
  leg = TLegend(0.7,0.7,0.95,0.9)


  for key in MCdict.keys():
    LineColor += 1
    if LineColor==5: LineColor += 1
    histosDict[key].SetLineColor(LineColor)

    histosDict[key].SetMaximum(2e3)
    if LineColor==1:
      histosDict[key].Draw()
    else:
      histosDict[key].Draw("same")

    leg.AddEntry(histosDict[key],key,"lep")

  leg.Draw("same")
  c.SetLogy()
  c.SaveAs("compareMcorr.pdf")


compareMcorr()
