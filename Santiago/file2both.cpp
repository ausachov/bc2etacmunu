#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TText.h"
#include "TGraph.h"
#include "TArrow.h"
#include "TFile.h"
#include "TLatex.h"
#include "TMarker.h"
#include <cmath>


double NormProdVect2(double x[3], double y[3])
{
double c0=x[1]*y[2]-x[2]*y[1];
double c1=x[0]*y[2]-x[2]*y[0];
double c2=x[0]*y[1]-x[1]*y[0];

double r2=c1*c1+c2*c2+c0*c0;
return r2;
}

double dotProd(double x[3], double y[3])
{
double r=x[0]*y[0]+x[1]*y[1]+x[2]*y[2];
return r;
}

double Mass(double x[4], double y[4])
{
double r3=sqrt((x[0]+y[0])*(x[0]+y[0])-(x[1]+y[1])*(x[1]+y[1])-(x[2]+y[2])*(x[2]+y[2])-(x[3]+y[3])*(x[3]+y[3]));
return r3;
}

double MassHipot1(double x[4], double y[4], double m1, double m2)
{
double r3=sqrt((sqrt(m1*m1+x[1]*x[1]+x[2]*x[2]+x[3]*x[3])+sqrt(m2*m2+y[1]*y[1]+y[2]*y[2]+y[3]*y[3]))*(sqrt(m1*m1+x[1]*x[1]+x[2]*x[2]+x[3]*x[3])+sqrt(m2*m2+y[1]*y[1]+y[2]*y[2]+y[3]*y[3]))-(x[1]+y[1])*(x[1]+y[1])-(x[2]+y[2])*(x[2]+y[2])-(x[3]+y[3])*(x[3]+y[3]));
return r3;
}

using namespace RooFit;
using namespace RooStats;
void file2both () {

FILE *archivo1;
FILE *archivo2;
./TMVA_files/signal_etac2/DATA
archivo1=fopen("./TMVA_files/signal_etac1/DATA/values_DATA","r");
archivo2=fopen("./TMVA_files/signal_etac2/DATA/values_DATA","r");
double bdtg1, mlp1, val1;

std::ofstream MiArchivo("./txt/analisis2both.txt");
std::ofstream MiArchivo1("./txt/jpsimassandmcorr.txt");
std::ofstream MiArchivo2("./txt/weights.txt");

double x=3000;
double y=9500;
double xmpp=2750;
double ympp=3350;
double binsize=500;


TFile *f = TFile::Open("../../ROOTFILES/selected_TriggerBase.root");
TTree *DecayTree   = (TTree*)f->Get("DecayTree");
TH1F *hist2 = new TH1F("","",13,3000,9500);
//TH2F *hist = new TH2F("","",200,1300,2850,200,xmpp,ympp);
TH1F *hist01 = new TH1F("","",200,800,10000);
TH1F *hist02 = new TH1F("","",200,800,10000);
TGraph *f1 = new TGraph();

//----------- Create tree reader and its data containers---------------------
TTreeReader myReader("DecayTree", f);
//-------------------------Masa Jpsi y masa Bc corregida----------------------------------------
TTreeReaderValue<Double_t> jpsimass(myReader, "Jpsi_MM");
TTreeReaderValue<Double_t> bcMCORR(myReader, "Bc_LOKI_BPVCORRM");
TTreeReaderValue<Double_t> bcMCORRerr(myReader, "Bc_MCORRERR");
//-------------------------Stripping----------------------------------------
//-------Bc-------
TTreeReaderValue<Double_t> bcDIRA(myReader, "Bc_LOKI_DIRA");
TTreeReaderValue<Double_t> bcFlightdistance(myReader, "Bc_LOKI_FDCHI2");
TTreeReaderValue<Double_t> bcPt(myReader, "Bc_PT");
TTreeReaderValue<Double_t> bcEndvertex(myReader, "Bc_ENDVERTEX_CHI2");
TTreeReaderValue<Int_t> bcEndvertexNDOF(myReader, "Bc_ENDVERTEX_NDOF");
//-------ppbar-------
TTreeReaderValue<Double_t> jpsiDIRA(myReader, "Jpsi_LOKI_DIRA");
TTreeReaderValue<Double_t> jpsiFlightdistance(myReader, "Jpsi_LOKI_FDCHI2");
TTreeReaderValue<Double_t> jpsiEndvertex(myReader, "Jpsi_ENDVERTEX_CHI2");
TTreeReaderValue<Int_t> jpsiEndvertexNDOF(myReader, "Jpsi_ENDVERTEX_NDOF");
//-------p and pbar-------
TTreeReaderValue<Double_t> protonpTrackChi2(myReader, "ProtonP_TRACK_CHI2NDOF");
TTreeReaderValue<Double_t> protonpTrackGhost(myReader, "ProtonP_TRACK_GhostProb");
TTreeReaderValue<Double_t> protonpIp(myReader, "ProtonP_IPCHI2_OWNPV");
TTreeReaderValue<Bool_t> protonpismuon(myReader, "ProtonP_isMuon");
TTreeReaderValue<Double_t> protonpP(myReader, "ProtonP_P");
TTreeReaderValue<Double_t> protonpPt(myReader, "ProtonP_PT");
TTreeReaderValue<Double_t> protonpPIDp(myReader, "ProtonP_PIDp");
TTreeReaderValue<Double_t> protonpPIDk(myReader, "ProtonP_PIDK");

TTreeReaderValue<Double_t> protonnTrackChi2(myReader, "ProtonM_TRACK_CHI2NDOF");
TTreeReaderValue<Double_t> protonnTrackGhost(myReader, "ProtonM_TRACK_GhostProb");
TTreeReaderValue<Double_t> protonnIp(myReader, "ProtonM_IPCHI2_OWNPV");
TTreeReaderValue<Bool_t> protonnismuon(myReader, "ProtonM_isMuon");
TTreeReaderValue<Double_t> protonnP(myReader, "ProtonM_P");
TTreeReaderValue<Double_t> protonnPt(myReader, "ProtonM_PT");
TTreeReaderValue<Double_t> protonnPIDp(myReader, "ProtonM_PIDp");
TTreeReaderValue<Double_t> protonnPIDk(myReader, "ProtonM_PIDK");
//-------mu-------
TTreeReaderValue<Double_t> muTrackChi2(myReader, "Mu_TRACK_CHI2NDOF");
TTreeReaderValue<Double_t> muTrackGhost(myReader, "Mu_TRACK_GhostProb");
TTreeReaderValue<Double_t> muIp(myReader, "Mu_IPCHI2_OWNPV");
TTreeReaderValue<Bool_t> muismuon(myReader, "Mu_isMuon");
TTreeReaderValue<Double_t> muP(myReader, "Mu_P");
//-------GEC-------
TTreeReaderValue<Int_t> multiplicity(myReader, "nSPDHits");
//------------------------------Other cuts---------------------------------------------------------------------------------------------------
//-------------------Selección en triggers------------------------------------
//-----L0---------
TTreeReaderValue<Bool_t> triggerl01(myReader, "Bc_L0HadronDecision_TOS");
TTreeReaderValue<Bool_t> triggerl02(myReader, "Bc_L0MuonDecision_TOS");
//-----HLT1-------
TTreeReaderValue<Bool_t> triggerl11(myReader, "Bc_Hlt1TwoTrackMVADecision_TOS");
TTreeReaderValue<Bool_t> triggerl12(myReader, "Bc_Hlt1SingleMuonNoIPDecision_TOS");
//-----HLT2-------
TTreeReaderValue<Bool_t> triggerl212(myReader, "Bc_Hlt2TopoMu2BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl213(myReader, "Bc_Hlt2TopoMu3BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl222(myReader, "Bc_Hlt2Topo2BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl223(myReader, "Bc_Hlt2Topo3BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl23(myReader, "Bc_Hlt2SingleMuonDecision_TOS");
TTreeReaderValue<Bool_t> triggerl24(myReader, "Bc_Hlt2XcMuXForTauB2XcMuDecision_TOS");
//---------------------Cortes en protones-----------------------------------
TTreeReaderValue<Double_t> protonpPe(myReader, "ProtonP_PE");
TTreeReaderValue<Double_t> protonpPx(myReader, "ProtonP_PX");
TTreeReaderValue<Double_t> protonpPy(myReader, "ProtonP_PY");
TTreeReaderValue<Double_t> protonpPz(myReader, "ProtonP_PZ");
TTreeReaderValue<Double_t> protonpProbp(myReader, "ProtonP_ProbNNp");
TTreeReaderValue<Double_t> protonpProbk(myReader, "ProtonP_ProbNNk");
TTreeReaderValue<Double_t> protonpProbpi(myReader, "ProtonP_ProbNNpi");
TTreeReaderValue<Double_t> protonnPe(myReader, "ProtonM_PE");
TTreeReaderValue<Double_t> protonnPx(myReader, "ProtonM_PX");
TTreeReaderValue<Double_t> protonnPy(myReader, "ProtonM_PY");
TTreeReaderValue<Double_t> protonnPz(myReader, "ProtonM_PZ");
TTreeReaderValue<Double_t> protonnProbp(myReader, "ProtonM_ProbNNp");
TTreeReaderValue<Double_t> protonnProbk(myReader, "ProtonM_ProbNNk");
TTreeReaderValue<Double_t> protonnProbpi(myReader, "ProtonM_ProbNNpi");
//---------------------Cortes en muones-------------------------------------
TTreeReaderValue<Double_t> muPt(myReader, "Mu_PT");
TTreeReaderValue<Double_t> muPe(myReader, "Mu_PE");
TTreeReaderValue<Double_t> muPx(myReader, "Mu_PX");
TTreeReaderValue<Double_t> muPy(myReader, "Mu_PY");
TTreeReaderValue<Double_t> muPz(myReader, "Mu_PZ");
TTreeReaderValue<Double_t> muOrivertex(myReader, "Mu_ORIVX_CHI2");
TTreeReaderValue<Double_t> muProbmu(myReader, "Mu_ProbNNmu");
//--------------------Cortes en charmonia------------------------------------
TTreeReaderValue<Double_t> jpsiPt(myReader, "Jpsi_PT");
TTreeReaderValue<Double_t> jpsiPx(myReader, "Jpsi_PX");
TTreeReaderValue<Double_t> jpsiPy(myReader, "Jpsi_PY");
TTreeReaderValue<Double_t> jpsiPz(myReader, "Jpsi_PZ");
//------------Selección en posición vertice secundario------------------------------------
TTreeReaderValue<Double_t> bcX(myReader, "Bc_ENDVERTEX_X");
TTreeReaderValue<Double_t> bcY(myReader, "Bc_ENDVERTEX_Y");
TTreeReaderValue<Double_t> bcZ(myReader, "Bc_ENDVERTEX_Z");
TTreeReaderValue<Double_t> bcXerr(myReader, "Bc_ENDVERTEX_XERR");
TTreeReaderValue<Double_t> bcYerr(myReader, "Bc_ENDVERTEX_YERR");
TTreeReaderValue<Double_t> bcZerr(myReader, "Bc_ENDVERTEX_ZERR");
TTreeReaderValue<Double_t> protonpX(myReader, "ProtonP_ORIVX_X");
TTreeReaderValue<Double_t> protonpY(myReader, "ProtonP_ORIVX_Y");
TTreeReaderValue<Double_t> protonpZ(myReader, "ProtonP_ORIVX_Z");
TTreeReaderValue<Double_t> protonpXerr(myReader, "ProtonP_ORIVX_XERR");
TTreeReaderValue<Double_t> protonpYerr(myReader, "ProtonP_ORIVX_YERR");
TTreeReaderValue<Double_t> protonpZerr(myReader, "ProtonP_ORIVX_ZERR");
TTreeReaderValue<Double_t> protonnX(myReader, "ProtonM_ORIVX_X");
TTreeReaderValue<Double_t> protonnY(myReader, "ProtonM_ORIVX_Y");
TTreeReaderValue<Double_t> protonnZ(myReader, "ProtonM_ORIVX_Z");
TTreeReaderValue<Double_t> protonnXerr(myReader, "ProtonM_ORIVX_XERR");
TTreeReaderValue<Double_t> protonnYerr(myReader, "ProtonM_ORIVX_YERR");
TTreeReaderValue<Double_t> protonnZerr(myReader, "ProtonM_ORIVX_ZERR");
TTreeReaderValue<Double_t> muX(myReader, "Mu_ORIVX_X");
TTreeReaderValue<Double_t> muY(myReader, "Mu_ORIVX_Y");
TTreeReaderValue<Double_t> muZ(myReader, "Mu_ORIVX_Z");
TTreeReaderValue<Double_t> muXerr(myReader, "Mu_ORIVX_XERR");
TTreeReaderValue<Double_t> muYerr(myReader, "Mu_ORIVX_YERR");
TTreeReaderValue<Double_t> muZerr(myReader, "Mu_ORIVX_ZERR");
//-------------------------Cortes Bc----------------------------------------
TTreeReaderValue<Double_t> bciX(myReader, "Bc_OWNPV_X");
TTreeReaderValue<Double_t> bciY(myReader, "Bc_OWNPV_Y");
TTreeReaderValue<Double_t> bciZ(myReader, "Bc_OWNPV_Z");
TTreeReaderValue<Double_t> bciXerr(myReader, "Bc_OWNPV_XERR");
TTreeReaderValue<Double_t> bciYerr(myReader, "Bc_OWNPV_YERR");
TTreeReaderValue<Double_t> bciZerr(myReader, "Bc_OWNPV_ZERR");
//---------------------------------------------------------------------------

int count=0;


while (myReader.Next()){
double cuadrimomPN[4]={*protonnPe,*protonnPx,*protonnPy,*protonnPz};
double cuadrimomPP[4]={*protonpPe,*protonpPx,*protonpPy,*protonpPz};
double cuadrimomMU[4]={*muPe,*muPx,*muPy,*muPz};

if((((*bcEndvertex)/(*bcEndvertexNDOF))<15)&&(*bcDIRA>0.999)&&(*bcFlightdistance>50)&&(Mass(cuadrimomPP,cuadrimomMU)>1000)&&(*bcPt>1500)&&(((*jpsiEndvertex)/(*jpsiEndvertexNDOF))<8)&&(*jpsiDIRA>0.999)&&(*jpsiFlightdistance>25)&&(*protonpTrackChi2<3)&&(*protonnTrackChi2<3)&&(*protonpTrackGhost<0.35)&&(*protonnTrackGhost<0.35)&&(*protonpIp>9)&&(*protonnIp>9)&&(*protonpismuon==false)&&(*protonnismuon==false)&&(*protonpP>15000)&&(*protonnP>15000)&&(*protonpPt>800)&&(*protonnPt>800)&&(*protonpPIDp>2)&&(*protonnPIDp>2)&&(((*protonpPIDp)-(*protonpPIDk))>2)&&(((*protonnPIDp)-(*protonnPIDk))>2)&&(*muTrackChi2<4)&&(*muTrackGhost<0.35)&&(*muIp>16)&&(*muismuon==true)&&(*muP>3000)&&(*multiplicity<600)&&((*triggerl01==true)||(*triggerl02==true))&&(*triggerl11==true)&&((*triggerl212==true)||(*triggerl213==true)||(*triggerl222==true)||(*triggerl223==true)||(*triggerl23==true)||(*triggerl24==true)))
{
if((*bcMCORR<25000)&&(*bcMCORR>0)&&(*muProbmu>0.2)&&(((*bcEndvertex)/(*bcEndvertexNDOF))<9)&&(*bcMCORRerr<900)&&(*protonpP<150000)&&(*protonnP<150000)&&((MassHipot1(cuadrimomPP,cuadrimomPN,139.57,139.57)<1770)||(MassHipot1(cuadrimomPP,cuadrimomPN,139.57,139.57)>1805))&&((MassHipot1(cuadrimomPP,cuadrimomPN,493.68,493.68)<1920)||(MassHipot1(cuadrimomPP,cuadrimomPN,493.68,493.68)>1950))&&(*jpsimass<ympp)&&(*jpsimass>xmpp))
{

fscanf(archivo1,"%lf	%lf	%lf\n", &bdtg1,&mlp1,&val1);

if(bdtg1>0.63)
{

fscanf(archivo2,"%lf	%lf	%lf\n", &bdtg1,&mlp1,&val1);
MiArchivo<<*jpsimass<<" , "<<val1<<" , "<<*jpsimass-val1<<std::endl;

if(bdtg1>0.2)
{
MiArchivo1<<*jpsimass<<" "<<*bcMCORR<<" "<<*bcEndvertex<<std::endl;
hist2->Fill(*bcMCORR);
//hist->Fill(MassHipot1(cuadrimomPN,cuadrimomPP,493.68,139.57),*jpsimass);
hist01->Fill(*protonnPt);
hist02->Fill(*protonpPt);
}

}
}
}
count++;
}

MiArchivo1.close();
fclose (archivo1);
fclose (archivo2);


  // Observable
  RooRealVar mes("mes", "m_{p#bar{p}} (MeV/c^{2})", xmpp, ympp);
  // Observable
  RooRealVar mesmcorr("mesmcorr", "m_{corr} (MeV/c^{2})", x, y);
  // Observable
  RooRealVar variable("variable", "variable", 0, 45);

  // Build Gaussian resolucion
  RooRealVar  sigMeanJpsi("sigMeanJpsi", "J/#Psi mass", 3096.5,"MeV/c^{2}");
  RooRealVar  sigMeanetac("sigMeanetac", "#eta_{c} mass", 2983.5,"MeV/c^{2}");
  RooRealVar  sigWidth("sigWidth", "Width", 9,"MeV/c^{2}");

  RooRealVar  decaywidthetac("decayWidthetac", "decayWidthetac", 32,"MeV/c^{2}");



  // Build Resonances fit
  RooVoigtian etacresonance("etacresonance","etacresonance",mes,sigMeanetac,decaywidthetac,sigWidth);
  RooGaussian gaussJpsi("Jpsiresonance","Jpsiresonance", mes, sigMeanJpsi, sigWidth);

  // Build Exponencial background PDF
  RooRealVar slope("slope", "slope", -0.1, -5., 0.); 
  RooExponential expo("expo", "Expo PDF", mes, slope);

  //--------Construct signal+background PDF
  RooRealVar nSigJpsi("nSigJpsi", "#signal events J/#Psi", 1111,0,4000);
  RooRealVar nSigetac("nSigetac", "#signal events #eta_{c}", 464,0,2000);
  RooRealVar nBkg("nBkg", "#background events", 13558,0,66000);
  RooAddPdf  model("model", "g1+g2+a", RooArgList(gaussJpsi,etacresonance,expo), RooArgList(nSigJpsi,nSigetac, nBkg));


  //----------Generate a toyMC sample from composite PDF

RooDataSet * data = RooDataSet::read("./txt/jpsimassandmcorr.txt",RooArgList(mes,mesmcorr,variable));


  //--------Perform extended ML fit of composite PDF to toy data
  model.fitTo(*data, RooFit::Extended());

//------------------Sweights---------------------------------------------------------------------------------
  SPlot* sData = new RooStats::SPlot("sData","An SPlot", *data, &model,RooArgList(nSigJpsi,nSigetac, nBkg));

 //for(Int_t i=0; i < 23595; i++)
 //  {
 //  MiArchivo2<< "Jpsi Weight   " << sData->GetSWeight(i,"nSigJpsi")
 //                 << "   etac Weight   " << sData->GetSWeight(i,"nSigetac")
 //                 << "   bkg Weight   " << sData->GetSWeight(i,"nBkg")
 //                 << "  Total Weight   " << sData->GetSumOfEventSWeight(i)
 //                 << std::endl;
 //  }
 //MiArchivo2<< "------------------------------------" << std::endl;

      MiArchivo2<< std::endl <<  "Yield of signal Jpsi is " << nSigJpsi.getVal() << ".  From sWeights it is " << sData->GetYieldFromSWeight("nSigJpsi") << std::endl;
      MiArchivo2<< "Yield of signal etac is " << nSigetac.getVal() << ".  From sWeights it is " << sData->GetYieldFromSWeight("nSigetac") << std::endl;
      MiArchivo2<< "Yield of background is " << nBkg.getVal() << ".  From sWeights it is " << sData->GetYieldFromSWeight("nBkg") << std::endl << std::endl;

  // create weighted data sets
  RooDataSet * dataw_sigJpsi = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,"nSigJpsi_sw") ;
  RooDataSet * dataw_sigetac = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,"nSigetac_sw") ;
  RooDataSet * dataw_Bkg     = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,"nBkg_sw") ;

      MiArchivo2<< "Número:   " << sData->GetNumSWeightVars()<< std::endl;

MiArchivo2.close();

bool z=data->write ( "./txt/events.txt");


  RooPlot* frame_sigJpsi = mesmcorr.frame((y-x)/binsize) ;
  frame_sigJpsi->SetTitle("sPlot for the signal Jpsi distribution");
  dataw_sigJpsi->plotOn(frame_sigJpsi, RooFit::DataError(RooAbsData::SumW2), MarkerSize(1.5), MarkerColor(kBlue)) ;

  RooPlot* frame_sigetac = mesmcorr.frame((y-x)/binsize) ;
  frame_sigetac->SetTitle("sPlot for the signal etac distribution");
  dataw_sigetac->plotOn(frame_sigetac, RooFit::DataError(RooAbsData::SumW2), MarkerSize(1.5), MarkerColor(kBlue)) ;

  RooPlot* frame_Bkg = mesmcorr.frame((y-x)/binsize) ;
  frame_Bkg->SetTitle("sPlot for the background distribution");
  dataw_Bkg->plotOn(frame_Bkg, RooFit::DataError(RooAbsData::SumW2), MarkerSize(1.5), MarkerColor(kBlue)) ;


//-----------------------------------------------------------------------------------------------

  RooPlot *mesFrame = mes.frame(50);
  mesFrame->SetTitle("     ");

 data->plotOn(mesFrame, RooFit::Name("Data"));
 model.plotOn(mesFrame, RooFit::Components(gaussJpsi), RooFit::LineColor(kRed), RooFit::LineStyle(kDashed), RooFit::Name("Jpsisignal"));
 model.plotOn(mesFrame, RooFit::Components(etacresonance), RooFit::LineColor(kGreen), RooFit::LineStyle(kDashed), RooFit::Name("etacsignal"));
 model.plotOn(mesFrame, RooFit::Components(expo), RooFit::LineStyle(kDashed), RooFit::Name("Exponential"));
 model.plotOn(mesFrame, RooFit::Name("Model"));

 model.paramOn(mesFrame,Layout(0.75,0.98,0.95));
 mesFrame->getAttText()->SetTextSize(0.04);
// data->statOn(mesFrame,Layout(0.30,0.45,0.95));
// mesFrame->getAttText()->SetTextSize(0.025);

   RooHist *hpull = mesFrame->pullHist();
   RooPlot *frame2 = mes.frame(Title(" "));
   frame2->addPlotable(hpull, "P");

auto legend1 = new TLegend(0.75,0.42,0.95,0.62);
legend1->SetBorderSize(0);
//legend->SetHeader("Convenciones","C"); // option "C" allows to center the header
legend1->AddEntry(mesFrame->findObject("Data"),"Data");
legend1->AddEntry(mesFrame->findObject("Model"),"Total fit");
legend1->AddEntry(mesFrame->findObject("Jpsisignal"),"J/#Psi signal fit");
legend1->AddEntry(mesFrame->findObject("etacsignal"),"#eta_{c} signal fit");
legend1->AddEntry(mesFrame->findObject("Exponential"),"Exponential background");

mes.setRange("etac", 2840, 3130);
RooAbsReal* fracInt1 = etacresonance.createIntegral(mes,NormSet(mes), Range("etac"));
RooAbsReal* fracInt3 = expo.createIntegral(mes,NormSet(mes), Range("etac"));
mes.setRange("jpsi", 2840, 3130);
RooAbsReal* fracInt2 = gaussJpsi.createIntegral(mes,NormSet(mes), Range("jpsi"));
RooAbsReal* fracInt4 = expo.createIntegral(mes,NormSet(mes), Range("jpsi"));
mes.setRange("Background-izquierdo", xmpp, 2840);
RooAbsReal* fracInt5 = expo.createIntegral(mes,NormSet(mes), Range("Background-izquierdo"));
mes.setRange("Background-derecho", 3130, ympp);
RooAbsReal* fracInt6 = expo.createIntegral(mes,NormSet(mes), Range("Background-derecho"));

MiArchivo<< "------------------------------------" << std::endl;
MiArchivo<< "------------------------------------" << std::endl;
MiArchivo<<sqrt((fracInt5->getVal()+fracInt6->getVal())*nBkg.getVal())<<","<<sqrt((fracInt5->getVal()+fracInt6->getVal())*nBkg.getAsymErrorHi()*nBkg.getAsymErrorHi()/(4*nBkg.getVal()))<<","<< endl;
MiArchivo<< "------------------------------------" << std::endl;
MiArchivo<<(fracInt5->getVal()+fracInt6->getVal())*nBkg.getVal()<<" , "<<(fracInt5->getVal()+fracInt6->getVal())*nBkg.getAsymErrorHi()<< endl;
MiArchivo<< "------------------------------------" << std::endl;
MiArchivo<<"Signal-etac: "<< fracInt1->getVal() << endl;
MiArchivo<<"Signal-jpsi: "<< fracInt2->getVal() << endl;
MiArchivo<<"Background central-etac: "<< fracInt3->getVal() << endl;
MiArchivo<<"Background central-jpsi: "<< fracInt4->getVal() << endl;
MiArchivo<<"Background izquierdo: "<< fracInt5->getVal() << endl;
MiArchivo<<"Background derecho: "<< fracInt6->getVal() << endl;
MiArchivo<<"Eventos background "<< nBkg.getVal()<< endl;
MiArchivo<<"Error background "<<nBkg.getAsymErrorHi()<< endl;





MiArchivo.close();


//-------------------------------------------------------------------------------
const int n = 13;
double v[n]={3250,3750,4250,4750,5250,5750,6250,6750,7250,7750,8250,8750,9250};
double ex[n]={250,250,250,250,250,250,250,250,250,250,250,250,250};
double y1[n]={26.7635,141.406,303.113,265.374,115.791,48.078,44.0141,11.1605,10.9526,-2.00186,0.348386,5.56519,-4.22257};
double ey1[n]={18.5272,48.401,54.4478,39.153,22.3424,14.4477,12.3765,9.2694,8.36676,5.66947,5.59956,5.86036,3.85471};
double y2[n]={8.67716,289.912,550.265,489.018,293.987,171.098,157.046,72.8629,14.316,5.30652,6.09118,3.11906,2.89161};
double ey2[n]={5.69739,24.9528,32.5375,27.8108,19.5927,14.4259,13.4933,9.5806,5.2834,3.67176,3.70028,2.80405,2.6589};
double b[n]={2055.72,9591.69,10341.9,4811.52,1413.15,580.841,363.055,294.987,196.746,154.687,131.52,95.3269,76.275};
double eb[n]={49.1738,110.947,117.092,80.3629,43.7661,28.1203,22.4818,19.8555,16.5562,14.1178,13.1329,11.4238,10.0526};





TGraphErrors *etac= new TGraphErrors(n,v,y1,ex,ey1);
TGraphErrors *jpsi= new TGraphErrors(n,v,y2,ex,ey2);
TGraphErrors *background= new TGraphErrors(n,v,b,ex,eb);

//-------------------------------------------------------------------------------
TCanvas *c = new TCanvas("file", "file", 500, 300);
   c->Divide(1,2,0);
   c->cd(1);
   gPad->SetRightMargin(0.001);
   gPad->SetBottomMargin(0.001);
   gPad->SetPad(0,0.25,0.9,0.92);
   mesFrame->GetYaxis()->SetTitleOffset(1.6);
   mesFrame->Draw();
   legend1->Draw("same");

   c->cd(2);
   gPad->SetRightMargin(0.001);
   gPad->SetBottomMargin(0.3);
   gPad->SetPad(0,0,0.9,0.25);
   frame2->GetYaxis()->SetTitleOffset(1.6);
   frame2->GetXaxis()->SetTitleSize(.1);
   frame2->GetYaxis()->SetLabelSize(0.05);
   frame2->GetXaxis()->SetLabelSize(0.07);
   frame2->Draw();

TLine *w = new TLine(xmpp,0,ympp,0);
w->SetLineColor(kBlack);
//w->SetLineStyle(9);
w->Draw();


//--------------------------------------------------------
TLine *ww = new TLine(x,0,y,0);
TCanvas *c1 = new TCanvas("file1", "file1", 3200, 1800);
c1->Divide(2,2);
c1->cd(1);
hist2->GetYaxis()->SetTitleSize(0.04);
hist2->GetXaxis()->SetTitleSize(0.04);	
hist2->SetMinimum(0);
hist2->SetTitle("M_{corr}");			
hist2->GetYaxis()->CenterTitle();
hist2->GetXaxis()->CenterTitle();
hist2->GetYaxis()->SetTitle("Number of events");				
hist2->GetXaxis()->SetTitle("B_{c} corrected mass [MeV/c^{2}]"); //Change axis-titles
hist2->Draw();

ww->SetLineColor(kBlack);
//w->SetLineStyle(9);
ww->Draw();

c1->cd(2);
frame_sigJpsi->Draw();
//frame_sigJpsi->SetMinimum(-100);
jpsi->SetMarkerColor(kRed);
jpsi->SetMarkerSize(1.5);
jpsi->SetMarkerStyle(kFullCircle);
jpsi->Draw("PSAME");
ww->Draw();

c1->cd(3);
frame_Bkg->Draw();
//frame_Bkg->SetMinimum(0);
background->SetMarkerColor(kRed);
background->SetMarkerSize(1.5);
background->SetMarkerStyle(kFullCircle);
background->Draw("PSAME");
ww->Draw();

c1->cd(4);
frame_sigetac->Draw();
//frame_sigetac->SetMinimum(0);
etac->SetMarkerColor(kRed);
etac->SetMarkerSize(1.5);
etac->SetMarkerStyle(kFullCircle);
etac->Draw("PSAME");
ww->Draw();

//--------------------------------------------------------
TCanvas *c2 = new TCanvas("file2", "file2", 1600, 900);
c2->Divide(1,2);
c2->cd(1);
hist01->GetYaxis()->SetTitleSize(0.04);
hist01->GetXaxis()->SetTitleSize(0.04);	
hist01->GetYaxis()->CenterTitle();
hist01->GetXaxis()->CenterTitle();
hist01->GetYaxis()->SetTitle("Events");				
hist01->GetXaxis()->SetTitle("ProtonN_PT"); //Change axis-titles
hist01->Draw();
c2->cd(2);
hist02->GetYaxis()->SetTitleSize(0.04);
hist02->GetXaxis()->SetTitleSize(0.04);	
hist02->GetYaxis()->CenterTitle();
hist02->GetXaxis()->CenterTitle();
hist02->GetYaxis()->SetTitle("Events");				
hist02->GetXaxis()->SetTitle("ProtonP_PT"); //Change axis-titles
hist02->Draw();
//--------------------------------------------------------
TCanvas *c3 = new TCanvas("file3", "file3", 1600, 900);
frame_sigJpsi->Draw();
//frame_sigJpsi->SetMinimum(0);
jpsi->Draw("PSAME");
ww->Draw("same");
c3->Print("SPLOTS-jpsi.png");
c3->Clear();


TCanvas *c4 = new TCanvas("file4", "file4", 1600, 900);
frame_sigetac->Draw();
//frame_sigetac->SetMinimum(0);
etac->Draw("PSAME");
ww->Draw("same");
c4->Print("SPLOTS-etac.png");
c4->Clear();

TCanvas *c5 = new TCanvas("file5", "file5", 1600, 900);
frame_Bkg->Draw();
//frame_Bkg->SetMinimum(0);
background->Draw("PSAME");
ww->Draw("same");
c5->Print("SPLOTS-bkg.png");
c5->Clear();



} 

