#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TText.h"
#include "TGraph.h"
#include "TArrow.h"
#include "TFile.h"
#include "TLatex.h"
#include <cmath>



using namespace RooFit;
using namespace RooStats;

void TESTwithSidebands() {


TFile *f = TFile::Open("./TMVA.root");
TTree *DecayTree1   = (TTree*)f->Get("dataset/TrainTree");
TTree *DecayTree2   = (TTree*)f->Get("dataset/TestTree");

TTreeReader myReader1("dataset/TrainTree", f);
TTreeReader myReader2("dataset/TestTree",  f);

TTreeReaderValue<Int_t> id1(myReader1, "classID");
TTreeReaderValue<Float_t> bdtg1(myReader1, "BDTG");

TTreeReaderValue<Int_t> id2(myReader2, "classID");
TTreeReaderValue<Float_t> bdtg2(myReader2, "BDTG");

//-------------------------------------

const int n = 1000;
double xmin=-1, xmax=1;
double cuts[n], counts2[n], counts3[n], efi[n], errefi[n], val[n], errval[n];

int countS, countB;

for(int i=0; i<n; i++)
{
cuts[i]=xmin+i*((xmax-xmin)/n);
counts2[i]=0;
counts3[i]=0;
}

//--------------------------------------------------------------
/*while (myReader1.Next())
{
for(int i=0; i<n; i++){if(*bdtg1>cuts[i]){if(*id1==0){counts3[i]++;}else{counts2[i]++;}}}
}*/

while (myReader2.Next())
{
for(int i=0; i<n; i++){if(*bdtg2>cuts[i]){if(*id2==0){counts3[i]++;}else{counts2[i]++;}}}
}

countS=counts3[0], countB=counts2[0];

//--------------------------------------------------------------
double xmaxvalue=-1, maxvalue=0;
for(int i=0; i<n; i++)
{
efi[i]=(counts3[i])/(countS);
errefi[i]=sqrt(efi[i]*(1-efi[i])/(countS));
val[i]=efi[i]/(2.5+sqrt(counts2[i]));
if(val[i]>maxvalue){maxvalue=val[i]; xmaxvalue=cuts[i];}
errval[i]=errefi[i]/(2.5+sqrt(counts2[i]));
}


std::cout<<"xmaxvalue="<<xmaxvalue<<", maxvalue="<<maxvalue<<", countS="<<countS<<", countB="<<countB<<std::endl;

//------------------------------------------------------------Print-----------------------------------------------------------------

TGraphErrors *ffff= new TGraphErrors(n,cuts,val,0,errval);

TCanvas *c1 = new TCanvas("file1", "file1", 500, 300);

ffff->SetTitle("");
ffff->GetXaxis()->SetLimits(xmin,xmax);
ffff->SetMarkerColor(kBlue);
ffff->SetMarkerSize(2);
ffff->SetMarkerStyle(11);
ffff->GetYaxis()->SetTitleSize(0.04);
ffff->GetXaxis()->SetTitleSize(0.04);				
ffff->GetYaxis()->CenterTitle();
ffff->GetXaxis()->CenterTitle();
ffff->GetYaxis()->SetTitle("Figure of merit value");				
ffff->GetXaxis()->SetTitle("Cut in BDTG output"); //Change axis-titles
ffff->Draw( "AP" );
TLine *w = new TLine(xmaxvalue,val[n-1],xmaxvalue,maxvalue);
w->SetLineColor(kRed);
w->SetLineStyle(9);
w->Draw();

}

