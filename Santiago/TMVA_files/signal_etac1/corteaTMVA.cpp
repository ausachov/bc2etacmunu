#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "TText.h"
#include "TGraph.h"
#include "TArrow.h"
#include "TFile.h"
#include "TLatex.h"
#include <cmath>


using namespace RooFit;
using namespace RooStats;
void corteaTMVA () {


//-------------------------------------
FILE *archivo1;
archivo1=fopen("./DATA/values_DATA","r");
double bdtg, mlp, val;
double xmpp=2750, ympp=3350;


std::ofstream MiArchivo1("./jpsimassandmcorr.txt");


for(int i=0; i<514822; i++)
{
fscanf(archivo1,"%lf	%lf	%lf\n", &bdtg,&mlp,&val); 

if(bdtg>0.63 && mlp>3000 && mlp<9500)
{
MiArchivo1<<val<<std::endl;
}

}





MiArchivo1.close();
fclose(archivo1);


  // Observable
  RooRealVar mes("mes", "m_{p#bar{p}} (MeV/c^{2})", xmpp, ympp);
 
  // Build Gaussian resolucion
  RooRealVar  sigMeanJpsi("sigMeanJpsi", "J/#Psi mass", 3096.5,"MeV/c^{2}");
  RooRealVar  sigMeanetac("sigMeanetac", "#eta_{c} mass", 2983.5,"MeV/c^{2}");
  RooRealVar  sigWidth("sigWidth", "Width", 9,"MeV/c^{2}");

  RooRealVar  decaywidthetac("decayWidthetac", "decayWidthetac", 32,"MeV/c^{2}");



  // Build Resonances fit
  RooVoigtian etacresonance("etacresonance","etacresonance",mes,sigMeanetac,decaywidthetac,sigWidth);
  RooGaussian gaussJpsi("Jpsiresonance","Jpsiresonance", mes, sigMeanJpsi, sigWidth);

  // Build Exponencial background PDF
  RooRealVar slope("slope", "slope", -0.1, -5., 0.); 
  RooExponential expo("expo", "Expo PDF", mes, slope);

  //--------Construct signal+background PDF
  RooRealVar nSigJpsi("nSigJpsi", "#signal events J/#Psi", 8000,0,15000);
  RooRealVar nSigetac("nSigetac", "#signal events #eta_{c}", 3000,0,10000);
  RooRealVar nBkg("nBkg", "#background events", 380000,0,660000);
  RooAddPdf  model("model", "g1+g2+a", RooArgList(gaussJpsi,etacresonance,expo), RooArgList(nSigJpsi,nSigetac, nBkg));


  //----------Generate a toyMC sample from composite PDF

RooDataSet * data = RooDataSet::read("./jpsimassandmcorr.txt",RooArgList(mes));


  //--------Perform extended ML fit of composite PDF to toy data
  model.fitTo(*data, RooFit::Extended());



//-----------------------------------------------------------------------------------------------

  RooPlot *mesFrame = mes.frame(50);
  mesFrame->SetTitle("     ");

 data->plotOn(mesFrame, RooFit::Name("Data"));
 model.plotOn(mesFrame, RooFit::Components(gaussJpsi), RooFit::LineColor(kRed), RooFit::LineStyle(kDashed), RooFit::Name("Jpsisignal"));
 model.plotOn(mesFrame, RooFit::Components(etacresonance), RooFit::LineColor(kGreen), RooFit::LineStyle(kDashed), RooFit::Name("etacsignal"));
 model.plotOn(mesFrame, RooFit::Components(expo), RooFit::LineStyle(kDashed), RooFit::Name("Exponential"));
 model.plotOn(mesFrame, RooFit::Name("Model"));

 model.paramOn(mesFrame,Layout(0.75,0.98,0.95));
 mesFrame->getAttText()->SetTextSize(0.04);
 data->statOn(mesFrame,Layout(0.30,0.45,0.95));
 mesFrame->getAttText()->SetTextSize(0.04);

   RooHist *hpull = mesFrame->pullHist();
   RooPlot *frame2 = mes.frame(Title(" "));
   frame2->addPlotable(hpull, "P");

auto legend1 = new TLegend(0.75,0.37,0.95,0.57);
legend1->SetBorderSize(0);
//legend->SetHeader("Convenciones","C"); // option "C" allows to center the header
legend1->AddEntry(mesFrame->findObject("Data"),"Data");
legend1->AddEntry(mesFrame->findObject("Model"),"Total fit");
legend1->AddEntry(mesFrame->findObject("Jpsisignal"),"J/#Psi signal fit");
legend1->AddEntry(mesFrame->findObject("etacsignal"),"#eta_{c} signal fit");
legend1->AddEntry(mesFrame->findObject("Exponential"),"Exponential background");


//-------------------------------------------------------------------------------

mes.setRange("etac", 2840, 3130);
RooAbsReal* fracInt1 = etacresonance.createIntegral(mes,NormSet(mes), Range("etac"));
RooAbsReal* fracInt3 = expo.createIntegral(mes,NormSet(mes), Range("etac"));
mes.setRange("jpsi", 2840, 3130);
RooAbsReal* fracInt2 = gaussJpsi.createIntegral(mes,NormSet(mes), Range("jpsi"));
RooAbsReal* fracInt4 = expo.createIntegral(mes,NormSet(mes), Range("jpsi"));
mes.setRange("Background-izquierdo", xmpp, 2840);
RooAbsReal* fracInt5 = expo.createIntegral(mes,NormSet(mes), Range("Background-izquierdo"));
mes.setRange("Background-derecho", 3130, ympp);
RooAbsReal* fracInt6 = expo.createIntegral(mes,NormSet(mes), Range("Background-derecho"));




//-------------------------------------------------------------------------------
TCanvas *c = new TCanvas("file2", "file2", 500, 300);
   c->Divide(1,2,0);
   c->cd(1);
   gPad->SetRightMargin(0.001);
   gPad->SetBottomMargin(0.001);
   gPad->SetPad(0,0.25,0.9,0.92);
   mesFrame->GetYaxis()->SetTitleOffset(1.6);
   mesFrame->Draw();
   legend1->Draw("same");

   c->cd(2);
   gPad->SetRightMargin(0.001);
   gPad->SetBottomMargin(0.3);
   gPad->SetPad(0,0,0.9,0.25);
   frame2->GetYaxis()->SetTitleOffset(1.6);
   frame2->GetXaxis()->SetTitleSize(.1);
   frame2->GetYaxis()->SetLabelSize(0.05);
   frame2->GetXaxis()->SetLabelSize(0.07);
   frame2->Draw();

TLine *w = new TLine(xmpp,0,ympp,0);
w->SetLineColor(kBlack);
//w->SetLineStyle(9);
w->Draw();

} 

