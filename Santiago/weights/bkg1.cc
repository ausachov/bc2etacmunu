#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cmath>
#include <time.h>

double Mass(double x[4], double y[4])
{
double r3=sqrt((x[0]+y[0])*(x[0]+y[0])-(x[1]+y[1])*(x[1]+y[1])-(x[2]+y[2])*(x[2]+y[2])-(x[3]+y[3])*(x[3]+y[3]));
return r3;
}

double MassHipot1(double x[4], double y[4], double m1, double m2)
{
double r3=sqrt((sqrt(m1*m1+x[1]*x[1]+x[2]*x[2]+x[3]*x[3])+sqrt(m2*m2+y[1]*y[1]+y[2]*y[2]+y[3]*y[3]))*(sqrt(m1*m1+x[1]*x[1]+x[2]*x[2]+x[3]*x[3])+sqrt(m2*m2+y[1]*y[1]+y[2]*y[2]+y[3]*y[3]))-(x[1]+y[1])*(x[1]+y[1])-(x[2]+y[2])*(x[2]+y[2])-(x[3]+y[3])*(x[3]+y[3]));
return r3;
}

void bkg1() 

{
TFile *f = TFile::Open("../../../ROOTFILES/selected_TriggerBase.root");
TTree *DecayTree   = (TTree*)f->Get("DecayTree");
TFile hfile("../txt/BKG1.root","RECREATE","Demo ROOT file with histograms & trees");

//----------- Create tree reader and its data containers---------------------
TTreeReader myReader("DecayTree", f);
//-------------------------Masa Jpsi y masa Bc corregida----------------------------------------
TTreeReaderValue<Double_t> jpsimass(myReader, "Jpsi_MM");
TTreeReaderValue<Double_t> bcMCORR(myReader, "Bc_LOKI_BPVCORRM");
TTreeReaderValue<Double_t> bcMCORRerr(myReader, "Bc_MCORRERR");
//-------------------------Stripping----------------------------------------
//-------Bc-------
TTreeReaderValue<Double_t> bcDIRA(myReader, "Bc_LOKI_DIRA");
TTreeReaderValue<Double_t> bcFlightdistance(myReader, "Bc_LOKI_FDCHI2");
TTreeReaderValue<Double_t> bcPt(myReader, "Bc_PT");
TTreeReaderValue<Double_t> bcEndvertex(myReader, "Bc_ENDVERTEX_CHI2");
TTreeReaderValue<Int_t> bcEndvertexNDOF(myReader, "Bc_ENDVERTEX_NDOF");
//-------ppbar-------
TTreeReaderValue<Double_t> jpsiDIRA(myReader, "Jpsi_LOKI_DIRA");
TTreeReaderValue<Double_t> jpsiFlightdistance(myReader, "Jpsi_LOKI_FDCHI2");
TTreeReaderValue<Double_t> jpsiEndvertex(myReader, "Jpsi_ENDVERTEX_CHI2");
TTreeReaderValue<Int_t> jpsiEndvertexNDOF(myReader, "Jpsi_ENDVERTEX_NDOF");
//-------p and pbar-------
TTreeReaderValue<Double_t> protonpTrackChi2(myReader, "ProtonP_TRACK_CHI2NDOF");
TTreeReaderValue<Double_t> protonpTrackGhost(myReader, "ProtonP_TRACK_GhostProb");
TTreeReaderValue<Double_t> protonpIp(myReader, "ProtonP_IPCHI2_OWNPV");
TTreeReaderValue<Bool_t> protonpismuon(myReader, "ProtonP_isMuon");
TTreeReaderValue<Double_t> protonpP(myReader, "ProtonP_P");
TTreeReaderValue<Double_t> protonpPt(myReader, "ProtonP_PT");
TTreeReaderValue<Double_t> protonpPIDp(myReader, "ProtonP_PIDp");
TTreeReaderValue<Double_t> protonpPIDk(myReader, "ProtonP_PIDK");

TTreeReaderValue<Double_t> protonnTrackChi2(myReader, "ProtonM_TRACK_CHI2NDOF");
TTreeReaderValue<Double_t> protonnTrackGhost(myReader, "ProtonM_TRACK_GhostProb");
TTreeReaderValue<Double_t> protonnIp(myReader, "ProtonM_IPCHI2_OWNPV");
TTreeReaderValue<Bool_t> protonnismuon(myReader, "ProtonM_isMuon");
TTreeReaderValue<Double_t> protonnP(myReader, "ProtonM_P");
TTreeReaderValue<Double_t> protonnPt(myReader, "ProtonM_PT");
TTreeReaderValue<Double_t> protonnPIDp(myReader, "ProtonM_PIDp");
TTreeReaderValue<Double_t> protonnPIDk(myReader, "ProtonM_PIDK");
//-------mu-------
TTreeReaderValue<Double_t> muTrackChi2(myReader, "Mu_TRACK_CHI2NDOF");
TTreeReaderValue<Double_t> muTrackGhost(myReader, "Mu_TRACK_GhostProb");
TTreeReaderValue<Double_t> muIp(myReader, "Mu_IPCHI2_OWNPV");
TTreeReaderValue<Bool_t> muismuon(myReader, "Mu_isMuon");
TTreeReaderValue<Double_t> muP(myReader, "Mu_P");
//-------GEC-------
TTreeReaderValue<Int_t> multiplicity(myReader, "nSPDHits");
//------------------------------Other cuts---------------------------------------------------------------------------------------------------
//-------------------Selección en triggers------------------------------------
//-----L0---------
TTreeReaderValue<Bool_t> triggerl01(myReader, "Bc_L0HadronDecision_TOS");
TTreeReaderValue<Bool_t> triggerl02(myReader, "Bc_L0MuonDecision_TOS");
//-----HLT1-------
TTreeReaderValue<Bool_t> triggerl11(myReader, "Bc_Hlt1TwoTrackMVADecision_TOS");
TTreeReaderValue<Bool_t> triggerl12(myReader, "Bc_Hlt1SingleMuonNoIPDecision_TOS");
//-----HLT2-------
TTreeReaderValue<Bool_t> triggerl212(myReader, "Bc_Hlt2TopoMu2BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl213(myReader, "Bc_Hlt2TopoMu3BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl222(myReader, "Bc_Hlt2Topo2BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl223(myReader, "Bc_Hlt2Topo3BodyDecision_TOS");
TTreeReaderValue<Bool_t> triggerl23(myReader, "Bc_Hlt2SingleMuonDecision_TOS");
TTreeReaderValue<Bool_t> triggerl24(myReader, "Bc_Hlt2XcMuXForTauB2XcMuDecision_TOS");
//---------------------Cortes en protones-----------------------------------
TTreeReaderValue<Double_t> protonpEta(myReader, "ProtonP_ETA");
TTreeReaderValue<Double_t> protonpPe(myReader, "ProtonP_PE");
TTreeReaderValue<Double_t> protonpPx(myReader, "ProtonP_PX");
TTreeReaderValue<Double_t> protonpPy(myReader, "ProtonP_PY");
TTreeReaderValue<Double_t> protonpPz(myReader, "ProtonP_PZ");
TTreeReaderValue<Double_t> protonpProbp(myReader, "ProtonP_ProbNNp");
TTreeReaderValue<Double_t> protonpProbk(myReader, "ProtonP_ProbNNk");
TTreeReaderValue<Double_t> protonpProbpi(myReader, "ProtonP_ProbNNpi");
TTreeReaderValue<Double_t> protonnEta(myReader, "ProtonM_ETA");
TTreeReaderValue<Double_t> protonnPe(myReader, "ProtonM_PE");
TTreeReaderValue<Double_t> protonnPx(myReader, "ProtonM_PX");
TTreeReaderValue<Double_t> protonnPy(myReader, "ProtonM_PY");
TTreeReaderValue<Double_t> protonnPz(myReader, "ProtonM_PZ");
TTreeReaderValue<Double_t> protonnProbp(myReader, "ProtonM_ProbNNp");
TTreeReaderValue<Double_t> protonnProbk(myReader, "ProtonM_ProbNNk");
TTreeReaderValue<Double_t> protonnProbpi(myReader, "ProtonM_ProbNNpi");
//---------------------Cortes en muones-------------------------------------
TTreeReaderValue<Double_t> muPe(myReader, "Mu_PE");
TTreeReaderValue<Double_t> muPx(myReader, "Mu_PX");
TTreeReaderValue<Double_t> muPy(myReader, "Mu_PY");
TTreeReaderValue<Double_t> muPz(myReader, "Mu_PZ");
TTreeReaderValue<Double_t> muOrivertex(myReader, "Mu_ORIVX_CHI2");
TTreeReaderValue<Double_t> muProbmu(myReader, "Mu_ProbNNmu");
//--------------------Cortes en charmonia------------------------------------
TTreeReaderValue<Double_t> jpsiEta(myReader, "Jpsi_ETA");
TTreeReaderValue<Double_t> jpsiP(myReader, "Jpsi_P");
TTreeReaderValue<Double_t> jpsiPt(myReader, "Jpsi_PT");
TTreeReaderValue<Double_t> jpsiPx(myReader, "Jpsi_PX");
TTreeReaderValue<Double_t> jpsiPy(myReader, "Jpsi_PY");
TTreeReaderValue<Double_t> jpsiPz(myReader, "Jpsi_PZ");
//------------Selección en posición vertice secundario------------------------------------
TTreeReaderValue<Double_t> bcX(myReader, "Bc_ENDVERTEX_X");
TTreeReaderValue<Double_t> bcY(myReader, "Bc_ENDVERTEX_Y");
TTreeReaderValue<Double_t> bcZ(myReader, "Bc_ENDVERTEX_Z");
TTreeReaderValue<Double_t> bcXerr(myReader, "Bc_ENDVERTEX_XERR");
TTreeReaderValue<Double_t> bcYerr(myReader, "Bc_ENDVERTEX_YERR");
TTreeReaderValue<Double_t> bcZerr(myReader, "Bc_ENDVERTEX_ZERR");
TTreeReaderValue<Double_t> protonpX(myReader, "ProtonP_ORIVX_X");
TTreeReaderValue<Double_t> protonpY(myReader, "ProtonP_ORIVX_Y");
TTreeReaderValue<Double_t> protonpZ(myReader, "ProtonP_ORIVX_Z");
TTreeReaderValue<Double_t> protonpXerr(myReader, "ProtonP_ORIVX_XERR");
TTreeReaderValue<Double_t> protonpYerr(myReader, "ProtonP_ORIVX_YERR");
TTreeReaderValue<Double_t> protonpZerr(myReader, "ProtonP_ORIVX_ZERR");
TTreeReaderValue<Double_t> protonnX(myReader, "ProtonM_ORIVX_X");
TTreeReaderValue<Double_t> protonnY(myReader, "ProtonM_ORIVX_Y");
TTreeReaderValue<Double_t> protonnZ(myReader, "ProtonM_ORIVX_Z");
TTreeReaderValue<Double_t> protonnXerr(myReader, "ProtonM_ORIVX_XERR");
TTreeReaderValue<Double_t> protonnYerr(myReader, "ProtonM_ORIVX_YERR");
TTreeReaderValue<Double_t> protonnZerr(myReader, "ProtonM_ORIVX_ZERR");
TTreeReaderValue<Double_t> muX(myReader, "Mu_ORIVX_X");
TTreeReaderValue<Double_t> muY(myReader, "Mu_ORIVX_Y");
TTreeReaderValue<Double_t> muZ(myReader, "Mu_ORIVX_Z");
TTreeReaderValue<Double_t> muXerr(myReader, "Mu_ORIVX_XERR");
TTreeReaderValue<Double_t> muYerr(myReader, "Mu_ORIVX_YERR");
TTreeReaderValue<Double_t> muZerr(myReader, "Mu_ORIVX_ZERR");
//-------------------------Cortes Bc----------------------------------------
TTreeReaderValue<Double_t> bciX(myReader, "Bc_OWNPV_X");
TTreeReaderValue<Double_t> bciY(myReader, "Bc_OWNPV_Y");
TTreeReaderValue<Double_t> bciZ(myReader, "Bc_OWNPV_Z");
TTreeReaderValue<Double_t> bciXerr(myReader, "Bc_OWNPV_XERR");
TTreeReaderValue<Double_t> bciYerr(myReader, "Bc_OWNPV_YERR");
TTreeReaderValue<Double_t> bciZerr(myReader, "Bc_OWNPV_ZERR");
//---------------------------------------------------------------------------

TTree *T1 = new TTree("T1","T1");

float vjpsimass; T1->Branch("Jpsi_MM",&vjpsimass,"Jpsi_MM/F");
//--------------------
float vprotonpPt; T1->Branch("ProtonP_PT",&vprotonpPt,"ProtonP_PT/F"); 
float vprotonpTrack; T1->Branch("ProtonP_TRACK_CHI2NDOF",&vprotonpTrack,"ProtonP_TRACK_CHI2NDOF/F");
float vprotonpIp; T1->Branch("ProtonP_IPCHI2_OWNPV",&vprotonpIp,"ProtonP_IPCHI2_OWNPV/F");
float vprotonpProbp; T1->Branch("ProtonP_MC15TuneV1_ProbNNp_corr",&vprotonpProbp,"ProtonP_MC15TuneV1_ProbNNp_corr/F");
float vprotonpProbk; T1->Branch("ProtonP_MC15TuneV1_ProbNNk_corr",&vprotonpProbk,"ProtonP_MC15TuneV1_ProbNNk_corr/F");
float vprotonpProbpi; T1->Branch("ProtonP_MC15TuneV1_ProbNNpi_corr",&vprotonpProbpi,"ProtonP_MC15TuneV1_ProbNNpi_corr/F"); 
float vprotonpPz; T1->Branch("ProtonP_PZ",&vprotonpPz,"ProtonP_PZ/F"); 
//----------------------
float vprotonnPt; T1->Branch("ProtonM_PT",&vprotonnPt,"ProtonM_PT/F"); 
float vprotonnTrack; T1->Branch("ProtonM_TRACK_CHI2NDOF",&vprotonnTrack,"ProtonM_TRACK_CHI2NDOF/F"); 
float vprotonnIp; T1->Branch("ProtonM_IPCHI2_OWNPV",&vprotonnIp,"ProtonM_IPCHI2_OWNPV/F"); 
float vprotonnProbp; T1->Branch("ProtonM_MC15TuneV1_ProbNNp_corr",&vprotonnProbp,"ProtonM_MC15TuneV1_ProbNNp_corr/F"); 
float vprotonnProbk; T1->Branch("ProtonM_MC15TuneV1_ProbNNk_corr",&vprotonnProbk,"ProtonM_MC15TuneV1_ProbNNk_corr/F");
float vprotonnProbpi; T1->Branch("ProtonM_MC15TuneV1_ProbNNpi_corr",&vprotonnProbpi,"ProtonM_MC15TuneV1_ProbNNpi_corr/F"); 
float vprotonnPz; T1->Branch("ProtonM_PZ",&vprotonnPz,"ProtonM_PZ/F");
//----------------------
float vjpsiEndvertex; T1->Branch("Jpsi_ENDVERTEX_CHI2",&vjpsiEndvertex,"Jpsi_ENDVERTEX_CHI2/F");
float vjpsiFlightdistance; T1->Branch("Jpsi_LOKI_FDCHI2",&vjpsiFlightdistance,"Jpsi_LOKI_FDCHI2/F");
float vjpsiPt; T1->Branch("Jpsi_PT",&vjpsiPt,"Jpsi_PT/F");
float vjpsiPz; T1->Branch("Jpsi_PZ",&vjpsiPz,"Jpsi_PZ/F");
//----------------------
float vmuTrack; T1->Branch("Mu_TRACK_CHI2NDOF",&vmuTrack,"Mu_TRACK_CHI2NDOF/F");
float vmuProbmu; T1->Branch("Mu_MC15TuneV1_ProbNNmu_corr",&vmuProbmu,"Mu_MC15TuneV1_ProbNNmu_corr/F"); 
//----------------------
float vbcMCORR; T1->Branch("Bc_LOKI_BPVCORRM",&vbcMCORR,"Bc_LOKI_BPVCORRM/F");
float vbcFlightdistance; T1->Branch("Bc_LOKI_FDCHI2",&vbcFlightdistance,"Bc_LOKI_FDCHI2/F");
float vbcEndvertex; T1->Branch("Bc_ENDVERTEX_CHI2",&vbcEndvertex,"Bc_ENDVERTEX_CHI2/F");
//-----------------------------
float vMinProbNNp; T1->Branch("Minimal_ProbNNp",&vMinProbNNp,"Minimal_ProbNNp/F");
float vSumProbNNp; T1->Branch("Sum_ProbNNp",&vSumProbNNp,"Sum_ProbNNp/F");
//----------------------
float vMinprotonIp; T1->Branch("Minimal_Proton_IPCHI2_OWNPV",&vMinprotonIp,"Minimal_Proton_IPCHI2_OWNPV/F");
//----------------------
float vMinprotonPt; T1->Branch("Minimal_Proton_PT",&vMinprotonPt,"Minimal_Proton_PT/F");
float vSumprotonPt; T1->Branch("Sum_Proton_PT",&vSumprotonPt,"Sum_Proton_PT/F");
float vAsimprotonPt; T1->Branch("Asim_Proton_PT",&vAsimprotonPt,"Asim_Proton_PT/F");
//----------------------
float vMinprotonPz; T1->Branch("Minimal_Proton_PZ",&vMinprotonPz,"Minimal_Proton_PZ/F");
float vSumprotonPz; T1->Branch("Sum_Proton_PZ",&vSumprotonPz,"Sum_Proton_PZ/F");
float vAsimprotonPz; T1->Branch("Asim_Proton_PZ",&vAsimprotonPz,"Asim_Proton_PZ/F");
//----------------------
float vMaxprotonTrackChi2; T1->Branch("Maximal_Proton_TRACK_CHI2NDOF",&vMaxprotonTrackChi2,"Maximal_Proton_TRACK_CHI2NDOF/F");
//----------------------
float vSumprotonP; T1->Branch("SumProton_P",&vSumprotonP,"SumProton_P/F");
//----------------------
float vjpsiEta; T1->Branch("Jpsi_ETA",&vjpsiEta,"Jpsi_ETA/F");
//----------------------
float vCosThetaPandPbar; T1->Branch("CosAngle_ProtonMandProtonP",&vCosThetaPandPbar,"CosAngle_ProtonMandProtonP/F");
float vCosThetaPpbarandMuon; T1->Branch("CosAngle_PpbarandMuon",&vCosThetaPpbarandMuon,"CosAngle_PpbarandMuon/F");


//-------------------------------------------------------------------------------------------------------------------------------
TTree *T2 = new TTree("T2","T2");

T2->Branch("Jpsi_MM",&vjpsimass,"Jpsi_MM/F");
//--------------------
T2->Branch("ProtonP_PT",&vprotonpPt,"ProtonP_PT/F"); 
T2->Branch("ProtonP_TRACK_CHI2NDOF",&vprotonpTrack,"ProtonP_TRACK_CHI2NDOF/F");
T2->Branch("ProtonP_IPCHI2_OWNPV",&vprotonpIp,"ProtonP_IPCHI2_OWNPV/F");
T2->Branch("ProtonP_MC15TuneV1_ProbNNp_corr",&vprotonpProbp,"ProtonP_MC15TuneV1_ProbNNp_corr/F");
T2->Branch("ProtonP_MC15TuneV1_ProbNNk_corr",&vprotonpProbk,"ProtonP_MC15TuneV1_ProbNNk_corr/F");
T2->Branch("ProtonP_MC15TuneV1_ProbNNpi_corr",&vprotonpProbpi,"ProtonP_MC15TuneV1_ProbNNpi_corr/F"); 
T2->Branch("ProtonP_PZ",&vprotonpPz,"ProtonP_PZ/F"); 
//----------------------
T2->Branch("ProtonM_PT",&vprotonnPt,"ProtonM_PT/F"); 
T2->Branch("ProtonM_TRACK_CHI2NDOF",&vprotonnTrack,"ProtonM_TRACK_CHI2NDOF/F"); 
T2->Branch("ProtonM_IPCHI2_OWNPV",&vprotonnIp,"ProtonM_IPCHI2_OWNPV/F"); 
T2->Branch("ProtonM_MC15TuneV1_ProbNNp_corr",&vprotonnProbp,"ProtonM_MC15TuneV1_ProbNNp_corr/F"); 
T2->Branch("ProtonM_MC15TuneV1_ProbNNk_corr",&vprotonnProbk,"ProtonM_MC15TuneV1_ProbNNk_corr/F");
T2->Branch("ProtonM_MC15TuneV1_ProbNNpi_corr",&vprotonnProbpi,"ProtonM_MC15TuneV1_ProbNNpi_corr/F"); 
T2->Branch("ProtonM_PZ",&vprotonnPz,"ProtonM_PZ/F");
//----------------------
T2->Branch("Jpsi_ENDVERTEX_CHI2",&vjpsiEndvertex,"Jpsi_ENDVERTEX_CHI2/F");
T2->Branch("Jpsi_LOKI_FDCHI2",&vjpsiFlightdistance,"Jpsi_LOKI_FDCHI2/F");
T2->Branch("Jpsi_PT",&vjpsiPt,"Jpsi_PT/F");
T2->Branch("Jpsi_PZ",&vjpsiPz,"Jpsi_PZ/F");
//----------------------
T2->Branch("Mu_TRACK_CHI2NDOF",&vmuTrack,"Mu_TRACK_CHI2NDOF/F");
T2->Branch("Mu_MC15TuneV1_ProbNNmu_corr",&vmuProbmu,"Mu_MC15TuneV1_ProbNNmu_corr/F"); 
//----------------------
T2->Branch("Bc_LOKI_BPVCORRM",&vbcMCORR,"Bc_LOKI_BPVCORRM/F");
T2->Branch("Bc_LOKI_FDCHI2",&vbcFlightdistance,"Bc_LOKI_FDCHI2/F");
T2->Branch("Bc_ENDVERTEX_CHI2",&vbcEndvertex,"Bc_ENDVERTEX_CHI2/F");
//-----------------------------
T2->Branch("Minimal_ProbNNp",&vMinProbNNp,"Minimal_ProbNNp/F");
T2->Branch("Sum_ProbNNp",&vSumProbNNp,"Sum_ProbNNp/F");
//----------------------
T2->Branch("Minimal_Proton_IPCHI2_OWNPV",&vMinprotonIp,"Minimal_Proton_IPCHI2_OWNPV/F");
//----------------------
T2->Branch("Minimal_Proton_PT",&vMinprotonPt,"Minimal_Proton_PT/F");
T2->Branch("Sum_Proton_PT",&vSumprotonPt,"Sum_Proton_PT/F");
T2->Branch("Asim_Proton_PT",&vAsimprotonPt,"Asim_Proton_PT/F");
//----------------------
T2->Branch("Minimal_Proton_PZ",&vMinprotonPz,"Minimal_Proton_PZ/F");
T2->Branch("Sum_Proton_PZ",&vSumprotonPz,"Sum_Proton_PZ/F");
T2->Branch("Asim_Proton_PZ",&vAsimprotonPz,"Asim_Proton_PZ/F");
//----------------------
T2->Branch("Maximal_Proton_TRACK_CHI2NDOF",&vMaxprotonTrackChi2,"Maximal_Proton_TRACK_CHI2NDOF/F");
//----------------------
T2->Branch("SumProton_P",&vSumprotonP,"SumProton_P/F");
//----------------------
T2->Branch("Jpsi_ETA",&vjpsiEta,"Jpsi_ETA/F");
//----------------------
T2->Branch("CosAngle_ProtonMandProtonP",&vCosThetaPandPbar,"CosAngle_ProtonMandProtonP/F");
T2->Branch("CosAngle_PpbarandMuon",&vCosThetaPpbarandMuon,"CosAngle_PpbarandMuon/F");


/*//-------------------------------------------------------------------------------------------------------------------------------
TTree *T3 = new TTree("T3","T3");

T3->Branch("Jpsi_MM",&vjpsimass,"Jpsi_MM/F");
//--------------------
T3->Branch("ProtonP_PT",&vprotonpPt,"ProtonP_PT/F"); 
T3->Branch("ProtonP_TRACK_CHI2NDOF",&vprotonpTrack,"ProtonP_TRACK_CHI2NDOF/F");
T3->Branch("ProtonP_IPCHI2_OWNPV",&vprotonpIp,"ProtonP_IPCHI2_OWNPV/F");
T3->Branch("ProtonP_MC15TuneV1_ProbNNp_corr",&vprotonpProbp,"ProtonP_MC15TuneV1_ProbNNp_corr/F");
T3->Branch("ProtonP_MC15TuneV1_ProbNNk_corr",&vprotonpProbk,"ProtonP_MC15TuneV1_ProbNNk_corr/F");
T3->Branch("ProtonP_MC15TuneV1_ProbNNpi_corr",&vprotonpProbpi,"ProtonP_MC15TuneV1_ProbNNpi_corr/F"); 
T3->Branch("ProtonP_PZ",&vprotonpPz,"ProtonP_PZ/F"); 
//----------------------
T3->Branch("ProtonM_PT",&vprotonnPt,"ProtonM_PT/F"); 
T3->Branch("ProtonM_TRACK_CHI2NDOF",&vprotonnTrack,"ProtonM_TRACK_CHI2NDOF/F"); 
T3->Branch("ProtonM_IPCHI2_OWNPV",&vprotonnIp,"ProtonM_IPCHI2_OWNPV/F"); 
T3->Branch("ProtonM_MC15TuneV1_ProbNNp_corr",&vprotonnProbp,"ProtonM_MC15TuneV1_ProbNNp_corr/F"); 
T3->Branch("ProtonM_MC15TuneV1_ProbNNk_corr",&vprotonnProbk,"ProtonM_MC15TuneV1_ProbNNk_corr/F");
T3->Branch("ProtonM_MC15TuneV1_ProbNNpi_corr",&vprotonnProbpi,"ProtonM_MC15TuneV1_ProbNNpi_corr/F"); 
T3->Branch("ProtonM_PZ",&vprotonnPz,"ProtonM_PZ/F");
//----------------------
T3->Branch("Jpsi_ENDVERTEX_CHI2",&vjpsiEndvertex,"Jpsi_ENDVERTEX_CHI2/F");
T3->Branch("Jpsi_LOKI_FDCHI2",&vjpsiFlightdistance,"Jpsi_LOKI_FDCHI2/F");
T3->Branch("Jpsi_PT",&vjpsiPt,"Jpsi_PT/F");
T3->Branch("Jpsi_PZ",&vjpsiPz,"Jpsi_PZ/F");
//----------------------
T3->Branch("Mu_TRACK_CHI2NDOF",&vmuTrack,"Mu_TRACK_CHI2NDOF/F");
T3->Branch("Mu_MC15TuneV1_ProbNNmu_corr",&vmuProbmu,"Mu_MC15TuneV1_ProbNNmu_corr/F"); 
//----------------------
T3->Branch("Bc_LOKI_BPVCORRM",&vbcMCORR,"Bc_LOKI_BPVCORRM/F");
T3->Branch("Bc_LOKI_FDCHI2",&vbcFlightdistance,"Bc_LOKI_FDCHI2/F");
T3->Branch("Bc_ENDVERTEX_CHI2",&vbcEndvertex,"Bc_ENDVERTEX_CHI2/F");
//-----------------------------
T3->Branch("Minimal_ProbNNp",&vMinProbNNp,"Minimal_ProbNNp/F");
T3->Branch("Sum_ProbNNp",&vSumProbNNp,"Sum_ProbNNp/F");
//----------------------
T3->Branch("Minimal_Proton_IPCHI2_OWNPV",&vMinprotonIp,"Minimal_Proton_IPCHI2_OWNPV/F");
//----------------------
T3->Branch("Minimal_Proton_PT",&vMinprotonPt,"Minimal_Proton_PT/F");
T3->Branch("Sum_Proton_PT",&vSumprotonPt,"Sum_Proton_PT/F");
T3->Branch("Asim_Proton_PT",&vAsimprotonPt,"Asim_Proton_PT/F");
//----------------------
T3->Branch("Minimal_Proton_PZ",&vMinprotonPz,"Minimal_Proton_PZ/F");
T3->Branch("Sum_Proton_PZ",&vSumprotonPz,"Sum_Proton_PZ/F");
T3->Branch("Asim_Proton_PZ",&vAsimprotonPz,"Asim_Proton_PZ/F");
//----------------------
T3->Branch("Maximal_Proton_TRACK_CHI2NDOF",&vMaxprotonTrackChi2,"Maximal_Proton_TRACK_CHI2NDOF/F");
//----------------------
T3->Branch("SumProton_P",&vSumprotonP,"SumProton_P/F");
//----------------------
T3->Branch("Jpsi_ETA",&vjpsiEta,"Jpsi_ETA/F");
//----------------------
T3->Branch("CosAngle_ProtonMandProtonP",&vCosThetaPandPbar,"CosAngle_ProtonMandProtonP/F");
T3->Branch("CosAngle_PpbarandMuon",&vCosThetaPpbarandMuon,"CosAngle_PpbarandMuon/F");


//-------------------------------------------------------------------------------------------------------------------------------
TTree *T4 = new TTree("T4","T4");

T4->Branch("Jpsi_MM",&vjpsimass,"Jpsi_MM/F");
//--------------------
T4->Branch("ProtonP_PT",&vprotonpPt,"ProtonP_PT/F"); 
T4->Branch("ProtonP_TRACK_CHI2NDOF",&vprotonpTrack,"ProtonP_TRACK_CHI2NDOF/F");
T4->Branch("ProtonP_IPCHI2_OWNPV",&vprotonpIp,"ProtonP_IPCHI2_OWNPV/F");
T4->Branch("ProtonP_MC15TuneV1_ProbNNp_corr",&vprotonpProbp,"ProtonP_MC15TuneV1_ProbNNp_corr/F");
T4->Branch("ProtonP_MC15TuneV1_ProbNNk_corr",&vprotonpProbk,"ProtonP_MC15TuneV1_ProbNNk_corr/F");
T4->Branch("ProtonP_MC15TuneV1_ProbNNpi_corr",&vprotonpProbpi,"ProtonP_MC15TuneV1_ProbNNpi_corr/F"); 
T4->Branch("ProtonP_PZ",&vprotonpPz,"ProtonP_PZ/F"); 
//----------------------
T4->Branch("ProtonM_PT",&vprotonnPt,"ProtonM_PT/F"); 
T4->Branch("ProtonM_TRACK_CHI2NDOF",&vprotonnTrack,"ProtonM_TRACK_CHI2NDOF/F"); 
T4->Branch("ProtonM_IPCHI2_OWNPV",&vprotonnIp,"ProtonM_IPCHI2_OWNPV/F"); 
T4->Branch("ProtonM_MC15TuneV1_ProbNNp_corr",&vprotonnProbp,"ProtonM_MC15TuneV1_ProbNNp_corr/F"); 
T4->Branch("ProtonM_MC15TuneV1_ProbNNk_corr",&vprotonnProbk,"ProtonM_MC15TuneV1_ProbNNk_corr/F");
T4->Branch("ProtonM_MC15TuneV1_ProbNNpi_corr",&vprotonnProbpi,"ProtonM_MC15TuneV1_ProbNNpi_corr/F"); 
T4->Branch("ProtonM_PZ",&vprotonnPz,"ProtonM_PZ/F");
//----------------------
T4->Branch("Jpsi_ENDVERTEX_CHI2",&vjpsiEndvertex,"Jpsi_ENDVERTEX_CHI2/F");
T4->Branch("Jpsi_LOKI_FDCHI2",&vjpsiFlightdistance,"Jpsi_LOKI_FDCHI2/F");
T4->Branch("Jpsi_PT",&vjpsiPt,"Jpsi_PT/F");
T4->Branch("Jpsi_PZ",&vjpsiPz,"Jpsi_PZ/F");
//----------------------
T4->Branch("Mu_TRACK_CHI2NDOF",&vmuTrack,"Mu_TRACK_CHI2NDOF/F");
T4->Branch("Mu_MC15TuneV1_ProbNNmu_corr",&vmuProbmu,"Mu_MC15TuneV1_ProbNNmu_corr/F"); 
//----------------------
T4->Branch("Bc_LOKI_BPVCORRM",&vbcMCORR,"Bc_LOKI_BPVCORRM/F");
T4->Branch("Bc_LOKI_FDCHI2",&vbcFlightdistance,"Bc_LOKI_FDCHI2/F");
T4->Branch("Bc_ENDVERTEX_CHI2",&vbcEndvertex,"Bc_ENDVERTEX_CHI2/F");
//-----------------------------
T4->Branch("Minimal_ProbNNp",&vMinProbNNp,"Minimal_ProbNNp/F");
T4->Branch("Sum_ProbNNp",&vSumProbNNp,"Sum_ProbNNp/F");
//----------------------
T4->Branch("Minimal_Proton_IPCHI2_OWNPV",&vMinprotonIp,"Minimal_Proton_IPCHI2_OWNPV/F");
//----------------------
T4->Branch("Minimal_Proton_PT",&vMinprotonPt,"Minimal_Proton_PT/F");
T4->Branch("Sum_Proton_PT",&vSumprotonPt,"Sum_Proton_PT/F");
T4->Branch("Asim_Proton_PT",&vAsimprotonPt,"Asim_Proton_PT/F");
//----------------------
T4->Branch("Minimal_Proton_PZ",&vMinprotonPz,"Minimal_Proton_PZ/F");
T4->Branch("Sum_Proton_PZ",&vSumprotonPz,"Sum_Proton_PZ/F");
T4->Branch("Asim_Proton_PZ",&vAsimprotonPz,"Asim_Proton_PZ/F");
//----------------------
T4->Branch("Maximal_Proton_TRACK_CHI2NDOF",&vMaxprotonTrackChi2,"Maximal_Proton_TRACK_CHI2NDOF/F");
//----------------------
T4->Branch("SumProton_P",&vSumprotonP,"SumProton_P/F");
//----------------------
T4->Branch("Jpsi_ETA",&vjpsiEta,"Jpsi_ETA/F");
//----------------------
T4->Branch("CosAngle_ProtonMandProtonP",&vCosThetaPandPbar,"CosAngle_ProtonMandProtonP/F");
T4->Branch("CosAngle_PpbarandMuon",&vCosThetaPpbarandMuon,"CosAngle_PpbarandMuon/F");*/



//-----------------------------------------------------------------------------------------------------------------
int num=0;
while (myReader.Next()){
double cuadrimomPN[4]={*protonnPe,*protonnPx,*protonnPy,*protonnPz};
double cuadrimomPP[4]={*protonpPe,*protonpPx,*protonpPy,*protonpPz};
double cuadrimomMU[4]={*muPe,*muPx,*muPy,*muPz};

if((((*bcEndvertex)/(*bcEndvertexNDOF))<15)&&(*bcDIRA>0.999)&&(*bcFlightdistance>50)&&(Mass(cuadrimomPP,cuadrimomMU)>1000)&&(*bcPt>1500)&&(((*jpsiEndvertex)/(*jpsiEndvertexNDOF))<8)&&(*jpsiDIRA>0.999)&&(*jpsiFlightdistance>25)&&(*protonpTrackChi2<3)&&(*protonnTrackChi2<3)&&(*protonpTrackGhost<0.35)&&(*protonnTrackGhost<0.35)&&(*protonpIp>9)&&(*protonnIp>9)&&(*protonpismuon==false)&&(*protonnismuon==false)&&(*protonpP>15000)&&(*protonnP>15000)&&(*protonpPt>800)&&(*protonnPt>800)&&(*protonpPIDp>2)&&(*protonnPIDp>2)&&(((*protonpPIDp)-(*protonpPIDk))>2)&&(((*protonnPIDp)-(*protonnPIDk))>2)&&(*muTrackChi2<4)&&(*muTrackGhost<0.35)&&(*muIp>16)&&(*muismuon==true)&&(*muP>3000)&&(*multiplicity<600)&&((*triggerl01==true)||(*triggerl02==true))&&(*triggerl11==true)&&((*triggerl212==true)||(*triggerl213==true)||(*triggerl222==true)||(*triggerl223==true)||(*triggerl23==true)||(*triggerl24==true)))
{
if((*bcMCORR<25000)&&(*bcMCORR>0)&&(*muProbmu>0.2)&&(((*bcEndvertex)/(*bcEndvertexNDOF))<9)&&(*bcMCORRerr<900)&&(*protonpP<150000)&&(*protonnP<150000)&&((MassHipot1(cuadrimomPP,cuadrimomPN,139.57,139.57)<1770)||(MassHipot1(cuadrimomPP,cuadrimomPN,139.57,139.57)>1805))&&((MassHipot1(cuadrimomPP,cuadrimomPN,493.68,493.68)<1920)||(MassHipot1(cuadrimomPP,cuadrimomPN,493.68,493.68)>1950))&&((*jpsimass>3130)||(*jpsimass<2840)))
{

//------------------------------------------
vjpsimass=*jpsimass; 
//--
vprotonpPt=*protonpPt; 
vprotonpTrack=*protonpTrackChi2; 
vprotonpIp=*protonpIp; 
vprotonpProbp=*protonpProbp; 
vprotonpProbk=*protonpProbk;  
vprotonpProbpi=*protonpProbpi; 
vprotonpPz=*protonpPz;
//--
vprotonnPt=*protonnPt; 
vprotonnTrack=*protonnTrackChi2; 
vprotonnIp=*protonnIp; 
vprotonnProbp=*protonnProbp; 
vprotonnProbk=*protonnProbk;
vprotonnProbpi=*protonnProbpi; 
vprotonnPz=*protonnPz;
//--
vjpsiEndvertex=*jpsiEndvertex; 
vjpsiFlightdistance=*jpsiFlightdistance;
vjpsiPt=*jpsiPt;
vjpsiPz=*jpsiPz;
//--
vmuTrack=*muTrackChi2;
vmuProbmu=*muProbmu;
//--
vbcMCORR=*bcMCORR; 
vbcFlightdistance=*bcFlightdistance;
vbcEndvertex=*bcEndvertex;
//--
if((*protonpProbp)<(*protonnProbp)){vMinProbNNp=*protonpProbp;}else{vMinProbNNp=*protonnProbp;}
vSumProbNNp=(*protonpProbp)+(*protonnProbp);
//--
if((*protonpIp)<(*protonnIp)){vMinprotonIp=*protonpIp;}else{vMinprotonIp=*protonnIp;}
//--
if((*protonpPt)<(*protonnPt)){vMinprotonPt=*protonpPt;}else{vMinprotonPt=*protonnPt;}
vSumprotonPt=(*protonpPt)+(*protonnPt);
vAsimprotonPt=((*protonpPt)-(*protonnPt))/((*protonpPt)+(*protonnPt));
//--
if((*protonpPz)<(*protonnPz)){vMinprotonPz=*protonpPz;}else{vMinprotonPz=*protonnPz;}
vSumprotonPz=(*protonpPz)+(*protonnPz);
vAsimprotonPz=((*protonpPz)-(*protonnPz))/((*protonpPz)+(*protonnPz));
//--
if((*protonpTrackChi2)<(*protonnTrackChi2)){vMaxprotonTrackChi2=*protonnTrackChi2;}else{vMaxprotonTrackChi2=*protonpTrackChi2;}
//--
vSumprotonP=(*protonpP)+(*protonnP); 
//--
vjpsiEta=*jpsiEta;
//--
vCosThetaPandPbar=((*protonpPx)*(*protonnPx)+(*protonpPy)*(*protonnPy)+(*protonpPz)*(*protonnPz))/((*protonpP)*(*protonnP));
vCosThetaPpbarandMuon=((*muPx)*(*jpsiPx)+(*muPy)*(*jpsiPy)+(*muPz)*(*jpsiPz))/((*muP)*(*jpsiP));
//-----------------------------------------
     if(num<(1*DecayTree->GetEntries()/2)){T1->Fill();}
else if(num<(2*DecayTree->GetEntries()/2)){T2->Fill();}
//-----------------------------------------
/*if((*protonpPt>1720)&&(*protonnPt>1720)&&(*protonnProbp>0.94)&&(*protonpProbp>0.94))
{
     if(num<(1*DecayTree->GetEntries()/2)){T3->Fill();}
else if(num<(2*DecayTree->GetEntries()/2)){T4->Fill();}
}*/

}
}
num++;
if(num%100000==0){std::cout<<num<<std::endl;}
}

std::cout<<"Número de eventos leidos: "<<num<<std::endl;

//-----------------------------------------------------------------------------------------------------------------

   T1->Print();
   T2->Print();
   //T3->Print();
   //T4->Print();
   hfile.Write();
   hfile.Close();
}
