from ROOT import *

import sys
sys.path.insert(0, '../')
from makeConfigDictionary import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");

def getData_d(w, iPT=0):
    
    nPTBins = 7
    pt = (['5500', '6500'],
          ['6500', '8000'], 
          ['8000', '10000'], 
          ['10000', '12000'], 
          ['12000', '14000'], 
          ['14000', '18000'],
          ['18000', '30000'])

    

    ntEtac_FromB = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')

    ntEtac_FromB.Add("../MC/tuples/selected/secondaryTopoOpt/incl_b_Sec_secondaryTopoOpt_allSelected_AddBr.root")
    ntJpsi_FromB.Add("../MC/tuples/selected/secondaryTopoOpt/Jpsi_Sec_secondaryTopoOpt_allSelected_AddBr.root")
    
    
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
    
    if(iPT == 0):
        cut = cutsDict["secondaryTopoOpt"]
        treeEtac_FromB = ntEtac_FromB.CopyTree("Jpsi_sec && "+cut)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree("Jpsi_sec && "+cut)
        
    elif ( iPT <= nPTBins ):
        cutJpsiPtL = 'Jpsi_PT > %s'%(pt[iPT-1][0])
        cutJpsiPtR = 'Jpsi_PT < %s'%(pt[iPT-1][1])
        cut = cutsDict["secondaryTopoOpt"]
        treeEtac_FromB = ntEtac_FromB.CopyTree("Jpsi_sec && "+cutJpsiPtL + '&&' + cutJpsiPtR + '&&' + cut)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree("Jpsi_sec && "+cutJpsiPtL + '&&' + cutJpsiPtR + '&&' + cut)
      
    else:
        print ('Incorrect number of PT bin %s'%(iPT))
    
    
    Jpsi_M_res = RooRealVar ('Jpsi_M_res','Jpsi_M_res',-100.0,100.0) 
    dsEtac_FromB = RooDataSet('dsEtac_FromB','dsEtac_FromB',treeEtac_FromB,RooArgSet(Jpsi_M_res))
    dsJpsi_FromB = RooDataSet('dsJpsi_FromB','dsJpsi_FromB',treeJpsi_FromB,RooArgSet(Jpsi_M_res))
    getattr(w,'import')(dsEtac_FromB)
    getattr(w,'import')(dsJpsi_FromB)
    

    


def fillRelWorkspace(w):
    
    Jpsi_M_res = w.var('Jpsi_M_res')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70

    
    
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)


    nEtac_FromB = RooRealVar('nEtac_FromB','num of Etac', 1e3, 10, 1.e5)
    nJpsi_FromB = RooRealVar('nJpsi_FromB','num of J/Psi', 2e3, 10, 1.e5)
    nEtacRel_FromB = RooRealVar('nEtacRel','num of Etac', 0.0, 3.0)
    


    nEtac_FromB_1 = RooFormulaVar('nEtac_FromB_1','num of Etac','@0*@1',RooArgList(nEtac_FromB,rG1toG2))
    nEtac_FromB_2 = RooFormulaVar('nEtac_FromB_2','num of Etac','@0-@1',RooArgList(nEtac_FromB,nEtac_FromB_1))
    nJpsi_FromB_1 = RooFormulaVar('nJpsi_FromB_1','num of J/Psi','@0*@1',RooArgList(nJpsi_FromB,rG1toG2))
    nJpsi_FromB_2 = RooFormulaVar('nJpsi_FromB_2','num of J/Psi','@0-@1',RooArgList(nJpsi_FromB,nJpsi_FromB_1))

    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 0.0, -50.0, 50.0) 

    
    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    
    gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M_res, mean_Etac,  sigma_eta_1) #mean_Etac -> mean_Jpsi
    gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M_res, mean_Etac,  sigma_eta_2) #mean_Etac -> mean_Jpsi
    
    
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2) 
    

    
    modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(gaussEta_1, gaussEta_2), RooArgList(nEtac_FromB_1, nEtac_FromB_2))
    modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_FromB_1, nJpsi_FromB_2))

    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac_FromB') 
    sample.defineType('Jpsi_FromB') 
    
    
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import('Etac_FromB',dataEtac_FromB), RooFit.Import('Jpsi_FromB',dataJpsi_FromB)) 
    
    
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac_FromB,'Etac_FromB') 
    simPdf.addPdf(modelJpsi_FromB,'Jpsi_FromB') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)
    


def fillRelWorkspaceCB(w):

    
    Jpsi_M_res = w.var('Jpsi_M_res')
    #Jpsi_M_res.setBins(1000,'cache')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70
    
    
    
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)

    nEtac_FromB = RooRealVar('nEtac_FromB','num of Etac', 1e3, 10, 1.e5)
    nJpsi_FromB = RooRealVar('nJpsi_FromB','num of J/Psi', 2e3, 10, 1.e5)
    nEtacRel_FromB = RooRealVar('nEtacRel','num of Etac', 0.0, 3.0)
    
    
    nEtac_FromB_1 = RooFormulaVar('nEtac_FromB_1','num of Etac','@0*@1',RooArgList(nEtac_FromB,rG1toG2))
    nEtac_FromB_2 = RooFormulaVar('nEtac_FromB_2','num of Etac','@0-@1',RooArgList(nEtac_FromB,nEtac_FromB_1))
    nJpsi_FromB_1 = RooFormulaVar('nJpsi_FromB_1','num of J/Psi','@0*@1',RooArgList(nJpsi_FromB,rG1toG2))
    nJpsi_FromB_2 = RooFormulaVar('nJpsi_FromB_2','num of J/Psi','@0-@1',RooArgList(nJpsi_FromB,nJpsi_FromB_1))

    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 0.0, -50.0, 50.0) 
    

    alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', 1., 0.0, 10.) 
    alpha_eta_2 = RooRealVar('alpha_eta_2','alpha of CB', 1., 0.0, 10.) 
    n_eta_1 = RooRealVar('n_eta_1','n of CB', 1., 0.0, 100.) 
    n_eta_2 = RooRealVar('n_eta_2','n of CB', 1., 0.0, 100.) 

    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    
    cb_etac_1 = BifurcatedCB("cb_etac_1", "Cystal Ball Function", Jpsi_M_res, mean_Etac, sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1", "Cystal Ball Function", Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    
    
    modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(cb_etac_1), RooArgList(nEtac_FromB))
    modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(cb_Jpsi_1), RooArgList(nJpsi_FromB))
    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac_FromB') 
    sample.defineType('Jpsi_FromB') 
    
    
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import('Etac_FromB',dataEtac_FromB), RooFit.Import('Jpsi_FromB',dataJpsi_FromB)) 
    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac_FromB,'Etac_FromB') 
    simPdf.addPdf(modelJpsi_FromB,'Jpsi_FromB') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)
    


def fitData(iPT=0, resoType='gauss', empty=False):

    gROOT.Reset()
    
    if resoType=='gauss': add = ""
    else: add="_CB"

    w = RooWorkspace('w',True)   
    
    getData_d(w, iPT)
    
    if resoType=='gauss': fillRelWorkspace(w)
    else: fillRelWorkspaceCB(w)
        
    
    Jpsi_M_res = w.var('Jpsi_M_res')
    
    
    modelEtac_FromB = w.pdf('modelEtac_FromB')
    modelJpsi_FromB = w.pdf('modelJpsi_FromB')
    
    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')
    
    
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    r = simPdf.fitTo(combData,RooFit.Save(True)) 
    r = simPdf.fitTo(combData,RooFit.Minos(True),RooFit.Save(True)) 
    
    
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')
    if empty:
        gROOT.ProcessLine('gStyle->SetOptStat(000000000)')

    frame = []
    for i in range(2):
        frame.append(Jpsi_M_res.frame(RooFit.Title('')))

    dataEtac_FromB.plotOn(frame[0])
    dataJpsi_FromB.plotOn(frame[1])
    
    
    modelEtac_FromB.plotOn(frame[0],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac_FromB = frame[0].chiSquare()
    print 'chi2Etac_FromB = ', chi2Etac_FromB
    
    modelJpsi_FromB.plotOn(frame[1],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_FromB = frame[1].chiSquare()
    print 'chi2Jpsi_FromB = ', chi2Jpsi_FromB
    
    c = TCanvas('Masses_Fit','Masses Fit',800,600)
    c.Divide(1,2)
    names = ["#eta_{c} from-b", "J/#psi from-b"]
    texMC = TLatex()
    texMC.SetNDC()

    for iC in range(2):
        pad = c.cd(iC+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl+0.005,yl+0.005,xh-0.005,yh-0.005)
        pad.SetLeftMargin(0.15);  pad.SetBottomMargin(0.15);  frame[iC].GetXaxis().SetTitle('M_{p#bar{p}} - M^{TRUE}_{p#bar{p}} / [MeV/c^2]')
        frame[iC].GetXaxis().SetTitleSize(0.06)
        frame[iC].GetYaxis().SetTitleSize(0.06)
        frame[iC].GetXaxis().SetTitleOffset(0.90)
        frame[iC].GetYaxis().SetTitleOffset(0.90)
        frame[iC].GetXaxis().SetTitleFont(12)
        frame[iC].GetYaxis().SetTitleFont(12)
        frame[iC].GetXaxis().SetLabelSize(0.05)
        frame[iC].GetYaxis().SetLabelSize(0.05)
        frame[iC].GetXaxis().SetLabelFont(62)
        frame[iC].GetYaxis().SetLabelFont(62)
        frame[iC].Draw()
        #frame[iC].SetMaximum(6.e2)
        frame[iC].SetMinimum(0.1)
        texMC.DrawLatex(0.6, 0.80, "LHCb simulation")
        texMC.DrawLatex(0.6, 0.75, "#sqrt{s}=13 TeV")
        texMC.DrawLatex(0.25, 0.75, names[iC])
        #pad.SetLogy()

    
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    


    if(iPT == 0):
        nameTxt  = 'resoSndTopo_log/fit_MassRes_fromB_%s_SndTopoOpt.txt'%(add)
        nameWksp = 'resoSndTopo_log/MC_MassResolution_fromB_%s_SndTopoOpt_wksp.root'%(add)
        nameRoot = 'resoSndTopo_log/MC_MassResolution_fromB_%s_SndTopoOpt_Fit_plot.root'%(add)
        namePic  = 'resoSndTopo_log/MC_MassResolution_fromB_%s_SndTopoOpt_empty.pdf'%(add)    
    else:
        nameTxt  = 'resoSndTopo_log/fit_MassRes_fromB_%s_SndTopoOpt_PT%s.txt'%(add,iPT)
        nameWksp = 'resoSndTopo_log/MC_MassResolution_fromB_%s_SndTopoOpt_wksp_PT%s.root'%(add,iPT)
        nameRoot = 'resoSndTopo_log/MC_MassResolution_fromB_%s_SndTopoOpt_Fit_plot_PT%s.root'%(add,iPT)
        namePic  = 'resoSndTopo_log/MC_MassResolution_fromB_%s_SndTopoOpt_PT%s.pdf'%(add,iPT)



    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    print "chi2 eta_c fromb %6.4f \n"%(chi2Etac_FromB)
    print "chi2 Jpsi fromb %6.4f \n"%(chi2Jpsi_FromB)
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()



    
    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')
    
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    r.correlationMatrix().Print('v')
    r.globalCorr().Print('v')
    



def MC_Resolution_Fit():
    nPTBins = 7 
    for iPT in range(nPTBins+1):
        fitData(iPT, resoType="gauss", empty=False)
        fitData(iPT, resoType="CB", empty=False)  
    
MC_Resolution_Fit()



