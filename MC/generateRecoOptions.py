import os
dir = "./gen4DV"
if not os.path.exists(dir):
    os.makedirs(dir)


def filename(signal,stripSel,loc):
    return signal+"_"+stripSel+"_"+loc+".py"

def generateCode(signal="etacmu", matchDict = {
        "mcMatchPp"   : "mcMatch(\'eta_c(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'eta_c(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => ^eta_c(1S) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => eta_c(1S) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => eta_c(1S) mu+ nu_mu]CC\', 2)"}, stripSel = "oldStr", loc      = "loc", mc_loki=True):

    mcMatchPp   = matchDict["mcMatchPp"]
    mcMatchPm   = matchDict["mcMatchPm"]
    mcMatchJpsi = matchDict["mcMatchJpsi"]
    mcMatchMu   = matchDict["mcMatchMu"]
    mcMatchBc   = matchDict["mcMatchBc"]





    fname = filename(signal,stripSel,loc)
    completeName = os.path.join(dir, fname)

    if stripSel in ['oldStrNoMcorr','newStrNoMcorr']:
        fIn = open("dv_reco_base_noMcorrCut.py","r")
    elif stripSel in ['oldStrNoMcorrNoPROBNN','newStrNoMcorrNoPROBNN']:
        fIn = open("dv_reco_base_noMcorr_noPROBNNCut.py","r")
    elif stripSel in ['NoCut']:
        fIn = open("dv_reco_base_noCut.py","r")
        # stripSel is not used in noCut option
    else:
        fIn = open("dv_reco_base.py","r")



    fOut = open(completeName, "w")
    oldStr = (stripSel=="oldStr") or (stripSel=="oldStrNoMcorr") or (stripSel=="oldStrNoMcorrNoPROBNN")
    local = (loc=="loc")
    fOut.write(
"""
mcMatchPp   = \"{}\"
mcMatchPm   = \"{}\"
mcMatchJpsi = \"{}\"
mcMatchMu   = \"{}\"
mcMatchBc   = \"{}\"
oldStr = {}
local = {}
mc_loki = {}
""".format(mcMatchPp,mcMatchPm,mcMatchJpsi,mcMatchMu,mcMatchBc,oldStr,local,mc_loki))
    for line in fIn.readlines():
        fOut.write(line)


    if loc=="loc":
        fOut.write(
"""
DaVinci().TupleFile      = \"{}.root\"

from Configurables import LHCbApp
LHCbApp().DDDBtag   = \"dddb-20170721-3\"
LHCbApp().CondDBtag = \"sim-20190430-1-vc-md100\"

from GaudiConf import IOHelper
IOHelper().inputFiles([\'testDst/{}.dst\'], clear=True)
""".format(signal,signal))
    else:
        fOut.write(
"""
DaVinci().TupleFile      = \"Tuple.root\"
""")
    fOut.close()
    fIn.close()



from ROOT import TChain
def testLocaly(signal="etacmu", stripSel="oldStr"):

    os.system("rm -rf "+signal+".root")
    fname = filename(signal=signal,stripSel=stripSel,loc="loc")
    os.system('/afs/cern.ch/user/a/ausachov/cmtuser/dv4bc/DaVinciDev_v42r7p3/run gaudirun.py gen4DV/'+fname)

    # raw_input["DaVinci is done, press enter to continue"]

    ch = TChain("Bc2JpsimunuTuple/DecayTree")
    ch.Add(signal+".root")


    nEvts = ch.GetEntries()
    if nEvts==0:
        import sys
        sys.exit(signal+" : no root file created !")
    status = "Failed"
    if nEvts>5:
        status = "OK"

    return status, nEvts




Matches = {
    "etacmu"    : {
        "mcMatchPp"   : "mcMatch(\'eta_c(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'eta_c(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => ^eta_c(1S) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => eta_c(1S) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => eta_c(1S) mu+ nu_mu]CC\', 2)",
        },
    "jpsimu"    : {
        "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => ^J/psi(1S) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => J/psi(1S) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => J/psi(1S) mu+ nu_mu]CC\', 2)",
        },
    "etactau"    : {
        "mcMatchPp"   : "mcMatch(\'eta_c(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'eta_c(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => ^eta_c(1S) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => eta_c(1S) (tau+ => ^mu+ nu_mu nu_tau~) nu_tau]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => eta_c(1S) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC\', 2)",
        },
    "jpsitau"    : {
        "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => ^J/psi(1S) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => J/psi(1S) (tau+ => ^mu+ nu_mu nu_tau~) nu_tau]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => J/psi(1S) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC\', 2)",
        },
    "chic0mu"    : {
        "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => (chi_c0(1P) => ^J/psi(1S) gamma) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => (chi_c0(1P) => J/psi(1S) gamma) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => (chi_c0(1P) => J/psi(1S) gamma) mu+ nu_mu]CC\', 2)",
        },
    "chic1mu"    : {
        "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => (chi_c1(1P) => ^J/psi(1S) gamma) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => (chi_c1(1P) => J/psi(1S) gamma) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => (chi_c1(1P) => J/psi(1S) gamma) mu+ nu_mu]CC\', 2)",
        },
    "chic2mu"    : {
        "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => (chi_c2(1P) => ^J/psi(1S) gamma) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => (chi_c2(1P) => J/psi(1S) gamma) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => (chi_c2(1P) => J/psi(1S) gamma) mu+ nu_mu]CC\', 2)",
        },
    "psi2smu"    : {
        "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => (psi(2S) => ^J/psi(1S) pi+ pi-) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => (psi(2S) => J/psi(1S) pi+ pi-) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => (psi(2S) => J/psi(1S) pi+ pi-) mu+ nu_mu]CC\', 2)",
        },
    "hcmu"    : {
        "mcMatchPp"   : "mcMatch(\'eta_c(1S) => ^p+  p~-\', 2)",
        "mcMatchPm"   : "mcMatch(\'eta_c(1S) => p+  ^p~-\', 2)",
        "mcMatchJpsi" : "mcMatch(\'[B_c+ => (h_c(1P) => ^eta_c(1S) gamma) mu+ nu_mu]CC\', 2)",
        "mcMatchMu"   : "mcMatch(\'[B_c+ => (h_c(1P) => eta_c(1S) gamma) ^mu+ nu_mu]CC\', 2)",
        "mcMatchBc"   : "mcMatch(\'[B_c+ => (h_c(1P) => eta_c(1S) gamma) mu+ nu_mu]CC\', 2)",
        },
    # DON'T run it for bkgs below, they have a bit special options!
    # "b2JpsiX"    : {
    #     "mcMatchPp"   : "mcMatch(\'J/psi(1S) => ^p+  p~-\', 2)",
    #     "mcMatchPm"   : "mcMatch(\'J/psi(1S) => p+  ^p~-\', 2)",
    #     "mcMatchJpsi" : "mcMatch(\'J/psi(1S) => p+  p~-\', 2)",
    #     "mcMatchMu"   : "ALL",
    #     "mcMatchBc"   : "ALL",
    #     },
    # "b2EtacX"    : {
    #     "mcMatchPp"   : "mcMatch(\'eta_c(1S) => ^p+  p~-\', 2)",
    #     "mcMatchPm"   : "mcMatch(\'eta_c(1S) => p+  ^p~-\', 2)",
    #     "mcMatchJpsi" : "mcMatch(\'eta_c(1S) => p+  p~-\', 2)",
    #     "mcMatchMu"   : "ALL",
    #     "mcMatchBc"   : "ALL",
    #     },
    }




# stripSels = ["oldStr",                "newStr",
#              "oldStrNoMcorr",         "newStrNoMcorr",
#              "oldStrNoMcorrNoPROBNN", "newStrNoMcorrNoPROBNN",
#              "NoCut"]
stripSels = ["oldStrNoMcorrNoPROBNN", "newStrNoMcorrNoPROBNN",
             "NoCut"]
keys_to_generate = Matches.keys()
keys_to_generate = ["b2JpsiX", "b2EtacX"]
if __name__ == '__main__':

    test = False

    testMatrix = {}
    print("Now: generating Options at ./gen4DV")

    for signal in keys_to_generate:
        for stripSel in stripSels:

            print(signal+"_" + stripSel + " is running")
            generateCode(signal=signal,matchDict=Matches[signal], stripSel=stripSel,loc="loc",mc_loki=False)
            generateCode(signal=signal,matchDict=Matches[signal], stripSel=stripSel,loc="ganga",mc_loki=False)

    if test:
        for signal in keys_to_generate:
            for stripSel in stripSels:
                print(signal+"_" + stripSel + " is checking")
                status, nEvts =   testLocaly(signal=signal,stripSel=stripSel)
                testMatrix[signal+"_"+stripSel] = {"status":status, "nEvts": nEvts}

        for key in testMatrix.keys():
            status = testMatrix[key]["status"]
            nEvts = testMatrix[key]["nEvts"]
            print(key + ": status = " + status + " ,  nEvts = "+str(nEvts))
        print("\n\n")
        for key in testMatrix.keys():
            if testMatrix[key]["status"]!="OK":
                print(key+" is not OK")
