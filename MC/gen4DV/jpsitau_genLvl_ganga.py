
decay   = "[B_c+ => ^(J/psi(1S) => ^p+ ^p~-) ^(tau+ => ^mu+ ^nu_mu ^nu_tau~) ^nu_tau]CC"
branches   = {'TauP': '[B_c+ => (J/psi(1S) => p+ p~-) ^(tau+ => mu+ nu_mu nu_tau~) nu_tau]CC', 'ProtonM': '[B_c+ => (J/psi(1S) => p+ ^p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC', 'MuP': '[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => ^mu+ nu_mu nu_tau~) nu_tau]CC', 'Jpsi': '[B_c+ => ^(J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC', 'Bc': '[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC', 'antiNu_Tau': '[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu ^nu_tau~) nu_tau]CC', 'Nu_Tau': '[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) ^nu_tau]CC', 'Nu': '[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ ^nu_mu nu_tau~) nu_tau]CC', 'ProtonP': '[B_c+ => (J/psi(1S) => ^p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC'}

# decay = "[B_c+ ->^(J/psi(1S) -> ^p+ ^p~-) ^mu+ ^nu_mu]CC"
# branches = {
#     "Bc"       :  "[B_c+ -> (J/psi(1S) ->  p+  p~-)  mu+ nu_mu]CC"
#    ,"ProtonP"  :  "[B_c+ -> (J/psi(1S) -> ^p+  p~-)  mu+ nu_mu]CC"
#    ,"ProtonM"  :  "[B_c+ -> (J/psi(1S) ->  p+ ^p~-)  mu+ nu_mu]CC"
#    ,"Jpsi"     :  "[B_c+ ->^(J/psi(1S) -> ^p+ ^p~-)  mu+ nu_mu]CC"
#    ,"MuP"      :  "[B_c+ -> (J/psi(1S) ->  p+  p~-) ^mu+ nu_mu]CC"
#    ,"Nu"       :  "[B_c+ -> (J/psi(1S) ->  p+  p~-)  mu+ ^nu_mu]CC"
# }


from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, MCDecayTreeTuple, DecayTreeTuple

from Configurables import GaudiSequencer, DataOnDemandSvc,CondDB
from Configurables import DstConf, CaloDstUnPackConf, LoKi__Hybrid__TupleTool
from Configurables import EventNodeKiller, ProcStatusCheck
from Configurables import StrippingReport, TimingAuditor, SequencerTimerTool

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


def addNtupleToDaVinci():

    MCTuple = MCDecayTreeTuple("MCDecayTreeTuple")

    MCTuple.Decay    = decay
    MCTuple.Branches = branches

    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth, TupleToolGeneration

    MCTruth = TupleToolMCTruth("MCTruth")
    MCTuple.addTool(MCTruth)
    MCTruth.ToolList =  ["MCTupleToolAngles"
                         , "MCTupleToolHierarchy"
                         , "MCTupleToolKinematic"
                         , "MCTupleToolReconstructed"
                         , "MCTupleToolPID"
                         , "MCTupleToolDecayType"
                         , "MCTupleToolPrompt"
                         ]


    from LoKiMC.functions import MCMOTHER, MCVFASPF, MCPRIMARY

    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTuple.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTuple.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTuple.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTuple.MCLoKiHybrid.Variables = {
        "TRUEM"     : "MCM",
        "MCVZ"      : "MCVFASPF(MCVZ)",
        "TRUEETA"   : "MCETA",
        "TRUEPHI"   : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "TRUE_Tz"   : "(3.3)*(MCMOTHER(MCVFASPF(MCVZ), 0)-MCVFASPF(MCVZ))*MCM/MCPZ",
        "MC_ISPRIMARY"      : "MCVFASPF(switch(MCPRIMARY,1,0))"
        }
    ######################################################################
    from Configurables import LoKi__Hybrid__EvtTupleTool
    MCTuple.addTool( LoKi__Hybrid__EvtTupleTool, name = "ELoKiHybrid" )
    MCTuple.ToolList += [ "LoKi::Hybrid::EvtTupleTool/ELoKiHybrid" ]
    MCTuple.ELoKiHybrid.Preambulo = [ "from LoKiCore.basic import LHCb" ]
    MCTuple.ELoKiHybrid.VOID_Variables = {
        "nSPDHits" : "RECSUMMARY( LHCb.RecSummary.nSPDhits, -1, '/Event/Rec/Summary', False )"
    }

    return MCTuple

# def getDBInfo(sim, magnet, lyr):
#   if sim:
#     if lyr == "2011" or lyr == "2012":
#       TagDDDB = "dddb-20150928"
#       TagCondDB = "sim-20160321"#-2-vc-md100
#     if lyr == "2011":
#       TagDDDB   += "-1"
#       TagCondDB += "-1"
#       TagCondDB += "-vc"
#     if lyr == "2012":
#       TagCondDB += "-2"
#       TagCondDB += "-vc"
#     if lyr == "2015":
#       TagDDDB   = "dddb-20150724"
#       TagCondDB = "sim-20161124-vc"
#     if lyr == "2016":
#       TagDDDB   = "dddb-20150724"
#       TagCondDB = "sim-20161124-2-vc"
#     if lyr == "2017" :
#        TagDDDB  = "dddb-20170721-3"
#        TagCondDB= "sim-20190430-1-vc"
#
#     if magnet == "MD":
#       TagCondDB += "-md100"
#     else:
#       TagCondDB += "-mu100"
#   return TagDDDB, TagCondDB


Year = "2017"
DaVinci().InputType = "DST"
if Year == "2011" or Year == "2012":
    importOptions("$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py")

DaVinci().Simulation     = True
DaVinci().Lumi = False

mcTuple= addNtupleToDaVinci()
DaVinci().UserAlgorithms += [mcTuple]
DaVinci().DataType = Year
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
# DaVinci().TupleFile = "Tuple.root"
from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]
from Configurables import CondDB

#
# from GaudiConf import IOHelper
# # Use the local input data
# IOHelper().inputFiles([
#                       '00098995_00000042_7.AllStreams.dst'
#                       ], clear=True)

DaVinci().TupleFile      = "Tuple.root"
