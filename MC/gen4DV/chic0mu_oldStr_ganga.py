
mcMatchPp   = "mcMatch('J/psi(1S) => ^p+  p~-', 2)"
mcMatchPm   = "mcMatch('J/psi(1S) => p+  ^p~-', 2)"
mcMatchJpsi = "mcMatch('[B_c+ => (chi_c0(1P) => ^J/psi(1S) gamma) mu+ nu_mu]CC', 2)"
mcMatchMu   = "mcMatch('[B_c+ => (chi_c0(1P) => J/psi(1S) gamma) ^mu+ nu_mu]CC', 2)"
mcMatchBc   = "mcMatch('[B_c+ => (chi_c0(1P) => J/psi(1S) gamma) mu+ nu_mu]CC', 2)"
oldStr = True
local = False

# mcMatchPp   = "mcMatch('eta_c(1S) => ^p+  p~-', 2)"
# mcMatchPm   = "mcMatch('eta_c(1S) => p+  ^p~-', 2)"
# mcMatchJpsi = "mcMatch('[B_c+ => ^eta_c(1S) mu+ nu_mu]CC', 2)"
# mcMatchMu   = "mcMatch('[B_c+ => eta_c(1S) ^mu+ nu_mu]CC', 2)"
# mcMatchBc   = "mcMatch('[B_c+ => eta_c(1S) mu+ nu_mu]CC', 2)"
# oldStr = True
# local = True

from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles, FilterInTrees
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)

def fillTuple( tuple, myTriggerList ):

    from Configurables import TupleToolVtxIsoln, TupleToolIsolationTwoBody, TupleToolDira, TupleToolDocas, TupleToolTauMuDiscrVars, TupleToolSLTools

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                      "TupleToolVtxIsoln",
                      "TupleToolIsolationTwoBody",
                      "TupleToolDira"
                     ]



    from Configurables import TupleToolNeutrinoReco
    tuple.addTool( TupleToolNeutrinoReco,name = "TupleToolNeutrinoReco" )
    tuple.TupleToolNeutrinoReco.Verbose = True
    tuple.TupleToolNeutrinoReco.MotherMass=6274.9 #MeV
    tuple.TupleToolNeutrinoReco.ExtraName='mBc'

    from Configurables import TupleToolTISTOS, TupleToolDecay

    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTupleTool(TupleToolDecay, name = 'Bc')
    tuple.Bc.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForBc" ]
    tuple.Bc.addTool(TupleToolTISTOS, name="TupleToolTISTOSForBc" )
    tuple.Bc.TupleToolTISTOSForBc.Verbose=True
    tuple.Bc.TupleToolTISTOSForBc.TriggerList = myTriggerList


    tuple.Bc.addTupleTool("TupleToolSLTools")
    tuple.Bc.addTupleTool("TupleToolSLTruth")
    tuple.Bc.addTupleTool("TupleToolTauMuDiscrVars")


    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    LoKi_Bc.Preambulo = [
      "MASSES = LoKi.AuxDTFBase.MASSES",
      "massEtac = MASSES() ",
      "massEtac['J/psi(1S)'] = 2.9836 * GeV"
      ]

    varsB = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        # "m_scaled"             : "DTF_FUN ( M , False )",
        "DOCA"                 : "DOCA(1,2)",
        "m_pv"     : "DTF_FUN ( M , True )",
        "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"  : "DTF_CHI2NDOF( True  )",
        "DTFEtac_M": 'DTF_FUN(M , False, strings( [ "J/psi(1S)"] ) , massEtac)'
      }

    LoKi_Bc.Variables = varsB
    tuple.Bc.addTupleTool(LoKi_Bc)

    from Configurables import TupleToolDecayTreeFitter
    tuple.Bc.ToolList += [ "TupleToolDecayTreeFitter/DTFJpsi"]
    tuple.Bc.addTool(TupleToolDecayTreeFitter("DTFJpsi"))
    tuple.Bc.DTFJpsi.Verbose=True
    tuple.Bc.DTFJpsi.daughtersToConstrain=["J/psi(1S)"]
    tuple.Bc.DTFJpsi.constrainToOriginVertex=False


    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth, TupleToolSLTruth
    tuple.ToolList += ["TupleToolMCBackgroundInfo"]

    MCTruth = TupleToolMCTruth("MCTruth")
    tuple.addTool(MCTruth)
    MCTruth.ToolList =  ["MCTupleToolAngles"
                         , "MCTupleToolHierarchy"
                         , "MCTupleToolKinematic"
                         , "MCTupleToolReconstructed"
                         , "MCTupleToolPID"
                         , "MCTupleToolDecayType"
                         , "MCTupleToolPrompt"
                         ]




    from LoKiMC.functions import MCMOTHER, MCVFASPF, MCPRIMARY

    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTruth.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTruth.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTruth.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTruth.MCLoKiHybrid.Variables = {
        "TRUEM"     : "MCM",
        "MCVZ"      : "VFASPF(MCVZ)",
        "TRUEETA"   : "MCETA",
        "TRUEPHI"   : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "TRUE_Tz"   : "(3.3)*(MCMOTHER(MCVFASPF(MCVZ), 0)-MCVFASPF(MCVZ))*MCM/MCPZ",
        "MC_ISPRIMARY"      : "MCVFASPF(switch(MCPRIMARY,1,0))"
        }



    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)

    tuple.Bc.addTupleTool( 'TupleToolSubMass' )
    tuple.Bc.TupleToolSubMass.Substitution += ["p+ => pi+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["p+ => K+"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => pi+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => e+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => K+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => p+"]


myTriggerList = [
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",

                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackMuonMVADecision",
                 "Hlt1SingleMuonNoIPDecision",
                 "Hlt1DiMuonNoIPDecision"

                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TrackAllL0Decision",

                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",

                 "Hlt2PhiIncPhiDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision",

                 "Hlt2Bc2JpsiXTFBc2JpsiMuXDecision",
                 "Hlt2Bc2JpsiXBc2JpsiHDecision",

                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",

                 "Hlt2SingleMuonDecision",
                 "Hlt2SingleMuonRareDecision",
                 "Hlt2SingleMuonVHighPTDecision"
                 ]


MyPreambulo = [
    'from LoKiPhysMC.decorators import *',
    'from LoKiPhysMC.functions import mcMatch' ,
    'from LoKiCore.functions import monitor'
]

from StandardParticles import StdLooseProtons as Protons
from StandardParticles import StdLooseMuons as Muons


import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection

jpsi_daughters = {
  'p+'  : mcMatchPp+" & \
          (PROBNNp > 0.1)  & (PT > 400*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
  'p~-' : mcMatchPm+" & \
          (PROBNNp > 0.1)  & (PT > 400*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)"
}
jpsi_comb = "in_range(2.7*GeV, AM, 3.3*GeV)"
jpsi_mom = "in_range(2.7*GeV, MM, 3.3*GeV) & (VFASPF(VCHI2/VDOF) < 9.)"
jpsi_sel = SimpleSelection(
    'Sel_jpsi',
    ConfigurableGenerators.CombineParticles,
    [Protons],
    DecayDescriptor='J/psi(1S) -> p+ p~-',
    Preambulo       = MyPreambulo,
    DaughtersCuts=jpsi_daughters,
    CombinationCut=jpsi_comb,
    MotherCut=jpsi_mom
)

bc_comb = "in_range(3.2*GeV, AM, 6.8*GeV)"
# bc_mom  = mcMatchBc+" & \
#            in_range(3.2*GeV, MM, 6.8*GeV) & in_range(3.2*GeV, BPVCORRM, 6.8*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)"
bc_mom  = mcMatchBc+" & \
           in_range(3.2*GeV, MM, 6.8*GeV) & in_range(3.2*GeV, BPVCORRM, 6.8*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)"
bc_daughters = {
 "mu+"       : mcMatchMu+" & \
               (PROBNNmu > 0.1) & (PT > 500*MeV) & (TRGHOSTPROB<0.4) & (TRCHI2DOF < 5)",
 "J/psi(1S)" : mcMatchJpsi
}

bc_sel = SimpleSelection(
    'Sel_Bc',
    ConfigurableGenerators.CombineParticles,
    [jpsi_sel, Muons],
    DecayDescriptor="[B_c+ -> J/psi(1S) mu+]cc",
    Preambulo       = MyPreambulo,
    DaughtersCuts= bc_daughters,
    CombinationCut=bc_comb,
    MotherCut=bc_mom
)
bc_seq = SelectionSequence('bc_seq', TopSelection=bc_sel)


Bc2JpsimunuDecay = "[B_c+ ->^(J/psi(1S) -> ^p+ ^p~-) ^mu+]CC"
Bc2JpsimunuBranches = {
     "Bc"       :  "[B_c+ -> (J/psi(1S) -> p+  p~-)  mu+]CC",
     "Jpsi"     :  "[B_c+ ->^(J/psi(1S) -> p+  p~-)  mu+]CC",
     "ProtonP"  :  "[B_c+ -> (J/psi(1S) ->^p+  p~-)  mu+]CC",
     "ProtonM"  :  "[B_c+ -> (J/psi(1S) -> p+ ^p~-)  mu+]CC",
     "Mu"       :  "[B_c+ -> (J/psi(1S) -> p+  p~-) ^mu+]CC"
}

Bc2JpsimunuTuple = DecayTreeTuple('Bc2JpsimunuTuple')
Bc2JpsimunuTuple.Inputs   = [bc_sel.outputLocation()]
Bc2JpsimunuTuple.Decay    =  Bc2JpsimunuDecay
Bc2JpsimunuTuple.Branches =  Bc2JpsimunuBranches
fillTuple( Bc2JpsimunuTuple, myTriggerList)


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Bc2JpsimunuFilters = LoKi_Filters (
     STRIP_Code = """
                  HLT_PASS('StrippingB2PPbarMuForTauMuLineDecision')
                  """
    )

year = "2017"
from Configurables import CondDB
if oldStr:
    DaVinci().EventPreFilters = Bc2JpsimunuFilters.filters('Bc2JpsimunuFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = True
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
# DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [bc_seq.sequence(), Bc2JpsimunuTuple]       # The algorithms

DaVinci().InputType = "DST"

DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


# if local:
#     from Configurables import LHCbApp
#     LHCbApp().DDDBtag   = "dddb-20170721-3"
#     LHCbApp().CondDBtag = "sim-20190430-1-vc-mu100"
#
#     from GaudiConf import IOHelper
#     IOHelper().inputFiles(['../{}.dst'.format(signal)], clear=True)

DaVinci().TupleFile      = "Tuple.root"
