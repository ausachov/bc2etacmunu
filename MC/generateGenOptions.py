import os
dir = "./gen4DV"
if not os.path.exists(dir):
    os.makedirs(dir)


def filename(signal,loc):
    return signal+"_genLvl_"+loc+".py"

def generateCode(signal="etacmu",
                 decay = "[B_c+ =>^(J/psi(1S) => ^p+ ^p~-) ^mu+ ^nu_mu]CC",
                 branches = {
    "Bc"       :  "[B_c+ => (J/psi(1S) =>  p+  p~-)  mu+ nu_mu]CC"
   ,"ProtonP"  :  "[B_c+ => (J/psi(1S) => ^p+  p~-)  mu+ nu_mu]CC"
   ,"ProtonM"  :  "[B_c+ => (J/psi(1S) =>  p+ ^p~-)  mu+ nu_mu]CC"
   ,"Jpsi"     :  "[B_c+ =>^(J/psi(1S) => ^p+ ^p~-)  mu+ nu_mu]CC"
   ,"MuP"      :  "[B_c+ => (J/psi(1S) =>  p+  p~-) ^mu+ nu_mu]CC"
   ,"Nu"       :  "[B_c+ => (J/psi(1S) =>  p+  p~-)  mu+ ^nu_mu]CC"
},
                loc      = "loc"):




    fname = filename(signal,loc)
    completeName = os.path.join(dir, fname)

    fIn = open("dv_genLvl_base.py","r")

    fOut = open(completeName, "w")
    local = (loc=="loc")
    fOut.write(
"""
decay   = \"{}\"
branches   = {}
""".format(decay,branches,local))
    for line in fIn.readlines():
        fOut.write(line)


    if loc=="loc":
        fOut.write(
"""
DaVinci().TupleFile      = \"{}_genLvl.root\"

from Configurables import LHCbApp
LHCbApp().DDDBtag   = \"dddb-20170721-3\"
LHCbApp().CondDBtag = \"sim-20190430-1-vc-mu100\"

from GaudiConf import IOHelper
IOHelper().inputFiles([\'testDst/{}.dst\'], clear=True)
""".format(signal,signal))
    else:
        fOut.write(
"""
DaVinci().TupleFile      = \"Tuple.root\"
""")
    fOut.close()
    fIn.close()



from ROOT import *
def testLocaly(signal="etacmu"):

    fname = filename(signal=signal,loc="loc")
    os.system("rm -rf "+signal+"_genLvl.root")
    os.system('/afs/cern.ch/user/a/ausachov/cmtuser/DaVinciDev_v42r7p3/run gaudirun.py gen4DV/'+fname)

    ch = TChain("MCDecayTreeTuple/MCDecayTree")
    ch.Add(signal+"_genLvl.root")

    nEvts = ch.GetEntries()
    if nEvts==0:
        import sys
        sys.exit(signal+" : no root file created !")
    status = "Failed"
    if nEvts>5:
        status = "OK"

    return status, nEvts




decayDict = {
    "etacmu"    : {
        "decay"      : "[B_c+ =>^(eta_c(1S) => ^p+ ^p~-) ^mu+ ^nu_mu]CC",
        "branches"   : {
            "Bc"       :  "[B_c+ => (eta_c(1S) =>  p+  p~-)  mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (eta_c(1S) => ^p+  p~-)  mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (eta_c(1S) =>  p+ ^p~-)  mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ =>^(eta_c(1S) => ^p+ ^p~-)  mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (eta_c(1S) =>  p+  p~-) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (eta_c(1S) =>  p+  p~-)  mu+ ^nu_mu]CC"
        }},
    "jpsimu"    : {
        "decay"      : "[B_c+ =>^(J/psi(1S) => ^p+ ^p~-) ^mu+ ^nu_mu]CC",
        "branches"   : {
            "Bc"       :  "[B_c+ => (J/psi(1S) =>  p+  p~-)  mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (J/psi(1S) => ^p+  p~-)  mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (J/psi(1S) =>  p+ ^p~-)  mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ =>^(J/psi(1S) => ^p+ ^p~-)  mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (J/psi(1S) =>  p+  p~-) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (J/psi(1S) =>  p+  p~-)  mu+ ^nu_mu]CC"
        }},
    "etactau"    : {
        "decay"     : "[B_c+ => ^(eta_c(1S) => ^p+ ^p~-) ^(tau+ => ^mu+ ^nu_mu ^nu_tau~) ^nu_tau]CC",
        "branches"  : {
            "Bc"        :  "[B_c+ => (eta_c(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"ProtonP"   :  "[B_c+ => (eta_c(1S) => ^p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"ProtonM"   :  "[B_c+ => (eta_c(1S) => p+ ^p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"Jpsi"      :  "[B_c+ => ^(eta_c(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"MuP"       :  "[B_c+ => (eta_c(1S) => p+ p~-) (tau+ => ^mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"Nu"        :  "[B_c+ => (eta_c(1S) => p+ p~-) (tau+ => mu+ ^nu_mu nu_tau~) nu_tau]CC"
           ,"TauP"      :  "[B_c+ => (eta_c(1S) => p+ p~-) ^(tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"antiNu_Tau":  "[B_c+ => (eta_c(1S) => p+ p~-) (tau+ => mu+ nu_mu ^nu_tau~) nu_tau]CC"
           ,"Nu_Tau"    :  "[B_c+ => (eta_c(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) ^nu_tau]CC"
        }},
    "jpsitau"    : {
        "decay"     : "[B_c+ => ^(J/psi(1S) => ^p+ ^p~-) ^(tau+ => ^mu+ ^nu_mu ^nu_tau~) ^nu_tau]CC",
        "branches"  : {
            "Bc"       :  "[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"ProtonP"  :  "[B_c+ => (J/psi(1S) => ^p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"ProtonM"  :  "[B_c+ => (J/psi(1S) => p+ ^p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"Jpsi"     :  "[B_c+ => ^(J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"MuP"      :  "[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => ^mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"Nu"       :  "[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ ^nu_mu nu_tau~) nu_tau]CC"
           ,"TauP"      :  "[B_c+ => (J/psi(1S) => p+ p~-) ^(tau+ => mu+ nu_mu nu_tau~) nu_tau]CC"
           ,"antiNu_Tau":  "[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu ^nu_tau~) nu_tau]CC"
           ,"Nu_Tau"    :  "[B_c+ => (J/psi(1S) => p+ p~-) (tau+ => mu+ nu_mu nu_tau~) ^nu_tau]CC"
        }},
    "chic0mu"    : {
        "decay"     : "[B_c+ => ^(chi_c0(1P) => ^(J/psi(1S) => ^p+ ^p~-) ^gamma) ^mu+ ^nu_mu]CC",
        "branches"  : {
            "Bc"       :  "[B_c+ => (chi_c0(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (chi_c0(1P) => (J/psi(1S) => ^p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (chi_c0(1P) => (J/psi(1S) => p+ ^p~-) gamma) mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ => (chi_c0(1P) => ^(J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (chi_c0(1P) => (J/psi(1S) => p+ p~-) gamma) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (chi_c0(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ ^nu_mu]CC"
           ,"Chic0"    :  "[B_c+ => ^(chi_c0(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"Gamma"    :  "[B_c+ => (chi_c0(1P) => (J/psi(1S) => p+ p~-) ^gamma) mu+ nu_mu]CC"
        }},
    "chic1mu"    : {
        "decay"     : "[B_c+ => ^(chi_c1(1P) => ^(J/psi(1S) => ^p+ ^p~-) ^gamma) ^mu+ ^nu_mu]CC",
        "branches"  : {
            "Bc"       :  "[B_c+ => (chi_c1(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (chi_c1(1P) => (J/psi(1S) => ^p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (chi_c1(1P) => (J/psi(1S) => p+ ^p~-) gamma) mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ => (chi_c1(1P) => ^(J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (chi_c1(1P) => (J/psi(1S) => p+ p~-) gamma) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (chi_c1(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ ^nu_mu]CC"
           ,"Chic1"    :  "[B_c+ => ^(chi_c1(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"Gamma"    :  "[B_c+ => (chi_c1(1P) => (J/psi(1S) => p+ p~-) ^gamma) mu+ nu_mu]CC"
        }},
    "chic2mu"    : {
        "decay"     : "[B_c+ => ^(chi_c2(1P) => ^(J/psi(1S) => ^p+ ^p~-) ^gamma) ^mu+ ^nu_mu]CC",
        "branches"  : {
            "Bc"       :  "[B_c+ => (chi_c2(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (chi_c2(1P) => (J/psi(1S) => ^p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (chi_c2(1P) => (J/psi(1S) => p+ ^p~-) gamma) mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ => (chi_c2(1P) => ^(J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (chi_c2(1P) => (J/psi(1S) => p+ p~-) gamma) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (chi_c2(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ ^nu_mu]CC"
           ,"Chic2"    :  "[B_c+ => ^(chi_c2(1P) => (J/psi(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"Gamma"    :  "[B_c+ => (chi_c2(1P) => (J/psi(1S) => p+ p~-) ^gamma) mu+ nu_mu]CC"
        }},
    "psi2smu"    : {
        "decay"     : "[B_c+ => ^(psi(2S) => ^(J/psi(1S) => ^p+ ^p~-) ^pi+ ^pi-) ^mu+ ^nu_mu]CC",
        "branches"  : {
            "Bc"       :  "[B_c+ => (psi(2S) => (J/psi(1S) => p+ p~-) pi+ pi-) mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (psi(2S) => (J/psi(1S) => ^p+ p~-) pi+ pi-) mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (psi(2S) => (J/psi(1S) => p+ ^p~-) pi+ pi-) mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ => (psi(2S) => ^(J/psi(1S) => p+ p~-) pi+ pi-) mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (psi(2S) => (J/psi(1S) => p+ p~-) pi+ pi-) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (psi(2S) => (J/psi(1S) => p+ p~-) pi+ pi-) mu+ ^nu_mu]CC"
           ,"Psi2s"    :  "[B_c+ => ^(psi(2S) => (J/psi(1S) => p+ p~-) pi+ pi-) mu+ nu_mu]CC"
           ,"PiP"      :  "[B_c+ => (psi(2S) => (J/psi(1S) => p+ p~-) ^pi+ pi-) mu+ nu_mu]CC"
           ,"PiM"      :  "[B_c+ => (psi(2S) => (J/psi(1S) => p+ p~-) ^pi+ ^pi-) mu+ nu_mu]CC"
        }},
    "hcmu"    : {
        "decay"     : "[B_c+ => ^(h_c(1P) => ^(eta_c(1S) => ^p+ ^p~-) ^gamma) ^mu+ ^nu_mu]CC",
        "branches"  : {
            "Bc"       :  "[B_c+ => (h_c(1P) => (eta_c(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonP"  :  "[B_c+ => (h_c(1P) => (eta_c(1S) => ^p+ p~-) gamma) mu+ nu_mu]CC"
           ,"ProtonM"  :  "[B_c+ => (h_c(1P) => (eta_c(1S) => p+ ^p~-) gamma) mu+ nu_mu]CC"
           ,"Jpsi"     :  "[B_c+ => (h_c(1P) => ^(eta_c(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"MuP"      :  "[B_c+ => (h_c(1P) => (eta_c(1S) => p+ p~-) gamma) ^mu+ nu_mu]CC"
           ,"Nu"       :  "[B_c+ => (h_c(1P) => (eta_c(1S) => p+ p~-) gamma) mu+ ^nu_mu]CC"
           ,"Hc"       :  "[B_c+ => ^(h_c(1P) => (eta_c(1S) => p+ p~-) gamma) mu+ nu_mu]CC"
           ,"Gamma"    :  "[B_c+ => (h_c(1P) => (eta_c(1S) => p+ p~-) ^gamma) mu+ nu_mu]CC"
        }},
    # "b2JpsiX"    : {
    #     "decay"      : "[B_c+ =>^(J/psi(1S) => ^p+ ^p~-) ^mu+ ^nu_mu]CC",
    #     "branches"   : {
    #         "Bc"       :  "[B_c+ => (J/psi(1S) =>  p+  p~-)  mu+ nu_mu]CC"
    #        ,"ProtonP"  :  "[B_c+ => (J/psi(1S) => ^p+  p~-)  mu+ nu_mu]CC"
    #        ,"ProtonM"  :  "[B_c+ => (J/psi(1S) =>  p+ ^p~-)  mu+ nu_mu]CC"
    #        ,"Jpsi"     :  "[B_c+ =>^(J/psi(1S) => ^p+ ^p~-)  mu+ nu_mu]CC"
    #        ,"MuP"      :  "[B_c+ => (J/psi(1S) =>  p+  p~-) ^mu+ nu_mu]CC"
    #        ,"Nu"       :  "[B_c+ => (J/psi(1S) =>  p+  p~-)  mu+ ^nu_mu]CC"
    #     }},
    }





if __name__ == '__main__':

    test = True

    testMatrix = {}
    print("Now: generating Gen Options at ./gen4DV")

    for signal in decayDict.keys():
        print(signal+"__genLvl is running")
        decay    = decayDict[signal]["decay"]
        branches = decayDict[signal]["branches"]
        generateCode(signal=signal,decay=decay,branches=branches,loc="loc")
        generateCode(signal=signal,decay=decay,branches=branches,loc="ganga")
        if test:
            print(signal+"__genLvl is checking")
            status, nEvts =   testLocaly(signal=signal)
            testMatrix[signal+"_genLvl"] = {"status":status, "nEvts": nEvts}


    if test:
        for key in testMatrix.keys():
            status = testMatrix[key]["status"]
            nEvts = testMatrix[key]["nEvts"]
            print(key + ": status = " + status + " ,  nEvts = "+str(nEvts))
        print("\n\n")
        for key in testMatrix.keys():
            if testMatrix[key]["status"]!="OK":
                print(key+" is not OK")
