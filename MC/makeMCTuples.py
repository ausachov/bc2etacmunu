import sys
import argparse

EvtTypes = {
            #  "etacmu"    : "14543021"
            # ,"jpsimu"    : "14543022"
            # ,"etactau"   : "14543031"
            # ,"jpsitau"   : "14543032"
            # ,"chic0mu"   : "14543211"
            # ,"chic1mu"   : "14543212"
            # ,"chic2mu"   : "14543213"
            # ,"hcmu"      : "14543214"
            # ,"psi2smu"   : "14545021"
            # ,"Bd2JpsiX"  : "11442001" #deprecated
            # ,"Bu2JpsiX"  : "12442001" #deprecated
            "b2JpsiX"   : "10030000"
            ,"b2EtacX"   : "10030060"
            }

def filename(signal="etacmu",stripSel="newStr",loc="ganga"):
    return signal+"_"+stripSel+"_"+loc+".py"

def makeSingleTuple(signal="etacmu", year="2017", polarity="Up", stripSel="newStr"):

    global EvtTypes, filename
    myJobName = 'MC_Bc2'+signal+"_"+stripSel+'_Mag'+polarity+'_'+year


    if year=='2017':
        year_short = '17'
        reco       = '17'
        strip      = 'Stripping29r2'
        davinciVer = 'v42r7p3'
        platform = "x86_64-slc6-gcc62-opt"
    elif year=='2016':
        year_short = '16'
        reco       = '16'
        strip      = 'Stripping28r2'
        davinciVer = 'v42r7p3'
        platform = "x86_64-slc6-gcc62-opt"
    elif year=='2018':
        year_short = '18'
        reco       = '18'
        strip      = 'Stripping34'
        davinciVer = 'v44r7'
        platform   = 'x86_64+avx2+fma-centos7-gcc62-opt'

    evtType = EvtTypes[signal]

    davinciDir = '/afs/cern.ch/user/a/ausachov/cmtuser/dv4bc/DaVinciDev_'+davinciVer
    import os
    if os.path.exists(davinciDir):
        myApplication = GaudiExec()
        myApplication.directory = davinciDir
    else:
        myApplication = prepareGaudiExec('DaVinci',davinciVer, myPath='$HOME/cmtuser/')

    myApplication.platform = platform

    fname = filename(signal=signal,stripSel=stripSel,loc='ganga')
    if signal=="Bd2JpsiX":
        fname = filename(signal="Bu2JpsiX",stripSel=stripSel,loc='ganga')

    if stripSel == "NoCut_allHLT":
        fname = "./HltSummary/"+fname
    else:
        fname = "./gen4DV/"+fname

    if polarity=='Up':
        myApplication.options = [fname, 'mu_{}_tag.py'.format(year)]
    if polarity=='Down':
        myApplication.options = [fname, 'md_{}_tag.py'.format(year)]

    path, generator, sim_v = "","",""
    if signal=="b2JpsiX" or signal=="b2EtacX":
        if year=="2017":
            path = '/MC/{0}/Beam6500GeV-{1}-Mag{2}-Nu1.6-25ns-Pythia8/Sim09j/Trig0x62661709/Reco{3}/Turbo04a-WithTurcal/{4}NoPrescalingFlagged/{5}/ALLSTREAMS.DST'.format(year, year, polarity, year_short, strip, evtType)
        if year=="2016":
            path = '/MC/{0}/Beam6500GeV-{1}-Mag{2}-Nu1.6-25ns-Pythia8/Sim09j/Trig0x6139160F/Reco{3}/Turbo03a/{4}NoPrescalingFlagged/{5}/ALLSTREAMS.DST'.format(year, year, polarity, year_short, strip, evtType)
        if year=="2018":
            path = '/MC/{0}/Beam6500GeV-{1}-Mag{2}-Nu1.6-25ns-Pythia8/Sim09j/Trig0x617d18a4/Reco{3}/Turbo05-WithTurcal/{4}NoPrescalingFlagged/{5}/ALLSTREAMS.DST'.format(year, year, polarity, year_short, strip, evtType)
    else:
        if year=="2017":
            path = '/MC/{0}/Beam6500GeV-{1}-Mag{2}-Nu1.6-25ns-BcVegPyPythia8/Sim09h/Trig0x62661709/Reco{3}/Turbo04a-WithTurcal/{4}NoPrescalingFlagged/{5}/ALLSTREAMS.DST'.format(year, year, polarity, year_short, strip, evtType)
        if year=="2016":
            path = "MC/{0}/Beam6500GeV-{1}-Mag{2}-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco{3}/Turbo03/{4}NoPrescalingFlagged/{5}/ALLSTREAMS.DST".format(year, year, polarity, year_short, strip, evtType)


    data  = BKQuery(path).getDataset()
    validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().items() if len(rep)])
    mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = False, bulksubmit = False )

    myBackend = Dirac()
    j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [DiracFile('Tuple.root')],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
    # j.backend.settings['BannedSites'] = ['LCG.NCBJ.pl','LCG.Beijing.cn']
    queues.add(j.submit)
    print('\n\n\n')



# years      = ['2016','2017','2018']
years      = ['2017']
# polarities = ['Down','Up']
polarities = ['Up']

# stripSels  = ['oldStrNoMcorrNoPROBNN','newStrNoMcorrNoPROBNN','NoCut']
# for signal in EvtTypes.keys():
# 	for year in years:
# 		for stripSel in stripSels:
# 			for polarity in polarities:
# 				print('MC Bc2', signal, 'nu', year, polarity, stripSel,' submitting below! \n')
# 				makeSingleTuple(signal=signal,year=year,polarity=polarity,stripSel=stripSel)

#2016 and 2018 available only for b2Etac and b2JpsiX
# signals = ["b2JpsiX","b2EtacX"]
signals = ["b2JpsiX"]
stripSel = "NoCut"
for signal in signals:
    for year in years:
        for polarity in polarities:
            print('MC Bc2', signal, 'nu', year, polarity, stripSel,' submitting below! \n')
            makeSingleTuple(signal=signal,year=year,polarity=polarity,stripSel=stripSel)
