import sys
import argparse

EvtTypes = {
            # "etacmu"     : "14543021"
            # ,"jpsimu"    : "14543022"
            # ,"etactau"   : "14543031"
            # ,"jpsitau"   : "14543032"
            # ,"chic0mu"   : "14543211"
            # ,"chic1mu"   : "14543212"
            # ,"chic2mu"   : "14543213"
            # ,"hcmu"      : "14543214"
            # ,"psi2smu"   : "14545021"
            # ,
            # "Bd2JpsiX"   : "11442001"
            # ,"Bu2JpsiX"   : "12442001"
            "b2JpsiX"   : "10030000"
            ,"b2EtacX"   : "10030060"
            }
# stripSels     = ['oldStrNoMcorrNoPROBNN','newStrNoMcorrNoPROBNN','NoCut','NoCut_allHLT']
stripSels     = ['NoCut']

import os
dir = "/eos/user/a/ausachov/MC_Bc/"
if not os.path.exists(dir):
    os.makedirs(dir)

for stripSel in stripSels:
    stripDir = dir + stripSel
    if not os.path.exists(stripDir):
        os.makedirs(stripDir)
    for signal in EvtTypes.keys():
        sigDir = stripDir+'/'+signal
        if not os.path.exists(sigDir):
            os.makedirs(sigDir)
        for year in ["2016","2017","2018"]:
            yearDir = sigDir+"/"+year
            if not os.path.exists(yearDir):
                os.makedirs(yearDir)
            if not os.path.exists(yearDir+"/MagUp"):
                os.makedirs(yearDir+"/MagUp")
            if not os.path.exists(yearDir+"/MagDown"):
                os.makedirs(yearDir+"/MagDown")



istart = 2060
iend = 2072
for ii in range(istart, iend+1):
    try:
        ijob = jobs(ii)
        if ijob.status=='completed' or ijob.status!='completed':
            name = ijob.name
            print("Job {} : downloading tuples".format(ii))
            print(name)
            strs = name.split("_")

            signal = strs[1][3:]
            if len(strs)==5 :
                stripSel = strs[2]
                polarity = strs[3]
                year     = strs[4]
            elif len(strs)==6 :
                stripSel = strs[2]+"_"+strs[3]
                polarity = strs[4]
                year     = strs[5]

            polDir = dir+stripSel+"/"+signal+"/"+year+"/"+polarity

            print(polDir)
            for sj in ijob.subjobs:
                sjid = sj.id
                idDir = "{0}/{1}".format(polDir,sjid)
                print(idDir)
                if not os.path.exists(idDir):
                    os.makedirs(idDir)
                sj.outputfiles[0].localDir = idDir
                sj.outputfiles[0].get()
    except:
        pass

for stripSel in stripSels:
    stripDir = dir + stripSel
    for signal in EvtTypes.keys():
        sigDir = stripDir+'/'+signal
        os.system("hadd -f6 {0}/mergedUp.root {0}/*/MagUp/*/*.root".format(sigDir))
        os.system("hadd -f6 {0}/mergedDown.root {0}/*/MagDown/*/*.root".format(sigDir))
