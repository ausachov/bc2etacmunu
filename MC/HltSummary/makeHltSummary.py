from ROOT import *

# gROOT.SetBatch(1)
# Disable INFO level logging, i.e. WARNING and up
# gErrorIgnoreLevel = kWarning
# # Disable INFO level RooFit logging
# ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
# # *Really* shut up, RooFit
# if silence_roofit:
#     ROOT.RooMsgService.instance().setSilentMode(True)

ch = TChain("Bc2JpsimunuTuple/DecayTree")
ch.Add("/eos/user/a/ausachov/MC_Bc/NoCut_allHLT/etacmu/merged.root")

branchList = ch.GetListOfBranches()
nBranches = branchList.GetEntries()

l0_rates = {}
hlt1_rates = {}
hlt2_rates = {}


hist = TH1F("br","br",10000,0,2)
for i in range(nBranches):
    obj = branchList.At(i)
    name = obj.GetName()
    if name.startswith("Bc_L0") and name.endswith("TOS"):
        ch.Draw(name+">>br")
        mean = hist.GetMean()
        l0_rates[name] = mean
    elif name.startswith("Bc_Hlt1") and name.endswith("TOS"):
        ch.Draw(name+">>br")
        mean = hist.GetMean()
        hlt1_rates[name] = mean
    elif name.startswith("Bc_Hlt2") and name.endswith("TOS"):
        ch.Draw(name+">>br")
        mean = hist.GetMean()
        hlt2_rates[name] = mean



import operator
sorted_l0_rates = sorted(l0_rates.items(), key=operator.itemgetter(1), reverse=True)
sorted_hlt1_rates = sorted(hlt1_rates.items(), key=operator.itemgetter(1), reverse=True)
sorted_hlt2_rates = sorted(hlt2_rates.items(), key=operator.itemgetter(1), reverse=True)



print "\n \t \033[1m L0 Lines: \033[0m"
for line in sorted_l0_rates:
    print "{: >25}  :  {:.2f}".format(*line)


nBestLines = 12
print "\n \033[1m HLT1 Lines: \033[0m"
for i in range(nBestLines):
    line = sorted_hlt1_rates[i]
    print "{: >80}  :  {:.2f}".format(*line)

print "\n \033[1m HLT2 Lines: \033[0m"
for i in range(nBestLines):
    line = sorted_hlt2_rates[i]
    print "{: >80}  :  {:.2f}".format(*line)
