
mcMatchPp   = "mcMatch('eta_c(1S) => ^p+  p~-', 2)"
mcMatchPm   = "mcMatch('eta_c(1S) => p+  ^p~-', 2)"
mcMatchJpsi = "mcMatch('[B_c+ => ^eta_c(1S) mu+ nu_mu]CC', 2)"
mcMatchMu   = "mcMatch('[B_c+ => eta_c(1S) ^mu+ nu_mu]CC', 2)"
mcMatchBc   = "mcMatch('[B_c+ => eta_c(1S) mu+ nu_mu]CC', 2)"
oldStr = False
local = True

from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles, FilterInTrees
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)

def fillTuple( tuple, myTriggerList ):

    from Configurables import TupleToolVtxIsoln, TupleToolIsolationTwoBody, TupleToolDira, TupleToolDocas, TupleToolTauMuDiscrVars, TupleToolSLTools

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                      "TupleToolVtxIsoln",
                      "TupleToolIsolationTwoBody",
                      "TupleToolDira"
                     ]



    from Configurables import TupleToolNeutrinoReco
    tuple.addTool( TupleToolNeutrinoReco,name = "TupleToolNeutrinoReco" )
    tuple.TupleToolNeutrinoReco.Verbose = True
    tuple.TupleToolNeutrinoReco.MotherMass=6274.9 #MeV
    tuple.TupleToolNeutrinoReco.ExtraName='mBc'

    from Configurables import TupleToolTrigger, TupleToolTISTOS, TupleToolDecay
    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')
    TJpsi = tuple.Jpsi.addTupleTool("TupleToolTISTOS")
    TJpsi.VerboseL0=True #if you want to fill info for each trigger line
    TJpsi.VerboseHlt1=True
    TJpsi.VerboseHlt2=True
    TJpsi.TriggerList = myTriggerList

    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTupleTool(TupleToolDecay, name = 'Bc')
    TBc = tuple.Bc.addTupleTool("TupleToolTISTOS")
    TBc.VerboseL0=True #if you want to fill info for each trigger line
    TBc.VerboseHlt1=True
    TBc.VerboseHlt2=True
    TBc.TriggerList = myTriggerList


    tuple.Bc.addTupleTool("TupleToolSLTools")
    tuple.Bc.addTupleTool("TupleToolSLTruth")
    tuple.Bc.addTupleTool("TupleToolTauMuDiscrVars")


    LoKi_Bc=LoKi__Hybrid__TupleTool("LoKi_Bc")
    LoKi_Bc.Preambulo = [
      "MASSES = LoKi.AuxDTFBase.MASSES",
      "massEtac = MASSES() ",
      "massEtac['J/psi(1S)'] = 2.9836 * GeV"
      ]

    varsB = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        # "m_scaled"             : "DTF_FUN ( M , False )",
        "DOCA"                 : "DOCA(1,2)",
        "m_pv"     : "DTF_FUN ( M , True )",
        "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2"  : "DTF_CHI2NDOF( True  )",
        "DTFEtac_M": 'DTF_FUN(M , False, strings( [ "J/psi(1S)"] ) , massEtac)'
      }

    LoKi_Bc.Variables = varsB
    tuple.Bc.addTupleTool(LoKi_Bc)

    from Configurables import TupleToolDecayTreeFitter
    tuple.Bc.ToolList += [ "TupleToolDecayTreeFitter/DTFJpsi"]
    tuple.Bc.addTool(TupleToolDecayTreeFitter("DTFJpsi"))
    tuple.Bc.DTFJpsi.Verbose=True
    tuple.Bc.DTFJpsi.daughtersToConstrain=["J/psi(1S)"]
    tuple.Bc.DTFJpsi.constrainToOriginVertex=False


    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth, TupleToolSLTruth
    tuple.ToolList += ["TupleToolMCBackgroundInfo"]

    MCTruth = TupleToolMCTruth("MCTruth")
    MCTruth.ToolList =  ["MCTupleToolAngles"
                         , "MCTupleToolHierarchy"
                         , "MCTupleToolKinematic"
                         , "MCTupleToolReconstructed"
                         , "MCTupleToolPID"
                         , "MCTupleToolDecayType"
                         , "MCTupleToolPrompt"
                         ]




    from LoKiMC.functions import MCMOTHER, MCVFASPF, MCPRIMARY

    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTruth.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTruth.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTruth.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTruth.MCLoKiHybrid.Variables = {
        "TRUEM"     : "MCM",
        "MCVZ"      : "MCVFASPF(MCVZ)",
        "TRUEETA"   : "MCETA",
        "TRUEPHI"   : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "TRUE_Tz"   : "(3.3)*(MCMOTHER(MCVFASPF(MCVZ), 0)-MCVFASPF(MCVZ))*MCM/MCPZ",
        "MC_ISPRIMARY"  : "MCVFASPF(switch(MCPRIMARY,1,0))"
        }
    tuple.addTupleTool(MCTruth)



    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTupleTool(LoKi_All)

    tuple.Bc.addTupleTool( 'TupleToolSubMass' )
    tuple.Bc.TupleToolSubMass.Substitution += ["p+ => pi+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["p+ => K+"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
    tuple.Bc.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => pi+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => e+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => K+"]
    tuple.Bc.TupleToolSubMass.Substitution += ["mu+ => p+"]


myTriggerList = ["L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",

                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1TrackMVATightDecision', 'Hlt1TwoTrackMVATightDecision', 'Hlt1TrackMuonDecision', 'Hlt1TrackMuonMVADecision', 'Hlt1DiMuonHighMassDecision', 'Hlt1DiMuonLowMassDecision', 'Hlt1SingleMuonHighPTDecision', 'Hlt1DiMuonNoL0Decision', 'Hlt1B2GammaGammaDecision', 'Hlt1B2HH_LTUNB_KKDecision', 'Hlt1B2HH_LTUNB_KPiDecision', 'Hlt1B2HH_LTUNB_PiPiDecision', 'Hlt1B2PhiGamma_LTUNBDecision', 'Hlt1B2PhiPhi_LTUNBDecision', 'Hlt1BeamGasBeam1Decision', 'Hlt1BeamGasBeam2Decision', 'Hlt1BeamGasNoBeamBeam1Decision', 'Hlt1BeamGasNoBeamBeam2Decision', 'Hlt1Bottomonium2KstarKstarDecision', 'Hlt1Bottomonium2PhiPhiDecision', 'Hlt1CalibHighPTLowMultTrksDecision', 'Hlt1CalibMuonAlignJpsiDecision', 'Hlt1CalibRICHMirrorRICH1Decision', 'Hlt1CalibRICHMirrorRICH2Decision', 'Hlt1CalibTrackingKKDecision', 'Hlt1CalibTrackingKPiDecision', 'Hlt1CalibTrackingKPiDetachedDecision', 'Hlt1CalibTrackingPiPiDecision', 'Hlt1DiMuonNoIPDecision', 'Hlt1DiMuonNoIPSSDecision', 'Hlt1DiProtonDecision', 'Hlt1DiProtonLowMultDecision', 'Hlt1IncPhiDecision', 'Hlt1L0AnyDecision', 'Hlt1L0AnyNoSPDDecision', 'Hlt1LowMultMaxVeloAndHerschelDecision', 'Hlt1LowMultMaxVeloCutDecision', 'Hlt1LowMultMuonDecision', 'Hlt1LowMultPassThroughDecision', 'Hlt1LowMultVeloAndHerschel_HadronsDecision', 'Hlt1LowMultVeloAndHerschel_LeptonsDecision', 'Hlt1LowMultVeloCut_HadronsDecision', 'Hlt1LowMultVeloCut_LeptonsDecision', 'Hlt1LumiDecision', 'Hlt1MBNoBiasDecision', 'Hlt1MultiDiMuonNoIPDecision', 'Hlt1MultiMuonNoL0Decision', 'Hlt1NoBiasEmptyEmptyDecision', 'Hlt1NoBiasNonBeamBeamDecision', 'Hlt1ODINTechnicalDecision', 'Hlt1SingleElectronNoIPDecision', 'Hlt1SingleMuonHighPTNoMUIDDecision', 'Hlt1SingleMuonNoIPDecision', 'Hlt1Tell1ErrorDecision', 'Hlt1VeloClosingMicroBiasDecision', 'Hlt1ErrorEventDecision',
'Hlt2B2HH_B2HHDecision', 'Hlt2B2HH_B2KKDecision', 'Hlt2B2HH_B2KPiDecision', 'Hlt2B2HH_B2PiPiDecision', 'Hlt2B2HH_Lb2PKDecision', 'Hlt2B2HH_Lb2PPiDecision', 'Hlt2B2Kpi0_B2K0pi0Decision', 'Hlt2B2Kpi0_B2Kpi0Decision', 'Hlt2BHadB02PpPpPmPmDecision', 'Hlt2Bc2JpsiXTFBc2JpsiMuXDecision', 'Hlt2BeamGasDecision', 'Hlt2BottomoniumDiKstarTurboDecision', 'Hlt2BottomoniumDiPhiDecision', 'Hlt2CaloPIDBd2KstGammaTurboCalibDecision', 'Hlt2CaloPIDBs2PhiGammaTurboCalibDecision', 'Hlt2CaloPIDD2EtapPiTurboCalibDecision', 'Hlt2CaloPIDDsst2DsGammaTurboCalibDecision', 'Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalibDecision', 'Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalibDecision', 'Hlt2CaloPIDEta2MuMuGammaTurboCalibDecision', 'Hlt2CcDiHadronDiPhiDecision', 'Hlt2CcDiHadronDiProtonDecision', 'Hlt2CcDiHadronDiProtonLowMultDecision', 'Hlt2CharmHadD02KmPipTurboDecision', 'Hlt2CharmHadDp2EtaKp_Eta2EmEpGDecision', 'Hlt2CharmHadDp2EtaKp_Eta2PimPipGDecision', 'Hlt2CharmHadDp2EtaKp_Eta2PimPipPi0_Pi0MDecision', 'Hlt2CharmHadDp2EtaKp_Eta2PimPipPi0_Pi0RDecision', 'Hlt2CharmHadDp2EtaPip_Eta2EmEpGDecision', 'Hlt2CharmHadDp2EtaPip_Eta2PimPipGDecision', 'Hlt2CharmHadDp2EtaPip_Eta2PimPipPi0_Pi0MDecision', 'Hlt2CharmHadDp2EtaPip_Eta2PimPipPi0_Pi0RDecision', 'Hlt2CharmHadDp2EtapKp_Etap2EtaPimPip_EtaRDecision', 'Hlt2CharmHadDp2EtapKp_Etap2PimPipGDecision', 'Hlt2CharmHadDp2EtapPip_Etap2EtaPimPip_EtaRDecision', 'Hlt2CharmHadDp2EtapPip_Etap2PimPipGDecision', 'Hlt2CharmHadDp2KS0KS0KpTurboDecision', 'Hlt2CharmHadDp2KS0KS0PipTurboDecision', 'Hlt2CharmHadDp2KS0KmKpPip_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0KmKpPip_KS0LLTurboDecision', 'Hlt2CharmHadDp2KS0KmPipPip_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0KmPipPip_KS0LLTurboDecision', 'Hlt2CharmHadDp2KS0KpKpPim_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0KpKpPim_KS0LLTurboDecision', 'Hlt2CharmHadDp2KS0KpPimPip_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0KpPimPip_KS0LLTurboDecision', 'Hlt2CharmHadDp2KS0Kp_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0Kp_KS0LLTurboDecision', 'Hlt2CharmHadDp2KS0PimPipPip_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0PimPipPip_KS0LLTurboDecision', 'Hlt2CharmHadDp2KS0Pip_KS0DDTurboDecision', 'Hlt2CharmHadDp2KS0Pip_KS0LLTurboDecision', 'Hlt2CharmHadDp2KmKmKpPipPipTurboDecision', 'Hlt2CharmHadDp2KmKpPimPipPipTurboDecision', 'Hlt2CharmHadDp2KmPimPipPipPipTurboDecision', 'Hlt2CharmHadDp2KpPi0_Pi02EmEpGDecision', 'Hlt2CharmHadDp2PipPi0_Pi02EmEpGDecision', 'Hlt2CharmHadDpDsp2KmKpKpPi0Decision', 'Hlt2CharmHadDpDsp2KmKpPipPi0Decision', 'Hlt2CharmHadDpDsp2KmPipPipPi0Decision', 'Hlt2CharmHadDpDsp2KpKpPimPi0Decision', 'Hlt2CharmHadDpDsp2KpPimPipPi0Decision', 'Hlt2CharmHadDpDsp2PimPipPipPi0Decision', 'Hlt2CharmHadDpToKmKpKpTurboDecision', 'Hlt2CharmHadDpToKmKpPipTurboDecision', 'Hlt2CharmHadDpToKmPipPipTurboDecision', 'Hlt2CharmHadDpToKmPipPip_ForKPiAsymTurboDecision', 'Hlt2CharmHadDpToKmPipPip_LTUNBTurboDecision', 'Hlt2CharmHadDpToKpKpPimTurboDecision', 'Hlt2CharmHadDpToKpPimPipTurboDecision', 'Hlt2CharmHadDpToPimPipPipTurboDecision', 'Hlt2CharmHadDsp2KS0KS0KpTurboDecision', 'Hlt2CharmHadDsp2KS0KS0PipTurboDecision', 'Hlt2CharmHadDsp2KS0KmKpPip_KS0DDTurboDecision', 'Hlt2CharmHadDsp2KS0KmKpPip_KS0LLTurboDecision', 'Hlt2CharmHadDsp2KS0KmPipPip_KS0DDTurboDecision', 'Hlt2CharmHadDsp2KS0KmPipPip_KS0LLTurboDecision', 'Hlt2CharmHadDsp2KS0KpKpPim_KS0DDTurboDecision', 'Hlt2CharmHadDsp2KS0KpKpPim_KS0LLTurboDecision', 'Hlt2CharmHadDsp2KS0KpPimPip_KS0DDTurboDecision', 'Hlt2CharmHadDsp2KS0KpPimPip_KS0LLTurboDecision', 'Hlt2CharmHadDsp2KS0PimPipPip_KS0DDTurboDecision', 'Hlt2CharmHadDsp2KS0PimPipPip_KS0LLTurboDecision', 'Hlt2CharmHadDsp2KmKmKpPipPipTurboDecision', 'Hlt2CharmHadDsp2KmKpPimPipPipTurboDecision', 'Hlt2CharmHadDsp2KmPimPipPipPipTurboDecision', 'Hlt2CharmHadDspToKmKpKpTurboDecision', 'Hlt2CharmHadDspToKmKpPipDecision', 'Hlt2CharmHadDspToKmKpPipTurboDecision', 'Hlt2CharmHadDspToKmKpPip_LTUNBTurboDecision', 'Hlt2CharmHadDspToKmPipPipTurboDecision', 'Hlt2CharmHadDspToKpKpPimTurboDecision', 'Hlt2CharmHadDspToKpPimPipTurboDecision', 'Hlt2CharmHadDspToPimPipPipTurboDecision', 'Hlt2CharmHadDst2PiD02KKGammaDecision', 'Hlt2CharmHadDst2PiD02KPiGammaDecision', 'Hlt2CharmHadDst2PiD02PiPiGammaDecision', 'Hlt2CharmHadDstp2D0Pip_D02EmEpDecision', 'Hlt2CharmHadDstp2D0Pip_D02GG_G2EmEpDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DDTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DD_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LLTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LL_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DDTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LLTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DDTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LLTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DDTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LLTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LL_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKmKpPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKpKpPimTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0MDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0RDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKpPimPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKpTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmPimPipPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0MDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0RDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0MDecision', 'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0RDecision', 'Hlt2CharmHadDstp2D0Pip_D02KpPimPimPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KpPimTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02KpPim_LTUNBTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02PimPimPipPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0MDecision', 'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0RDecision', 'Hlt2CharmHadDstp2D0Pip_D02PimPipTurboDecision', 'Hlt2CharmHadDstp2D0Pip_D02PimPip_LTUNBTurboDecision', 'Hlt2CharmHadInclDst2PiD02HHXBDTDecision', 'Hlt2CharmHadInclLcpToKmPpPipBDTDecision', 'Hlt2CharmHadInclSigc2PiLc2HHXBDTDecision', 'Hlt2CharmHadLc2LambdaPipPimPip_LamDDDecision', 'Hlt2CharmHadLc2LambdaPipPimPip_LamLLDecision', 'Hlt2CharmHadLc2XimKpPip_XimDDDDecision', 'Hlt2CharmHadLc2XimKpPip_XimDDLDecision', 'Hlt2CharmHadLc2XimKpPip_XimLLLDecision', 'Hlt2CharmHadLcp2KS0KS0PpTurboDecision', 'Hlt2CharmHadLcp2LamKmKpPip_Lam2PpPimTurboDecision', 'Hlt2CharmHadLcp2LamKmPipPip_Lam2PpPimTurboDecision', 'Hlt2CharmHadLcp2LamKp_LamDDTurboDecision', 'Hlt2CharmHadLcp2LamKp_LamLLTurboDecision', 'Hlt2CharmHadLcp2LamPip_LamDDTurboDecision', 'Hlt2CharmHadLcp2LamPip_LamLLTurboDecision', 'Hlt2CharmHadLcpToPpKmKmPipPipTurboDecision', 'Hlt2CharmHadLcpToPpKmKpPimPipTurboDecision', 'Hlt2CharmHadLcpToPpKmKpTurboDecision', 'Hlt2CharmHadLcpToPpKmPimPipPipTurboDecision', 'Hlt2CharmHadLcpToPpKmPipTurboDecision', 'Hlt2CharmHadLcpToPpKmPip_LTUNBTurboDecision', 'Hlt2CharmHadLcpToPpKpKpPimPimTurboDecision', 'Hlt2CharmHadLcpToPpKpPimPimPipTurboDecision', 'Hlt2CharmHadLcpToPpKpPimTurboDecision', 'Hlt2CharmHadLcpToPpPimPimPipPipTurboDecision', 'Hlt2CharmHadLcpToPpPimPipTurboDecision', 'Hlt2CharmHadOmccp2DpPpKmKmPip_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadOmccp2LcpKmKmPipPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadOmccp2Xic0KmPipPip_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadOmccp2XicpKmPip_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadOmm2LamKm_DDDTurboDecision', 'Hlt2CharmHadOmm2LamKm_DDLTurboDecision', 'Hlt2CharmHadOmm2LamKm_LLLTurboDecision', 'Hlt2CharmHadPentaToPhiPpPimTurboDecision', 'Hlt2CharmHadPentaToPhiPpTurboDecision', 'Hlt2CharmHadPromptH2LamLamBar_LamDDTurboDecision', 'Hlt2CharmHadPromptH2LamLamBar_LamLLTurboDecision', 'Hlt2CharmHadPromptH2LamLamBar_LamLL_LamDDTurboDecision', 'Hlt2CharmHadPromptH2LamLam_LamDDTurboDecision', 'Hlt2CharmHadPromptH2LamLam_LamLLTurboDecision', 'Hlt2CharmHadPromptH2LamLam_LamLL_LamDDTurboDecision', 'Hlt2CharmHadSecondaryH2LamPipPmDDDDTurboDecision', 'Hlt2CharmHadSecondaryH2LamPipPmDDLLTurboDecision', 'Hlt2CharmHadSecondaryH2LamPipPmLLLLTurboDecision', 'Hlt2CharmHadSecondaryH2LamPpPimDDDDTurboDecision', 'Hlt2CharmHadSecondaryH2LamPpPimDDLLTurboDecision', 'Hlt2CharmHadSecondaryH2LamPpPimLLLLTurboDecision', 'Hlt2CharmHadXic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurboDecision', 'Hlt2CharmHadXic2LambdaKmPipPip_LamDDDecision', 'Hlt2CharmHadXic2LambdaKmPipPip_LamLLDecision', 'Hlt2CharmHadXic2LambdaKmPip_LamDDDecision', 'Hlt2CharmHadXic2LambdaKmPip_LamLLDecision', 'Hlt2CharmHadXic2LambdaKpKm_LamDDDecision', 'Hlt2CharmHadXic2LambdaKpKm_LamLLDecision', 'Hlt2CharmHadXic2XimKp_XimDDDDecision', 'Hlt2CharmHadXic2XimKp_XimDDLDecision', 'Hlt2CharmHadXic2XimKp_XimLLLDecision', 'Hlt2CharmHadXic2XimPipPimPip_XimDDDDecision', 'Hlt2CharmHadXic2XimPipPimPip_XimDDLDecision', 'Hlt2CharmHadXic2XimPipPimPip_XimLLLDecision', 'Hlt2CharmHadXic2XimPipPip_XimDDDDecision', 'Hlt2CharmHadXic2XimPipPip_XimDDLDecision', 'Hlt2CharmHadXic2XimPipPip_XimLLLDecision', 'Hlt2CharmHadXic2XimPip_XimDDDDecision', 'Hlt2CharmHadXic2XimPip_XimDDLDecision', 'Hlt2CharmHadXic2XimPip_XimLLLDecision', 'Hlt2CharmHadXiccp2D0PpKmPim_D02KmPipTurboDecision', 'Hlt2CharmHadXiccp2D0PpKmPip_D02KmPipTurboDecision', 'Hlt2CharmHadXiccp2D0PpKpPim_D02KmPipTurboDecision', 'Hlt2CharmHadXiccp2D0Pp_D02KmPipTurboDecision', 'Hlt2CharmHadXiccp2DpPpKm_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccp2DpPpKp_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccp2DpPpPim_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccp2DspPpKm_Dsp2KmKpPipTurboDecision', 'Hlt2CharmHadXiccp2LcpKmPim_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccp2LcpKmPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccp2LcpKpPim_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccp2LcpPimPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccp2Xic0Kp_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccp2Xic0Pim_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccp2Xic0Pip_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccp2XicpKpPim_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccp2XicpPimPim_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccp2XicpPimPip_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2D0PpKmPimPip_D02KmPipTurboDecision', 'Hlt2CharmHadXiccpp2D0PpKmPipPip_D02KmPipTurboDecision', 'Hlt2CharmHadXiccpp2D0PpKpPimPip_D02KmPipTurboDecision', 'Hlt2CharmHadXiccpp2D0PpPip_D02KmPipTurboDecision', 'Hlt2CharmHadXiccpp2DpPpKmPim_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccpp2DpPpKmPip_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccpp2DpPpKpPip_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccpp2DpPp_Dp2KmPipPipTurboDecision', 'Hlt2CharmHadXiccpp2DspPpKmPip_Dsp2KmKpPipTurboDecision', 'Hlt2CharmHadXiccpp2LcpKmPimPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2LcpKmPipPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2LcpKpPimPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2LcpPim_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2LcpPip_Lcp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2Omc0KpKp_Omc0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccpp2Xic0KpPip_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccpp2Xic0PimPip_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccpp2Xic0PipPip_Xic0ToPpKmKmPipTurboDecision', 'Hlt2CharmHadXiccpp2XicpKp_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2XicpPim_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadXiccpp2XicpPip_Xicp2PpKmPipTurboDecision', 'Hlt2CharmHadXicpToPpKmPipTurboDecision', 'Hlt2CharmHadXim2LamPim_DDDTurboDecision', 'Hlt2CharmHadXim2LamPim_DDLTurboDecision', 'Hlt2CharmHadXim2LamPim_LLLTurboDecision', 'Hlt2DPS2mu2HcDecision', 'Hlt2DPS2muHcDecision', 'Hlt2DPS2x2muDecision', 'Hlt2DPS2x2muHcDecision', 'Hlt2DPS2xHcDecision', 'Hlt2DPS3x2muDecision', 'Hlt2DPS3xHcDecision', 'Hlt2DebugEventDecision', 'Hlt2DiElectronElSoftDecision', 'Hlt2DiElectronJPsiEETurboDecision', 'Hlt2DiMuonBDecision', 'Hlt2DiMuonBPromptDecision', 'Hlt2DiMuonBUBDecision', 'Hlt2DiMuonChicXTurboDecision', 'Hlt2DiMuonDetachedDecision', 'Hlt2DiMuonDetachedHeavyDecision', 'Hlt2DiMuonDetachedJPsiDecision', 'Hlt2DiMuonDetachedPsi2SDecision', 'Hlt2DiMuonJPsiDecision', 'Hlt2DiMuonJPsiHighPTDecision', 'Hlt2DiMuonJPsiTurboDecision', 'Hlt2DiMuonPsi2SDecision', 'Hlt2DiMuonPsi2SHighPTDecision', 'Hlt2DiMuonPsi2STurboDecision', 'Hlt2DiMuonSoftDecision', 'Hlt2DiMuonUpsilonTurboDecision', 'Hlt2DiMuonZDecision', 'Hlt2DisplVerticesDoubleDecision', 'Hlt2DisplVerticesDoublePSDecision', 'Hlt2DisplVerticesSingleDecision', 'Hlt2DisplVerticesSingleHighFDDecision', 'Hlt2DisplVerticesSingleHighMassDecision', 'Hlt2DisplVerticesSingleLoosePSDecision', 'Hlt2DisplVerticesSinglePSDecision', 'Hlt2DisplVerticesSingleVeryHighFDDecision', 'Hlt2EWConvPhotonDDHighPtDecision', 'Hlt2EWConvPhotonDDVHighPtDecision', 'Hlt2EWConvPhotonLLHighPtDecision', 'Hlt2EWConvPhotonLLVHighPtDecision', 'Hlt2EWDiElectronDYDecision', 'Hlt2EWDiElectronHighMassDecision', 'Hlt2EWDiMuonDY1Decision', 'Hlt2EWDiMuonDY2Decision', 'Hlt2EWDiMuonDY3Decision', 'Hlt2EWDiMuonDY4Decision', 'Hlt2EWDiMuonZDecision', 'Hlt2EWSingleElectronHighPtDecision', 'Hlt2EWSingleElectronLowPtDecision', 'Hlt2EWSingleElectronVHighPtDecision', 'Hlt2EWSingleMuonHighPtDecision', 'Hlt2EWSingleMuonHighPtNoMUIDDecision', 'Hlt2EWSingleMuonLowPtDecision', 'Hlt2EWSingleMuonVHighPtDecision', 'Hlt2ExoticaDiMuonNoIPSSTurboDecision', 'Hlt2ExoticaDiMuonNoIPTurboDecision', 'Hlt2ExoticaDisplDiEDecision', 'Hlt2ExoticaDisplDiMuonDecision', 'Hlt2ExoticaDisplDiMuonNoPointDecision', 'Hlt2ExoticaDisplPhiPhiDecision', 'Hlt2ExoticaLFVPrmptDecision', 'Hlt2ExoticaLFVPrmptSSTurboDecision', 'Hlt2ExoticaLFVPrmptTurboDecision', 'Hlt2ExoticaPrmptDiMuonSSTurboDecision', 'Hlt2ExoticaPrmptDiMuonTurboDecision', 'Hlt2ExoticaQuadMuonNoIPDecision', 'Hlt2ExoticaRHNuDecision', 'Hlt2ForwardDecision', 'Hlt2Hb2V0V0Hb2KSKSDDDecision', 'Hlt2Hb2V0V0Hb2KSKSLDDecision', 'Hlt2Hb2V0V0Hb2KSKSLLDecision', 'Hlt2JetsDiJetMuMuDecision', 'Hlt2JetsDiJetMuMuLowPtDecision', 'Hlt2JetsDiJetSVDecision', 'Hlt2JetsDiJetSVHighPtDecision', 'Hlt2JetsDiJetSVLowPtDecision', 'Hlt2JetsDiJetSVMuDecision', 'Hlt2JetsDiJetSVMuLowPtDecision', 'Hlt2JetsDiJetSVSVDecision', 'Hlt2JetsDiJetSVSVLowPtDecision', 'Hlt2JetsJetLowPtDecision', 'Hlt2JetsJetSVDecision', 'Hlt2LFVJpsiMuETurboDecision', 'Hlt2LFVPhiMuETurboDecision', 'Hlt2LFVPromptPhiMuETurboDecision', 'Hlt2LFVUpsilonMuETurboDecision', 'Hlt2LowMultChiC2HHDecision', 'Hlt2LowMultChiC2HHHHDecision', 'Hlt2LowMultChiC2HHHHWSDecision', 'Hlt2LowMultChiC2HHWSDecision', 'Hlt2LowMultChiC2PPDecision', 'Hlt2LowMultChiC2PPWSDecision', 'Hlt2LowMultD2K3PiDecision', 'Hlt2LowMultD2K3PiWSDecision', 'Hlt2LowMultD2KKPiDecision', 'Hlt2LowMultD2KKPiWSDecision', 'Hlt2LowMultD2KPiDecision', 'Hlt2LowMultD2KPiPiDecision', 'Hlt2LowMultD2KPiPiWSDecision', 'Hlt2LowMultD2KPiWSDecision', 'Hlt2LowMultDiElectronDecision', 'Hlt2LowMultDiElectron_noTrFiltDecision', 'Hlt2LowMultDiMuonDecision', 'Hlt2LowMultDiMuon_PSDecision', 'Hlt2LowMultDiPhotonDecision', 'Hlt2LowMultDiPhoton_HighMassDecision', 'Hlt2LowMultHadron_noTrFiltDecision', 'Hlt2LowMultL2pPiDecision', 'Hlt2LowMultL2pPiWSDecision', 'Hlt2LowMultLMR2HHDecision', 'Hlt2LowMultLMR2HHHHDecision', 'Hlt2LowMultLMR2HHHHWSDecision', 'Hlt2LowMultLMR2HHWSDecision', 'Hlt2LowMultMuonDecision', 'Hlt2LowMultPi0Decision', 'Hlt2LowMultTechnical_MinBiasDecision', 'Hlt2LumiDecision', 'Hlt2MBNoBiasDecision', 'Hlt2MajoranaBLambdaMuDDDecision', 'Hlt2MajoranaBLambdaMuLLDecision', 'Hlt2NoBiasNonBeamBeamDecision', 'Hlt2PIDB2KJPsiEENegTaggedTurboCalibDecision', 'Hlt2PIDB2KJPsiEEPosTaggedTurboCalibDecision', 'Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalibDecision', 'Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalibDecision', 'Hlt2PIDD02KPiTagTurboCalibDecision', 'Hlt2PIDDetJPsiEEPosNegTaggedTurboCalibDecision', 'Hlt2PIDDetJPsiMuMuNegTaggedTurboCalibDecision', 'Hlt2PIDDetJPsiMuMuPosTaggedTurboCalibDecision', 'Hlt2PIDDs2PiPhiKKNegTaggedTurboCalibDecision', 'Hlt2PIDDs2PiPhiKKPosTaggedTurboCalibDecision', 'Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalibDecision', 'Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalibDecision', 'Hlt2PIDKs2PiPiDDTurboCalibDecision', 'Hlt2PIDKs2PiPiLLTurboCalibDecision', 'Hlt2PIDLambda2PPiDDTurboCalibDecision', 'Hlt2PIDLambda2PPiDDhighPTTurboCalibDecision', 'Hlt2PIDLambda2PPiDDveryhighPTTurboCalibDecision', 'Hlt2PIDLambda2PPiLLTurboCalibDecision', 'Hlt2PIDLambda2PPiLLhighPTTurboCalibDecision', 'Hlt2PIDLambda2PPiLLisMuonTurboCalibDecision', 'Hlt2PIDLambda2PPiLLveryhighPTTurboCalibDecision', 'Hlt2PIDLb2LcMuNuTurboCalibDecision', 'Hlt2PIDLc2KPPiTurboCalibDecision', 'Hlt2PIDOmega2LambdaKDDDTurboCalibDecision', 'Hlt2PIDOmega2LambdaKLLLTurboCalibDecision', 'Hlt2PassThroughDecision', 'Hlt2PhiBs2PhiPhiDecision', 'Hlt2PhiIncPhiDecision', 'Hlt2PhiPhi2EETurboDecision', 'Hlt2PhiPhi2KsKsDecision', 'Hlt2PhiPhi2KsKsD0CtrlDecision', 'Hlt2PhiPromptPhi2EETurboDecision', 'Hlt2RadiativeB2GammaGammaDecision', 'Hlt2RadiativeB2GammaGammaDDDecision', 'Hlt2RadiativeB2GammaGammaDoubleDecision', 'Hlt2RadiativeB2GammaGammaLLDecision', 'Hlt2RadiativeBd2KstGammaDecision', 'Hlt2RadiativeBd2KstGammaULUnbiasedDecision', 'Hlt2RadiativeBs2PhiGammaDecision', 'Hlt2RadiativeBs2PhiGammaUnbiasedDecision', 'Hlt2RadiativeHypb2L0HGammaOmDecision', 'Hlt2RadiativeHypb2L0HGammaOmEEDecision', 'Hlt2RadiativeHypb2L0HGammaXiDecision', 'Hlt2RadiativeHypb2L0HGammaXiEEDecision', 'Hlt2RadiativeIncHHGammaDecision', 'Hlt2RadiativeIncHHGammaEEDecision', 'Hlt2RadiativeIncHHHGammaDecision', 'Hlt2RadiativeIncHHHGammaEEDecision', 'Hlt2RadiativeLb2L0GammaEELLDecision', 'Hlt2RadiativeLb2L0GammaLLDecision', 'Hlt2RareCharmD02EMuDecision', 'Hlt2RareCharmD02KKMuMuDecision', 'Hlt2RareCharmD02KKMueDecision', 'Hlt2RareCharmD02KKeeDecision', 'Hlt2RareCharmD02KMuDecision', 'Hlt2RareCharmD02KPiDecision', 'Hlt2RareCharmD02KPiMuMuDecision', 'Hlt2RareCharmD02KPiMuMuSSDecision', 'Hlt2RareCharmD02KPiMuMuUntagDecision', 'Hlt2RareCharmD02KPiMueDecision', 'Hlt2RareCharmD02KPieeDecision', 'Hlt2RareCharmD02MuMuDecision', 'Hlt2RareCharmD02PiPiDecision', 'Hlt2RareCharmD02PiPiMuMuDecision', 'Hlt2RareCharmD02PiPiMueDecision', 'Hlt2RareCharmD02PiPieeDecision', 'Hlt2RareCharmD2KEEOSDecision', 'Hlt2RareCharmD2KEESSDecision', 'Hlt2RareCharmD2KEEWSDecision', 'Hlt2RareCharmD2KEMuOSDecision', 'Hlt2RareCharmD2KMuEOSDecision', 'Hlt2RareCharmD2KMuESSDecision', 'Hlt2RareCharmD2KMuEWSDecision', 'Hlt2RareCharmD2KMuMuOSDecision', 'Hlt2RareCharmD2KMuMuSSDecision', 'Hlt2RareCharmD2KMuMuWSDecision', 'Hlt2RareCharmD2PiEEOSDecision', 'Hlt2RareCharmD2PiEESSDecision', 'Hlt2RareCharmD2PiEEWSDecision', 'Hlt2RareCharmD2PiEMuOSDecision', 'Hlt2RareCharmD2PiMuEOSDecision', 'Hlt2RareCharmD2PiMuESSDecision', 'Hlt2RareCharmD2PiMuEWSDecision', 'Hlt2RareCharmD2PiMuMuOSDecision', 'Hlt2RareCharmD2PiMuMuSSDecision', 'Hlt2RareCharmD2PiMuMuWSDecision', 'Hlt2RareCharmLc2PMuMuDecision', 'Hlt2RareCharmLc2PMuMuSSDecision', 'Hlt2RareCharmLc2PMueDecision', 'Hlt2RareCharmLc2PeeDecision', 'Hlt2RareStrangeKPiMuMuDecision', 'Hlt2RareStrangeKPiMuMuSSDecision', 'Hlt2RareStrangeKsPiPiEETOSDecision', 'Hlt2RareStrangeSigmaPMuMuDecision', 'Hlt2SLB_B2D0Mu_D02KmKpTurboDecision', 'Hlt2SLB_B2D0Mu_D02KmPipTurboDecision', 'Hlt2SLB_B2D0Mu_D02PimPipTurboDecision', 'Hlt2SLB_B2DstMu_D02KmKpTurboDecision', 'Hlt2SLB_B2DstMu_D02KmPipTurboDecision', 'Hlt2SLB_B2DstMu_D02PimPipTurboDecision', 'Hlt2SingleMuonDecision', 'Hlt2SingleMuonHighPTDecision', 'Hlt2SingleMuonLowPTDecision', 'Hlt2SingleMuonRareDecision', 'Hlt2SingleMuonVHighPTDecision', 'Hlt2StrangeKPiPiPiTurboDecision', 'Hlt2StrangeLFVMuonElectronSoftDecision', 'Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision', 'Hlt2Topo4BodyDecision', 'Hlt2TopoE2BodyDecision', 'Hlt2TopoE3BodyDecision', 'Hlt2TopoE4BodyDecision', 'Hlt2TopoEE2BodyDecision', 'Hlt2TopoEE3BodyDecision', 'Hlt2TopoEE4BodyDecision', 'Hlt2TopoMu2BodyDecision', 'Hlt2TopoMu3BodyDecision', 'Hlt2TopoMu4BodyDecision', 'Hlt2TopoMuE2BodyDecision', 'Hlt2TopoMuE3BodyDecision', 'Hlt2TopoMuE4BodyDecision', 'Hlt2TopoMuMu2BodyDecision', 'Hlt2TopoMuMu3BodyDecision', 'Hlt2TopoMuMu4BodyDecision', 'Hlt2TopoMuMuDDDecision', 'Hlt2TrackEffDiMuonDownstreamMinusHighStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamMinusHighStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamMinusLowStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamMinusLowStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamPlusHighStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamPlusHighStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamPlusLowStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonDownstreamPlusLowStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTMinusHighStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTMinusHighStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTMinusLowStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTMinusLowStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTPlusHighStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTPlusHighStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTPlusLowStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonMuonTTPlusLowStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonMinusHighStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonMinusHighStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonMinusLowStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonMinusLowStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonPlusHighStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonPlusHighStatTaggedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonPlusLowStatMatchedTurboCalibDecision', 'Hlt2TrackEffDiMuonVeloMuonPlusLowStatTaggedTurboCalibDecision', 'Hlt2TrackEffElectronDetachedEEKTurboCalibDecision', 'Hlt2TrackEffElectronDetachedEKTurboCalibDecision', 'Hlt2TrackEffElectronDetachedEPiTurboCalibDecision', 'Hlt2TrackEffElectronDetachedMuKTurboCalibDecision', 'Hlt2TrackEffElectronDetachedMuMuKTurboCalibDecision', 'Hlt2TrackEffElectronDetachedMuPiTurboCalibDecision', 'Hlt2TrackEff_D0ToK3piPionProbeTurboDecision', 'Hlt2TrackEff_D0ToKpiKaonProbeTurboDecision', 'Hlt2TrackEff_D0ToKpiPionProbeTurboDecision', 'Hlt2TransparentDecision', 'Hlt2TriMuonDetachedDecision', 'Hlt2TriMuonTau23MuDecision', 'Hlt2XcMuXForTauB2XcFakeMuDecision', 'Hlt2XcMuXForTauB2XcMuDecision']


MyPreambulo = [
    'from LoKiPhysMC.decorators import *',
    'from LoKiPhysMC.functions import mcMatch' ,
    'from LoKiCore.functions import monitor'
]

from StandardParticles import StdLooseProtons as Protons
from StandardParticles import StdLooseMuons as Muons


import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection

jpsi_daughters = {
  'p+'  : mcMatchPp,
  'p~-' : mcMatchPm
}
jpsi_comb = "in_range(2.0*GeV, AM, 4.0*GeV)"
jpsi_mom = "in_range(2.0*GeV, MM, 4.0*GeV)"
jpsi_sel = SimpleSelection(
    'Sel_jpsi',
    ConfigurableGenerators.CombineParticles,
    [Protons],
    DecayDescriptor='J/psi(1S) -> p+ p~-',
    Preambulo       = MyPreambulo,
    DaughtersCuts=jpsi_daughters,
    CombinationCut=jpsi_comb,
    MotherCut=jpsi_mom
)

bc_comb = "in_range(3.0*GeV, AM, 8.0*GeV)"
# bc_mom  = mcMatchBc+" & \
#            in_range(3.2*GeV, MM, 6.8*GeV) & in_range(3.2*GeV, BPVCORRM, 6.8*GeV) & (VFASPF(VCHI2/VDOF) < 9.) & (PT > 3000)"
bc_mom  = mcMatchBc+" & in_range(3.0*GeV, MM, 8.0*GeV)"
bc_daughters = {
 "mu+"       : mcMatchMu,
 "J/psi(1S)" : mcMatchJpsi
}

bc_sel = SimpleSelection(
    'Sel_Bc',
    ConfigurableGenerators.CombineParticles,
    [jpsi_sel, Muons],
    DecayDescriptor="[B_c+ -> J/psi(1S) mu+]cc",
    Preambulo       = MyPreambulo,
    DaughtersCuts= bc_daughters,
    CombinationCut=bc_comb,
    MotherCut=bc_mom
)
bc_seq = SelectionSequence('bc_seq', TopSelection=bc_sel)


Bc2JpsimunuDecay = "[B_c+ ->^(J/psi(1S) -> ^p+ ^p~-) ^mu+]CC"
Bc2JpsimunuBranches = {
     "Bc"       :  "[B_c+ -> (J/psi(1S) -> p+  p~-)  mu+]CC",
     "Jpsi"     :  "[B_c+ ->^(J/psi(1S) -> p+  p~-)  mu+]CC",
     "ProtonP"  :  "[B_c+ -> (J/psi(1S) ->^p+  p~-)  mu+]CC",
     "ProtonM"  :  "[B_c+ -> (J/psi(1S) -> p+ ^p~-)  mu+]CC",
     "Mu"       :  "[B_c+ -> (J/psi(1S) -> p+  p~-) ^mu+]CC"
}

Bc2JpsimunuTuple = DecayTreeTuple('Bc2JpsimunuTuple')
Bc2JpsimunuTuple.Inputs   = [bc_sel.outputLocation()]
Bc2JpsimunuTuple.Decay    =  Bc2JpsimunuDecay
Bc2JpsimunuTuple.Branches =  Bc2JpsimunuBranches
fillTuple( Bc2JpsimunuTuple, myTriggerList)



year = "2017"
from Configurables import CondDB
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = True
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
# DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [bc_seq.sequence(), Bc2JpsimunuTuple]       # The algorithms

DaVinci().InputType = "DST"

DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]


# if local:
#     from Configurables import LHCbApp
#     LHCbApp().DDDBtag   = "dddb-20170721-3"
#     LHCbApp().CondDBtag = "sim-20190430-1-vc-mu100"
#
#     from GaudiConf import IOHelper
#     IOHelper().inputFiles(['../{}.dst'.format(signal)], clear=True)

DaVinci().TupleFile      = "etacmu.root"

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20190430-1-vc-mu100"

from GaudiConf import IOHelper
IOHelper().inputFiles(['../testDst/etacmu.dst'], clear=True)
