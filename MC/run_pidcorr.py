import os
import sys

# Use this before running
# lb-run -c best --nightly lhcb-head Urania/HEAD bash


## START OF CONFIG
# Read comments and check vars
# at least until end of config section

# List of input ROOT files with MC ntuples. Format:
#   (inputfile, outputfile, dataset)


# files = [
#   ("root://eoslhcb.cern.ch//eos/lhcb/wg/PID/PIDGen/Validation/Lb2Lcpi/sim08_2012_md.root", "sim08_2012_md_pidcorr.root", "MagDown_2017"),
# ]
EvtTypes = {
            #  "etacmu"    : "14543021"
            # ,"jpsimu"    : "14543022"
            # ,"etactau"   : "14543031"
            # ,"jpsitau"   : "14543032"
            # ,"chic0mu"   : "14543211"
            # ,"chic1mu"   : "14543212"
            # ,"chic2mu"   : "14543213"
            # ,"hcmu"      : "14543214"
            # ,"psi2smu"   : "14545021"
            # ,"Bd2JpsiX"  : "11442001" #deprecated
            # ,"Bu2JpsiX"  : "12442001" #deprecated,
            "b2JpsiX"   : "10030000"
            ,"b2EtacX"   : "10030060"
            }

dir = "/eos/user/a/ausachov/MC_Bc/"
# stripSels = ['oldStrNoMcorrNoPROBNN','newStrNoMcorrNoPROBNN','NoCut']
stripSels = ['NoCut']
files_pidcorr = []
files_pidgen  = []
for stripSel in stripSels:
    stripDir = dir + stripSel
    for signal in EvtTypes.keys():
        for year in ['2016','2017','2018']:
            sigDir = stripDir+'/'+signal+'/'+year
            os.system("rm -rf {}/mergedUp_pid*.root".format(sigDir))
            os.system("rm -rf {}/mergedDown_pid*.root".format(sigDir))
            os.system("rm -rf {}/mergedDown_app.root".format(sigDir))
            os.system("rm -rf {}/mergedUp_app.root".format(sigDir))

            if os.path.exists(sigDir+"/MagUp"):
                files_pidcorr.append((sigDir+"/MagUp/mergedUp.root",sigDir+"/mergedUp_pidcorr_tmp.root","MagUp_"+year))
                files_pidgen.append((sigDir+"/mergedUp_pidcorr_tmp.root",sigDir+"/mergedUp_pidcorr.root","MagUp_"+year))
            else: print(sigDir+"/MagUp  does NOT exist!")

            if os.path.exists(sigDir+"/MagDown"):
                files_pidcorr.append((sigDir+"/MagDown/mergedDown.root",sigDir+"/mergedDown_pidcorr_tmp.root","MagDown_"+year))
                files_pidgen.append((sigDir+"/mergedDown_pidcorr_tmp.root",sigDir+"/mergedDown_pidcorr.root","MagDown_"+year))
            else: print(sigDir+"/MagDown  does NOT exist!")


simversion = "run2"
# simversion = "sim09"

# Postfixes of the Pt, Eta and Ntracks variables (ntuple variable name w/o branch name)
# e.g. if the ntuple contains "pion_PT", it should be just "PT"
ptvar = "PT"
etavar = "ETA"
pvar   = "P"
## Could use P variable instead of eta
# etavar = None
# pvar   = "p"

ntrvar = "nTracks"  # This should correspond to the number of "Best tracks", not "Long tracks"!

# Dictionary of tracks with their PID variables, in the form {branch name}:{pidvars}
# For each track branch name, {pidvars} is a dictionary in the form {ntuple variable}:{pid config},
#   where
#     {ntuple variable} is the name of the corresponding ntuple PID variable without branch name,
#   and
#     {pid_config} is the string describing the PID configuration.
# Run PIDCorr.py without arguments to get the full list of PID configs
tracks_pidcorr = {
  'ProtonP'   : {
       "MC15TuneV1_ProbNNk"        :"p_LbLcPi_MC15TuneV1_ProbNNK_Brunel",
       "MC15TuneV1_ProbNNp"        :"p_LbLcPi_MC15TuneV1_ProbNNp_Brunel",
       "MC15TuneV1_ProbNNpi"       :"p_LbLcPi_MC15TuneV1_ProbNNpi_Brunel",
       },
  'ProtonM'   : {
       "MC15TuneV1_ProbNNk"        :"p_LbLcPi_MC15TuneV1_ProbNNK_Brunel",
       "MC15TuneV1_ProbNNp"        :"p_LbLcPi_MC15TuneV1_ProbNNp_Brunel",
       "MC15TuneV1_ProbNNpi"       :"p_LbLcPi_MC15TuneV1_ProbNNpi_Brunel",
       },
}

# TO STUDY ABOUT THIS SAMPLES
tracks_pidgen = {
  'ProtonP'   : {
       "PIDK"                  :"p_CombDLLK_Brunel",
       "PIDp"                  :"p_CombDLLp_Brunel",
       # "pidgen_LbLcPi_DLLpgt5_DLLpKgt5_MC15TuneV1_ProbNNp_Brunel":"p_LbLcPi_DLLpgt5_DLLpKgt5_MC15TuneV1_ProbNNp_Brunel",
       # # "pidgen_LbLcPi_MC12TuneV2gt0.05_MC15TuneV1_ProbNNp_Brunel":"p_LbLcPi_MC12TuneV2gt0.05_MC15TuneV1_ProbNNp_Brunel",
       # # "pidgen_LbLcPi_PIDpgt0_MC15TuneV1_ProbNNp_Brunel":"p_LbLcPi_PIDpgt0_MC15TuneV1_ProbNNp_Brunel",
       "Lc_ProbNNp"            :"p_Lc_MC15TuneV1_ProbNNp_Brunel",
       },
  'ProtonM'   : {
       "PIDK"                  :"p_CombDLLK_Brunel",
       "PIDp"                  :"p_CombDLLp_Brunel",
       # "pidgen_LbLcPi_DLLpgt5_DLLpKgt5_MC15TuneV1_ProbNNp_Brunel":"p_LbLcPi_DLLpgt5_DLLpKgt5_MC15TuneV1_ProbNNp_Brunel",
       # #\ "pidgen_LbLcPi_MC12TuneV2gt0.05_MC15TuneV1_ProbNNp_Brunel":"p_LbLcPi_MC12TuneV2gt0.05_MC15TuneV1_ProbNNp_Brunel",
       # # "pidgen_LbLcPi_PIDpgt0_MC15TuneV1_ProbNNp_Brunel":"p_LbLcPi_PIDpgt0_MC15TuneV1_ProbNNp_Brunel",
       "Lc_ProbNNp"            :"p_Lc_MC15TuneV1_ProbNNp_Brunel",
       },
  'Mu'   : {
       "PIDK"                 :"mu_CombDLLK_Brunel",
       "IsMuon_PIDK"          :"mu_CombDLLK_IsMuon_Brunel",
       "PIDe"                 :"mu_CombDLLe_Brunel",
       "PIDmu"                :"mu_CombDLLmu_Brunel",
       "IsMuon_PIDmu"         :"mu_CombDLLmu_IsMuon_Brunel",
       "PIDp"                 :"mu_CombDLLp_Brunel",
       "MC15TuneV1_ProbNNk"   :"mu_MC15TuneV1_ProbNNK_Brunel",
       "MC15TuneV1_ProbNNmu"  :"mu_MC15TuneV1_ProbNNmu_Brunel",
       "MC15TuneV1_ProbNNpi"  :"mu_MC15TuneV1_ProbNNpi_Brunel"
       },
}




# IF ON LXPLUS: if /tmp exists and is accessible, use for faster processing
# IF NOT: use /tmp if you have enough RAM
# temp_folder = '/tmp'
# ELSE: use current folder
# temp_folder = '/tmp'
temp_folder = '/tmp'
treeName = "DecayTree"
treeFolder = "Bc2JpsimunuTuple"

## END OF CONFIG


def AddBRs(files = []):
    from ROOT import ROOT, TTree, TChain, TFile
    from array import array

    from ROOT import RDataFrame
    ROOT.EnableImplicitMT()
    for input_file, _ , _  in files :
        print("Adding branches to ", input_file)
        dataFrame = RDataFrame(treeFolder+"/"+treeName, input_file)
        newDF = dataFrame.Define("Mu_IsMuon_PIDK","Mu_isMuon * Mu_PIDK") \
                         .Define("Mu_IsMuon_PIDmu","Mu_isMuon * Mu_PIDmu") \
                         .Define("ProtonP_Lc_ProbNNp","ProtonP_MC15TuneV1_ProbNNp") \
                         .Define("ProtonM_Lc_ProbNNp","ProtonM_MC15TuneV1_ProbNNp")
        newDF.Snapshot(treeFolder+"/"+treeName, input_file[:-5]+"_app.root")




# make sure we don't overwrite local files and prefix them with random strings
def make_pidcorr(files = [], tracks = {}):
    import string
    import random
    rand_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))  # get 10 random chars for temp_file prefix

    temp_file_prefix = temp_folder + '/' + rand_string  # prefix temp files with folder and unique ID


    for input_file, output_file, dataset in files :
      output_tree = treeName
      treename    = treeFolder+"/"+treeName
      inFile = input_file[:-5]+"_app.root"
      print("PROCESSING ",inFile," PIDCorr")
      tmpinfile = inFile
      tmpoutfile = "%s_tmp1.root" % temp_file_prefix
      for track, subst in tracks.iteritems() :
        for var, config in subst.iteritems() :
          command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDCorr.py"
          command += " -m %s_%s" % (track, ptvar)
          if etavar:
            command += " -e %s_%s" % (track, etavar)
          elif pvar:
            command += " -q %s_%s" % (track, pvar)
          else:
            print('Specify either ETA or P branch name per track')
            sys.exit(1)
          command += " -n %s" % ntrvar
          command += " -t %s" % treename
          command += " -p %s_%s_corr" % (track, var)
          command += " -s %s_%s" % (track, var)
          command += " -c %s" % config
          command += " -d %s" % dataset
          command += " -i %s" % tmpinfile
          command += " -o %s" % tmpoutfile
          command += " -S %s" % simversion
          # command += " -a"

          treename = output_tree
          tmpinfile = tmpoutfile
          if 'tmp1' in tmpoutfile:
            tmpoutfile = tmpoutfile.replace('tmp1', 'tmp2')
          else :
            tmpoutfile = tmpoutfile.replace('tmp2', 'tmp1')

          print(command)
          os.system(command)

      if "root://" in output_file:
        print("xrdcp %s %s" % (tmpinfile, output_file))
        os.system("xrdcp %s %s" % (tmpinfile, output_file))
      else:
        print("mv %s %s" % (tmpinfile, output_file))
        os.system("mv %s %s" % (tmpinfile, output_file))

def make_pidgen(files = [], tracks = {}, correct_muons=True):

    tracks_to_correct = {}
    if not correct_muons:
        keys_to_corr = ["ProtonP","ProtonM"]
        tracks_to_correct = {k:v for k,v in tracks_to_correct.items() if k in keys_to_corr}
    else:
        tracks_to_correct = tracks


    import string
    import random
    rand_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))  # get 10 random chars for temp_file prefix

    temp_file_prefix = temp_folder + '/' + rand_string  # prefix temp files with folder and unique ID
    # output_tree = input_tree.split("/")[-1]

    for input_file, output_file, dataset in files :
      print("PROCESSING ",input_file," PIDGen")
      output_tree = treeName
      treename    = treeName
      tmpinfile = input_file
      tmpoutfile = "%s_tmp1.root" % temp_file_prefix
      for track, subst in tracks.iteritems() :
        for var, config in subst.iteritems() :
          command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDGen.py"
          command += " -m %s_%s" % (track, ptvar)
          if etavar:
            command += " -e %s_%s" % (track, etavar)
          elif pvar:
            command += " -q %s_%s" % (track, pvar)
          else:
            print('Specify either ETA or P branch name per track')
            sys.exit(1)
          command += " -n %s" % ntrvar
          command += " -t %s" % treename
          command += " -p %s_%s_corr" % (track, var)
          command += " -c %s" % config
          command += " -d %s" % dataset
          command += " -i %s" % tmpinfile
          command += " -o %s" % tmpoutfile
          command += " -a"
          # if seed :
          #   command += " -s %d" % seed

          treename = output_tree
          tmpinfile = tmpoutfile
          if 'tmp1' in tmpoutfile:
            tmpoutfile = tmpoutfile.replace('tmp1', 'tmp2')
          else :
            tmpoutfile = tmpoutfile.replace('tmp2', 'tmp1')

          print(command)
          os.system(command)

      if "root://" in output_file:
        print("xrdcp %s %s" % (tmpinfile, output_file))
        os.system("xrdcp %s %s" % (tmpinfile, output_file))
      else:
        print("mv %s %s" % (tmpinfile, output_file))
        os.system("mv %s %s" % (tmpinfile, output_file))

def rmOutfiles(files = []):
    for input_file, output_file, dataset in files:
        os.system('rm -rf {}'.format(output_file))

AddBRs(files = files_pidcorr)
make_pidcorr(files=files_pidcorr, tracks=tracks_pidcorr)
make_pidgen(files=files_pidgen,   tracks=tracks_pidgen, correct_muons = False)
rmOutfiles(files=files_pidcorr)
